"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, pyqtSlot, qDebug

class HeaderController(QObject):
	def __init__(self, userModel, headerView, notificationsModel, imageResolver):
		QObject.__init__(self);

		self.__userModel = userModel; # connectionController.userDataModel()
		self.__notificationsModel = notificationsModel;
		self.__imageResolver = imageResolver;
		self.headerView = headerView;

		self.__userModel.updated.connect(self.__updateView);
		self.__userModel.avatarModel().updated.connect(self.__updateAvatar);
		self.__notificationsModel.unreadCountChanged.connect(
			self.__notificationsUpdated
		);

	def __notificationsUpdated(self):
		self.headerView.setNotificationsCount(
			self.__notificationsModel.unreadCount()
		);

	def __updateAvatar(self):
		self.headerView.avatarWidget().setPath(
			self.__userModel.avatarModel().local()
		);

	def __updateView(self):
		if self.__userModel.userData():
			self.headerView.nameWidget().setText(
								self.__userModel.userData()['name']);

			# Counts
			self.headerView.setNotificationsCount(
				int(self.__userModel.userData()["notifications_count"])
			);
			self.headerView.setMessageCount(
				int(self.__userModel.userData()["unread_messages_count"])
			);
		else:
			self.headerView.setNotificationsCount(0);
			self.headerView.setMessageCount(0);
			self.headerView.avatarWidget().clear();
			self.headerView.nameWidget().setText("Signed out.");
