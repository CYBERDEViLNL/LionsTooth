"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, QThreadPool, pyqtSignal, pyqtSlot, qDebug

from core.requestsWorker import RequestsWorker

from diaspy.models import Post as DiaspyPost

class PostsManager(QObject):
	"""Managers Post() models
	Collects diaspy.models.Post()'s from StreamControllers and manages them.
	"""
	anchorClicked = pyqtSignal(str);
	def __init__(self, connectionWrapper, imageResolver,
				userController, userModel):
		QObject.__init__(self);
		self.connectionWrapper = connectionWrapper;
		self.imageResolver = imageResolver;

		"""TODO yeah this is confusing.
		userController - controlls data from users
		userModel - signed in user model
		"""
		self.userController = userController;
		self.userModel = userModel;
		self.__postControllers = {}# <! key: guid, value PostController()

		self.connectionWrapper.statusChanged.connect(
			self.__connectionStatusChanged
		);

	@pyqtSlot(int)
	def __connectionStatusChanged(self, status):
		if not status:
			for guid in self.__postControllers:
				self.__postControllers[guid].deleteLater();

	def postController(self, guid):
		"""Returns postController for given guid.
		"""
		return self.__postControllers[guid];

	def addPost(self, post): return self.addStream([post]);

	@pyqtSlot(str)
	def _delPostControler(self, guid): self.__postControllers.pop(guid);

	def addStream(self, stream):
		guids = [];
		if stream:
			for post in stream:
				guids.append(post.guid);
				if post.guid not in self.__postControllers:
					self.__postControllers.update({
						post.guid: PostController(self.connectionWrapper,
													post,
													self.imageResolver,
													self.userController,
													self.userModel)
					});
					self.postController(post.guid).anchorClicked.connect(
						self.anchorClicked
					);
					self.postController(post.guid).destroyed.connect(
						lambda nullptr, guid=post.guid: self._delPostControler(guid)
					);
		return guids;

class InteractionsController(QObject):
	"""Can controll single InteractionView
	"""
	requestMoreLikes = pyqtSignal();
	requestMoreReshares = pyqtSignal();
	anchorClicked = pyqtSignal(str);
	def __init__(self, connectionWrapper, userController, imageResolver=None):
		QObject.__init__(self);

		self.__connectionWrapper = connectionWrapper;
		self._imageResolver = imageResolver;
		self.__userController = userController;
		self.__data = {};

		self.__liked = False;
		self.__reshared = False;
		self.__likeAvatarGuids = {}; # key author guid, value avatar index
		self.__reshareAvatarGuids = {}; # key author guid, value avatar index
		self._lackReshares = False;
		self._lackLikes = False;

		self.__views = [];

	def haveLiked(self):
		"""Returns True if the current user liked this post.
		"""
		return self.__liked;

	def haveReshared(self):
		"""Returns True if the current user reshared this post.
		"""
		return self.__reshared;

	def updateLikesFailed(self):
		for view in self.__views: view.likeAvatars().requestMoreFailed();

	def updateRehsharesFailed(self):
		for view in self.__views: view.reshareAvatars().requestMoreFailed();

	@pyqtSlot(object)
	def _viewDeleted(self, view):
		self.__views.pop(self.__views.index(view));

	def addView(self, view):
		self.__views.append(view);
		index = len(self.__views) - 1;
		view.destroyed.connect(lambda nullptr, v=view: self._viewDeleted(v));
		self.initView(index);
		return index

	def setLikes(self, likes):
		self.__liked = False;
		self.__data["likes"] = likes;

		ourCount = len(likes);
		if ourCount != int(self.__data["likes_count"]):
			self._lackLikes = True;
		elif ourCount:
			self._lackLikes = False;

		# Our own like would always be the first.
		if (ourCount and likes[0]["author"]["guid"] == self.__connectionWrapper
			.connection().userdata()["guid"]):
			self.__liked = True;

		if not self._lackLikes:
			for like in likes:
				# Check if we have data for the users that liked this post.
				if like["author"]["guid"] not in self.__userController:
					self.__userController.addUser(like["author"]["guid"]);
					#TODO update avatars and its data
					#user = self.__userController.user(reshare["author"]["guid"]);
					#user.updated.connect();
				user = self.__userController.user(like["author"]["guid"]);
				user.addAuthorData(like["author"]);

			self.updateLikesViews();

			self.__data["likes_count"] = str(ourCount);
			self.updateViews();

	def setReshares(self, reshares):
		self.__reshared = False;
		self.__data["reshares"] = reshares;

		ourCount = len(reshares);
		if ourCount != int(self.__data["reshares_count"]):
			self._lackReshares = True;
		elif ourCount:
			self._lackReshares = False;

		# Our own reshare would always be the first.
		if (ourCount and reshares[0]["author"]["guid"] ==
			self.__connectionWrapper.connection().userdata()["guid"]):
			self.__reshared = True;

		if not self._lackReshares:
			for reshare in reshares:
				if (reshare["author"]["guid"] == self.__connectionWrapper
					.connection().userdata()["guid"]): self.__reshared = True;

				# Check if we have data for the users that liked this post.
				if reshare["author"]["guid"] not in self.__userController:
					self.__userController.addUser(reshare["author"]["guid"]);
					#TODO update avatars and its data
					#user = self.__userController.user(reshare["author"]["guid"]);
					#user.updated.connect();
				user = self.__userController.user(reshare["author"]["guid"]);
				user.addAuthorData(reshare["author"]);

			self.updateResharesViews();

			self.__data["reshares_count"] = str(ourCount);
			self.updateViews();

	def setData(self, data):
		self.__data = data;

		self.setLikes(self.__data["likes"]);
		self.setReshares(self.__data["reshares"]);

		self.updateViews();

	def updateViews(self):
		for index, view in enumerate(self.__views): self.updateView(index);

	def updateView(self, index):
		self.__views[index].setLikeCount(str(self.__data["likes_count"]));
		self.__views[index].setReshareCount(str(self.__data["reshares_count"]));
		self.__views[index].setCommentCount(str(self.__data["comments_count"]));

	def initViews(self):
		for index, view in enumerate(self.__views): self.initView(index);

	def initView(self, index):
		self.__views[index].likeAvatars().requestMore.connect(
			self.requestMoreLikes
		);
		self.__views[index].reshareAvatars().requestMore.connect(
			self.requestMoreReshares
		);

		if not self._lackLikes: self.updateLikesView(index);
		if not self._lackReshares: self.updateResharesView(index);

		self.updateView(index);

	def updateLikesViews(self):
		for index, view in enumerate(self.__views): self.updateLikesView(index);

	"""TODO create contoller class that controls likes and rehsares view
	they both need same functionality. See below..
	"""
	def updateLikesView(self, index):
		# First purge likes
		updatedGuids = [like["author"]["guid"] for like in self.__data["likes"]];
		guidsToRemove = [];
		for guid, avatarIndex in self.__likeAvatarGuids.items():
			if guid not in updatedGuids:
				# remove that avatar ..
				self.__views[index].likeAvatars().avatar(avatarIndex).deleteLater();
				guidsToRemove.append(guid);
		for guid in guidsToRemove: self.__likeAvatarGuids.pop(guid);

		if not self._lackLikes:
			for like in self.__data["likes"]:
				if like["author"]["guid"] not in self.__likeAvatarGuids:
					avatarIndex = self.__views[index].likeAvatars().addAvatar();
					avatar = self.__views[index].likeAvatars().avatar(avatarIndex);
					user = self.__userController.user(like["author"]["guid"]);
					avatar.setGuid(like["author"]["guid"]);
					avatar.setName(like["author"]["name"]);
					avatar.setDiasporaId(like["author"]["diaspora_id"]);
					avatar.setToolTip(avatar.infoStr());
					avatarUrl = user.avatar("small");
					diasporaId = user.data("diaspora_id", "");
					if diasporaId:
						avatar.setHyperlink(
							"#diaspora_id:{}".format(diasporaId)
						);
					avatar.anchorClicked.connect(self.anchorClicked);
					if avatarUrl:
						workerId = self._imageResolver.newJob(avatarUrl);
						self._imageResolver.worker(workerId).signals.result.connect(avatar.setPath);
						self._imageResolver.startWorker(workerId);
					else: qDebug("WTF DUDE, no avatar url.");

					self.__likeAvatarGuids.update(
						{like["author"]["guid"]:avatarIndex}
					);
				else: pass; # TODO Update data?

	def updateResharesViews(self):
		for index, view in enumerate(self.__views): self.updateResharesView(index);

	def updateResharesView(self, index):
		# First purge likes
		updatedGuids = [reshare["author"]["guid"] for reshare in self.__data["reshares"]];
		guidsToRemove = [];
		for guid, avatarIndex in self.__reshareAvatarGuids.items():
			if guid not in updatedGuids:
				# remove that avatar ..
				self.__views[index].reshareAvatars().avatar(avatarIndex).deleteLater();
				guidsToRemove.append(guid);
		for guid in guidsToRemove: self.__reshareAvatarGuids.pop(guid);

		if not self._lackReshares:
			for reshare in self.__data["reshares"]:
				if reshare["author"]["guid"] not in self.__reshareAvatarGuids:
					avatarIndex = self.__views[index].reshareAvatars().addAvatar();
					avatar = self.__views[index].reshareAvatars().avatar(avatarIndex);
					user = self.__userController.user(reshare["author"]["guid"]);
					avatar.setGuid(reshare["author"]["guid"]);
					avatar.setName(reshare["author"]["name"]);
					avatar.setDiasporaId(reshare["author"]["diaspora_id"]);
					avatar.setToolTip(avatar.infoStr());
					avatarUrl = user.avatar("small");
					diasporaId = user.data("diaspora_id", "");
					if diasporaId:
						avatar.setHyperlink(
							"#diaspora_id:{}".format(diasporaId)
						);
					avatar.anchorClicked.connect(self.anchorClicked);
					if avatarUrl:
						workerId = self._imageResolver.newJob(avatarUrl);
						self._imageResolver.worker(workerId).signals.result.connect(avatar.setPath);
						self._imageResolver.startWorker(workerId);

					self.__reshareAvatarGuids.update(
						{reshare["author"]["guid"]:avatarIndex}
					);
				else: pass; # TODO Update data?


class CommentController(QObject):
	anchorClicked = pyqtSignal(str);
	deleteRequest = pyqtSignal();
	def __init__(self, id, text, createdAt, user, imageResolver):
		QObject.__init__(self);

		self._id = id;
		self._imageResolver = imageResolver;
		self._markdown = text;
		self._user = user;
		self._createdAt = createdAt;
		self._views = [];

		user.updated.connect(self.updateAuthorViews);

	def id(self): return self._id;

	def addView(self, view):
		self._views.append(view);
		index = len(self._views) - 1;
		view.destroyed.connect(lambda nullptr, index=index: self._viewDeleted(index));
		view.deleteRequest.connect(self.deleteRequest);
		self.initView(index);
		return index;

	@pyqtSlot(int)
	def _viewDeleted(self, index): del self._views[index];

	def initView(self, index):
		# Set
		self._views[index].timeAgoWidget().setDatetime(self._createdAt);
		self._views[index].markdownView().setImageResolver(self._imageResolver);
		self._views[index].markdownView().setMarkdown(self._markdown);

		# Connect
		self._views[index].avatarWidget().anchorClicked.connect(self.anchorClicked);

		# Do
		self.initAuthorView(index);

	def updateAuthorViews(self):
		for index, view in enumerate(self._views):
			self.updateAuthorView(index);

	@pyqtSlot(str)
	def __avatarFailed(self, err): pass;

	def initAuthorView(self, index):
		diasporaId = self._user.data("diaspora_id", "");
		if diasporaId:
			self._views[index].avatarWidget().setHyperlink(
				"#diaspora_id:{}".format(diasporaId)
			);

		self.updateAuthorView(index)

	def updateAuthorView(self, index):
		if self._user:
			avatarUrl = self._user.avatar("small");
			if avatarUrl:
				workerId = self._imageResolver.newJob(avatarUrl);
				self._imageResolver.worker(workerId).signals.result.connect(
									self._views[index].avatarWidget().setPath);
				self._imageResolver.worker(workerId).signals.exception.connect(
														self.__avatarFailed);
				self._imageResolver.startWorker(workerId);
			self._views[index].authorNameWidget().setText(
				self._user.data("name", "${NOT_SET2}")
			);


class CommentsController(QObject):
	"""Controller for comments
	"""
	anchorClicked = pyqtSignal(str);
	deleteRequest = pyqtSignal(int); # id
	def __init__(self, connectionWrapper, commentsModel, imageResolver, userController, remoteCount=0):
		QObject.__init__(self);

		self.__connectionWrapper = connectionWrapper; # TODO for now its only  needed to determine if comment is the users. Usermodel is enough?
		self.__model = commentsModel;
		self._imageResolver = imageResolver;
		self.__userController = userController;
		self.__views = []; # commentLayouts on different pages
		self.__commentControllers = {}; #<! key: comment id, value controller
		self.__users = {}; # <! key: guid, value User()
		self._collapseCount = 3;
		self._remoteCount = remoteCount;
		self.__commentsIndex = []; # list with dicts key: comment id, value: index in view. index is relative to view index

		self.__modelUpdated();

	def setRemoteCount(self, count): self._remoteCount = count;

	def modelUpdated(self):
		 self.__modelUpdated();
		 self.updateViews();

	@pyqtSlot()
	def __commentDeleteRequest(self):
		commentController = self.sender();
		self.deleteRequest.emit(commentController.id());

	def __modelUpdated(self):
		self.purgeComments();
		for comment in self.__model:
			if comment.guid not in self.__commentControllers:
				if comment.author("guid"):
					if comment.author("guid") not in self.__userController:
						self.__userController.addUser(comment.author("guid"));
					user = self.__userController.user(comment.author("guid"));
					user.addAuthorData(comment.authordata());

					commentController = CommentController(
												comment.id,
												str(comment),
												comment.when(),
												user,
												self._imageResolver);
					commentController.anchorClicked.connect(self.anchorClicked);
					commentController.deleteRequest.connect(self.__commentDeleteRequest);
					self.__commentControllers.update({comment.guid
														:commentController});

	@pyqtSlot(object)
	def _viewDeleted(self, view):
		self.__views.pop(self.__views.index(view));

	def addView(self, view):
		self.__views.append(view);
		self.__commentsIndex.append({});
		index = len(self.__views) - 1;
		view.destroyed.connect(lambda nullptr, v=view: self._viewDeleted(view));
		view.collapseStateChanged.connect(self._collapseStateChanged);
		self.updateView(index);
		return index;

	def updateViews(self):
		for index, view in enumerate(self.__views): self.updateView(index);

	def purgeComments(self):
		updatedGuids = [comment.guid for comment in self.__model];
		guidsToRemove = [];
		for index, view in enumerate(self.__views):
			for guid, commentViewIndex in self.__commentsIndex[index].items():
				if guid not in updatedGuids: guidsToRemove.append([guid,commentViewIndex]);
			for guidToRemove in guidsToRemove:
				for view in self.__views:
					view.delCommentIndex(guidToRemove[1]);
				del self.__commentControllers[guidToRemove[0]];

	@pyqtSlot(int, str)
	def __commentViewDestroyed(self, viewIndex, guid):
		if guid in self.__commentsIndex[viewIndex]:
			self.__commentsIndex[viewIndex].pop(guid);

	def updateView(self, index):

		commentCount = len(self.__model);
		self.__views[index].setCount(self._remoteCount);

		""":NOTE: CommentsController updateView()
		Behaviour now is that when the post is updated and we have already
		3 or more comments and the post contains new comments that the new
		comments will be appended no mather if the collapseCount is reached or
		not. This since the original 3 shown comments are not deleted if new
		comments are added.

		Option 0:
			Leave it this way.

		Option 1:
			Shift comments to last 3. (delete the 3 shown comments and add the
										last 3 of the new comments agian.)

		Option 2:
			Don't append new comments yet but store their guids somewhere and
			add a button to the end that says show x new comments.

			If this happends again the container and button should be updated.

		"""
		# If collapsed (default) then add only last 3 comments.
		if self.__views[index].collapseState() and commentCount > self._collapseCount:
			viewCommentsCount = len(self.__views[index].comments());
			startIndex = commentCount - self._collapseCount;
			if viewCommentsCount > self._collapseCount:
				diff = viewCommentsCount - self._collapseCount;
				startIndex -= diff;
			for commentIndex in range(startIndex, commentCount):
				commentModel = self.__model[commentIndex];
				if commentModel.guid not in self.__commentsIndex[index]:

					ownComment = False;
					if (commentModel.author("guid") == self.__connectionWrapper
						.connection().userdata()["guid"]):
						ownComment = True;

					commentViewIndex = self.__views[index].addComment(
						ownComment=ownComment
					);
					commentView = self.__views[index].comment(commentViewIndex);
					self.__commentControllers[commentModel.guid].addView(
						commentView
					);
					self.__commentsIndex[index].update(
						{commentModel.guid:commentViewIndex}
					);
					commentView.destroyed.connect(
						lambda nullptr: self.__commentViewDestroyed(
							index, commentModel.guid
						)
					);

			
			self.__views[index].updateCollapseButton(viewCommentsCount);

		else: # If not collapsed or less comments than collapseCount, add all comments.
			for commentModel in self.__model:
				if commentModel.guid not in self.__commentsIndex[index]:

					ownComment = False;
					if (commentModel.author("guid") == self.__connectionWrapper
						.connection().userdata()["guid"]):
						ownComment = True;

					commentIndexView = self.__views[index].addComment(
						ownComment=ownComment
					);
					commentView = self.__views[index].comment(commentIndexView);
					self.__commentControllers[commentModel.guid].addView(
						commentView
					);
					self.__commentsIndex[index].update(
						{commentModel.guid:commentIndexView}
					);

			self.__views[index].updateCollapseButton(self._collapseCount);

	@pyqtSlot(bool)
	def _collapseStateChanged(self, state):
		commentCount = len(self.__model);
		commentsView = self.sender();
		if state: # Show less
			for commentIndex in range(0, commentCount - self._collapseCount):
				commentsView.delCommentIndex(0);
		else: # Show more
			viewCommentsCount = len(commentsView.comments());
			startIndex = commentCount - self._collapseCount;
			if viewCommentsCount > self._collapseCount:
				diff = viewCommentsCount - self._collapseCount;
				startIndex -= diff;
			for commentIndex in range(0, startIndex):
				commentModel = self.__model[commentIndex];

				ownComment = False;
				if (commentModel.author("guid") == self.__connectionWrapper
					.connection().userdata()["guid"]):
					ownComment = True;

				commentViewIndex = commentsView.addComment(commentIndex, ownComment=ownComment);
				commentView = commentsView.comment(commentViewIndex);

				viewIndex = self.__views.index(commentsView)
				self.__commentsIndex[viewIndex].update({commentModel.guid:commentViewIndex});
				commentView.destroyed.connect(lambda nullptr, index=viewIndex, guid=commentModel.guid: self.__commentViewDestroyed(index, guid));
				self.__commentControllers[commentModel.guid].addView(commentView);
		commentsView.updateCollapseButton(self._collapseCount);

class PostController(QObject):
	"""Controller for multiple post views
	"""
	anchorClicked = pyqtSignal(str);
	def __init__(self, connectionWrapper, postModel, imageResolver,
				userController, userModel, fromReshare=False):
		QObject.__init__(self);

		self.connectionWrapper = connectionWrapper;
		self._isReshare = False;
		self._fromReshare = fromReshare;
		self._imageResolver = imageResolver;
		self._userController = userController;
		self._user = None;
		self.__views = [];
		self.__model = postModel;
		self.__interactionController = None;

		self._userModel = userModel; # TODO this is confusing.. userModel = data from signed in user.
		self._publisher = None;

		self._liked = False;
		self._reshared = False;
		self._ownPost = False;
		self._hasPhotos = False;
		self._hasPoll = False;
		self._public = self.data(["public"], False);
		self._rootGuid = "";

		if (self.data(["author", "guid"], False) == self.connectionWrapper
											.connection().userdata()["guid"]):
			self._ownPost = True;

		""" Check on poll
		"""
		if self.data(["poll"], []): self._hasPoll = True;

		""" Check on photos
		"""
		if self.data(["photos"], []): self._hasPhotos = True;

		""" Init a UserController.User() instance.
		"""
		authorGuid = self.data(["author", "guid"], False);
		if authorGuid:
			if authorGuid not in self._userController:
				self._userController.addUser(authorGuid);
			self._user = self._userController.user(authorGuid);
			self._user.updated.connect(self.__updateAuthorDataViews);
		else:
			qDebug(
				"Houston.. we have a problem. `{}`".format(self.data())
			);

		""" Init InteractionsController()
		"""
		if not fromReshare:
			self.__interactionController = InteractionsController(
										self.connectionWrapper,
										self._userController,
										imageResolver = self._imageResolver);
			self.__interactionController.anchorClicked.connect(
				self.anchorClicked
			);
			#self._liked = self.__interactionController.haveLiked();
			#self._reshared = self.__interactionController.haveReshared();
			self.__interactionController.requestMoreLikes.connect(
				self.updateLikes
			);
			self.__interactionController.requestMoreReshares.connect(
				self.updateReshares
			);
			self.__commentsController = CommentsController(
				self.connectionWrapper,
				self.__model.comments,
				self._imageResolver,
				self._userController,
				remoteCount=self.__model.data()["interactions"]["comments_count"]
			);
			self.__commentsController.anchorClicked.connect(self.anchorClicked);
			self.__commentsController.deleteRequest.connect(
				self.__deleteComment
			);

		self.__resharePostController = None;
		""" Init a PostController for reshared post
		"""
		if (not fromReshare
			and self.data(["post_type"], "").lower() == "reshare"):
			self._isReshare = True;
			self._hasPhotos = False;

			""" yes it could have photos if the root has, but we don't want to
			show it twice do we? :-)"""
			self._rootGuid = self.data(["root", "guid"]);

			resharedPost = DiaspyPost(self.connectionWrapper.connection(),
									guid=self._rootGuid,
									fetch=False, comments=False,
									post_data=self.data(["root"]));
			self.__resharePostController = PostController(
												self.connectionWrapper,
												resharedPost,
												self._imageResolver,
												self._userController,
												self._userModel,
												fromReshare=True);

		self.__initModel();

	@pyqtSlot(object)
	def _togglePublisher(self, view):
		#view = self.sender();
		if view.publisher():
			view.deletePublisher();
		else:
			view.addPublisher(self._userModel);
			#view.publisher().publishRequest.connect(self._comment);
			# TODO remove those lambdas, create ids for views instead.
			view.publisher().publishRequest.connect(
				lambda v=view: self._comment(v)
			);

	@pyqtSlot(str)
	def _toggleParticipationFailed(self, err):
		qDebug("_toggleParticipationFailed {}".format(err));
		for view in self.__views:
			if view.participationButton():
				view.participationButton().setEnabled(True);

	@pyqtSlot()
	def _toggleParticipationSucceeded(self):
		qDebug("_toggleParticipationSucceeded");
		for view in self.__views:
			if view.participationButton():
				view.participationButton().setEnabled(True);
				view.setParticipated(self.data(["participation"]));

	def _toggleParticipation(self):
		for view in self.__views:
			if view.participationButton():
				view.participationButton().setEnabled(False);
		if self.data(["participation"]):
			print("unsubscribe");
			worker = RequestsWorker(self.__model.unsubscribe);
		else:
			print("subscribe");
			worker = RequestsWorker(self.__model.subscribe);
		worker.signals.exception.connect(self._toggleParticipationFailed);
		worker.signals.success.connect(self._toggleParticipationSucceeded);
		QThreadPool.globalInstance().start(worker);

	@pyqtSlot(str)
	def __commentFailed(self, err):
		qDebug("_commentFailed");
		for view in self.__views:
			if view.publisher(): view.publisher().setEnabled(True);

	@pyqtSlot()
	def __commentSucceeded(self):
		qDebug("_commentSucceeded");
		self.__commentsController.modelUpdated();
		for view in self.__views:
			if view.publisher(): view.publisher().published();

	def _comment(self, view):
		text = view.publisher().markdownText();
		if text:
			worker = RequestsWorker(self.__model.comment, text=text);
			worker.signals.exception.connect(self.__commentFailed);
			worker.signals.success.connect(self.__commentSucceeded);
			QThreadPool.globalInstance().start(worker);

	@pyqtSlot(str)
	def __deleteCommentFailed(self, err):
		qDebug("__deleteCommentFailed: `{}`".format(err));

	@pyqtSlot()
	def __deleteCommentSucceeded(self):
		qDebug("__deleteCommentSucceeded");
		self.__commentsController.modelUpdated();


	def __deleteComment(self, id):
		worker = RequestsWorker(self.__model.delete_comment, comment_id=id);
		worker.signals.exception.connect(self.__deleteCommentFailed);
		worker.signals.success.connect(self.__deleteCommentSucceeded);
		QThreadPool.globalInstance().start(worker);

	def __del(self):
		for view in self.__views:
			view.deleteLater();
			view.setParent(None);
		self.deleteLater();
		self.setParent(None);

	@pyqtSlot(object)
	def _viewDeleted(self, view):
		qDebug(
			"PostControler delete view index {}".format(
				self.__views.index(view)
			)
		);
		self.__views.pop(self.__views.index(view));

	def reshareController(self): return self.__resharePostController;

	def addView(self, view):
		self.__views.append(view);
		index = len(self.__views) - 1;

		if self.__resharePostController:
			self.__resharePostController.addView(view.reshareView());
			self.__resharePostController.anchorClicked.connect(
				self.anchorClicked
			);

		if not self._fromReshare:
			self.__interactionController.addView(view.interactionsView());
			self.__commentsController.addView(view.commentsView());

			view.likeButton().clicked.connect(self.__toggleLike);
			view.reshareButton().clicked.connect(self._reshare);
			#view.commentButton().clicked.connect(self._togglePublisher);
			view.commentButton().clicked.connect(
				lambda checked, v=view: self._togglePublisher(v)
			);

		if view.deleteButton():
			view.deleteButton().clicked.connect(self.__delete);
		if view.participationButton():
			view.setParticipated(self.data(["participation"]));
			view.participationButton().clicked.connect(
				self._toggleParticipation
			);
		# TODO
		#if view.reportButton(): view.reportButton().clicked.connect();
		#if view.hideButton(): view.hideButton().clicked.connect();
		#if view.blockButton(): view.blockButton().clicked.connect();

		view.requestUpdate.connect(self.updateData);
		view.anchorClicked.connect(self.anchorClicked);
		view.destroyed.connect(lambda nullptr, v=view: self._viewDeleted(v));

		self.__initView(index);

		return index;

	@pyqtSlot()
	def __deleteSucceeded(self):
		for view in self.__views: view.deleteLater();
		self.deleteLater();

	@pyqtSlot(str)
	def __deleteFailed(self, err):
		qDebug("[DEBUG] PostController.__deleteFailed `{0}`".format(err));
		for view in self.__views: view.deleteButton().setEnabled(True);

	def __delete(self):
		for view in self.__views: view.deleteButton().setEnabled(False);
		worker = RequestsWorker(self.__model.delete);
		worker.signals.exception.connect(self.__deleteFailed);
		worker.signals.success.connect(self.__deleteSucceeded);
		QThreadPool.globalInstance().start(worker);

	@pyqtSlot(object)
	def __likeSucceeded(self, likesJson):
		self.__modelUpdated();
		for view in self.__views: view.likeButton().setEnabled(True);

	@pyqtSlot(str)
	def __likeFailed(self, err):
		qDebug("[DEBUG] PostController.__likeFailed `{0}`".format(err));
		for view in self.__views: view.likeButton().setEnabled(True);

	def __like(self):
		for view in self.__views: view.likeButton().setEnabled(False);
		worker = RequestsWorker(self.__model.like);
		worker.signals.exception.connect(self.__likeFailed);
		worker.signals.result.connect(self.__likeSucceeded);
		QThreadPool.globalInstance().start(worker);

	@pyqtSlot()
	def __unlikeSucceeded(self):
		self.__modelUpdated();
		for view in self.__views: view.likeButton().setEnabled(True);

	@pyqtSlot(str)
	def __unlikeFailed(self, err):
		qDebug("[DEBUG] PostController.__unlikeFailed `{0}`".format(err));
		for view in self.__views: view.likeButton().setEnabled(True);

	def __unlike(self):
		for view in self.__views: view.likeButton().setEnabled(False);
		worker = RequestsWorker(self.__model.delete_like);
		worker.signals.exception.connect(self.__unlikeFailed);
		worker.signals.success.connect(self.__unlikeSucceeded);
		QThreadPool.globalInstance().start(worker);

	def __toggleLike(self):
		if self._liked: self.__unlike();
		else: self.__like();

	@pyqtSlot()
	def __reshareSucceeded(self):
		self.__modelUpdated();
		for view in self.__views: view.reshareButton().setEnabled(True);

	@pyqtSlot(str)
	def __reshareFailed(self, err):
		qDebug("[DEBUG] PostController.__reshareFailed `{0}`".format(err));
		for view in self.__views: view.reshareButton().setEnabled(True);

	def _reshare(self):
		for view in self.__views: view.reshareButton().setEnabled(False);
		worker = RequestsWorker(self.__model.reshare);
		worker.signals.exception.connect(self.__reshareFailed);
		worker.signals.success.connect(self.__reshareSucceeded);
		QThreadPool.globalInstance().start(worker);

	def __updateInteractions(self):
		if not self._fromReshare:
			self.__interactionController.setData(
				self.__model.data()["interactions"]
			);
			self._liked = self.__interactionController.haveLiked();
			self._reshared = self.__interactionController.haveReshared();

	@pyqtSlot()
	def __modelUpdated(self):
		self.__updateInteractions();
		self.__updateViews();
		self.__commentsController.setRemoteCount(
			self.__model.data()["interactions"]["comments_count"]
		);
		self.__commentsController.modelUpdated();

	@pyqtSlot()
	def __updateSucceeded(self): self.__modelUpdated();

	@pyqtSlot(str)
	def __updateFailed(self, err):
		qDebug("[DEBUG] PostController.__updateFailed() `{0}`".format(err));
		if len(err) > 18 and err[0:17] == "Diaspy error: 404":
			qDebug("post deleted");
			self.__del();

	def __updateData(self):
		"""This will also fetch authors of likes and reshares, also comments.
		Default posts from a stream lack likes, shares and comments.
		"""
		self.__model.fetch(comments=True);

	#@pyqtSlot()
	def updateData(self):
		worker = RequestsWorker(self.__updateData);
		worker.signals.exception.connect(self.__updateFailed);
		worker.signals.success.connect(self.__updateSucceeded);
		QThreadPool.globalInstance().start(worker);

	def __updateLikes(self): return self.__model.fetchlikes();
	def __updateReshares(self): return self.__model.fetchreshares();

	@pyqtSlot(object)
	def __updateLikesSucceeded(self, likes):
		qDebug("[DEBUG] PostController.__updateLikesSucceeded()");
		self.__interactionController.setLikes(likes);

	@pyqtSlot(str)
	def __updateLikesFailed(self, err):
		qDebug(
			"[DEBUG] PostController.__updateLikesFailed() `{0}`".format(err)
		);
		self.__interactionController.updateLikesFailed();

	def updateLikes(self):
		worker = RequestsWorker(self.__updateLikes);
		worker.signals.exception.connect(self.__updateLikesFailed);
		worker.signals.result.connect(self.__updateLikesSucceeded);
		QThreadPool.globalInstance().start(worker);

	@pyqtSlot(object)
	def __updateResharesSucceeded(self, reshares):
		self.__interactionController.setReshares(reshares);

	@pyqtSlot(str)
	def __updateResharesFailed(self, err):
		qDebug(
			"[DEBUG] PostController.__updateResharesFailed() `{0}`".format(err)
		);
		self.__interactionController.updateResharesFailed();

	def updateReshares(self):
		worker = RequestsWorker(self.__updateReshares);
		worker.signals.exception.connect(self.__updateResharesFailed);
		worker.signals.result.connect(self.__updateResharesSucceeded);
		QThreadPool.globalInstance().start(worker);

	def data(self, keyList=[], default=None):
		"""
		:param keyList: keys to look for in self.__model.data()
		:type keyList: list
		:param default: What to return if key not found.
		:type default: Anything
		"""
		history = [];
		result = self.__model.data();
		for key in keyList:
			if key in result:
				result = result[key]
				history.append(key);
			else:
				qDebug("[DEBUG] PostController.data() the following keys where"
						" requested `{0}` but key `{1}` does not exist."
						.format(keyList, key));
				
				return default;
		return result;

	def __initModel(self):
		# Add interaction data to InteractionController
		if not self._fromReshare:
			self.__interactionController.setData(
										self.__model.data()["interactions"]);
			self._liked = self.__interactionController.haveLiked();
			self._reshared = self.__interactionController.haveReshared();

		# Add author data to UserController
		if self._user: self._user.addAuthorData(self.data(["author"]));

		# Add author data from photos to UserController
		if self.hasPhotos():
			for photo in self.data(["photos"], []):
				if photo["author"]["guid"] not in self._userController:
					self._userController.addUser(photo["author"]["guid"]);
				user = self._userController.user(photo["author"]["guid"]);
				user.addAuthorData(photo["author"]);

	def __updateViews(self):
		for index, view in enumerate(self.__views): self.__updateView(index);

	def __updateView(self, index):
		# Update author data
		self.__updateAuthorDataView(index);

		# Update feedback buttons states
		if not self._fromReshare:
			if not self._ownPost and self._public:
				self.__views[index].setReshared(self._reshared);
			self.__views[index].setLiked(self._liked);

		# Update poll
		if self.hasPoll():
			self.__views[index].pollWidget().updateData(self.data(["poll"]));
			answer = self.data(["poll_participation_answer_id"]);
			if answer: self.__views[index].pollWidget().voted(answer);

	def __updatePolls(self):
		for index, view in enumerate(self.__views):
			view.pollWidget().updateData(self.data(["poll"]));
			answer = self.data(["poll_participation_answer_id"]);
			if answer: view.pollWidget().voted(answer);

	def __updateAuthorDataViews(self):
		for index, view in enumerate(self.__views):
			self.__updateAuthorDataView(index);

	def __updateAuthorDataView(self, index):
		# Avatar
		if self._user:
			avatarUrl = self._user.avatar("small");
			if avatarUrl:
				workerId = self._imageResolver.newJob(avatarUrl);
				self._imageResolver.worker(workerId).signals.result.connect(
					self.__views[index].avatarWidget().setPath
				);
				self._imageResolver.worker(workerId).signals.exception.connect(
					self.__avatarFailed
				);
				self._imageResolver.startWorker(workerId);

			# Author name
			self.__views[index].authorNameWidget.setText(
				self._user.data("name", "${NOT_SET}")
			);
			diasporaId = self._user.data("diaspora_id", "");
			if diasporaId:
				self.__views[index].avatarWidget().setHyperlink(
					"#diaspora_id:{}".format(diasporaId)
				);
		else:
			qDebug("[DEBUG] PostController.__updateAuthorData() no self._user");

	@pyqtSlot()
	def __thumbClicked(self):
		thumbIndex = self.sender().index();

		for view in self.__views:
			workerId = self._imageResolver.newJob(
				self.data(["photos"])[thumbIndex]["sizes"]["large"]
			);
			self._imageResolver.worker(workerId).signals.result.connect(
								view.photosView().photoWidget().setPath);
			self._imageResolver.worker(workerId).signals.exception.connect(
													self.__photoFailed);
			self._imageResolver.startWorker(workerId);

	@pyqtSlot()
	def __retryImage(self):
		image = self.sender();
		qDebug("retry {0}".format(image.url()));
		workerId = self._imageResolver.newJob(image.url());
		self._imageResolver.worker(workerId).signals.result.connect(
			image.setPath
		);
		self._imageResolver.worker(workerId).signals.exception.connect(
			image.setFailed
		);
		self._imageResolver.startWorker(workerId);

	@pyqtSlot(str)
	def __votePollFailed(self, err): qDebug(err);

	@pyqtSlot(object)
	def __voteSucceeded(self, data): self.__updatePolls();

	def __votePoll(self, answerId):
		return self.__model.vote_poll(answerId);

	@pyqtSlot(int)
	def __votePollRequest(self, answerId):
		worker = RequestsWorker(job=self.__votePoll, answerId=answerId);
		worker.signals.exception.connect(self.__votePollFailed);
		worker.signals.result.connect(self.__voteSucceeded);
		QThreadPool.globalInstance().start(worker);

	def __initView(self, index):
		"""
		NOTES:
			You cannot reshare when:
			 - It is your own post.
			 - The root of the post is your own post. (reshare of own post)
			 - The post is private.
			 - You already reshared the post.

			You cannot like when:
			- You already liked the post.

			You can only delete comments when:
			 - They are your own.
			 - They are made in your post.

			You can report all posts.
			You can report all comments.
		"""
		self.__updateAuthorDataView(index);

		if self._ownPost or not self._public:
			self.__views[index].reshareButton().hide();

		# Poll
		if self.hasPoll():
			answer = self.data(["poll_participation_answer_id"]);
			self.__views[index].addPoll(self.data(["poll"]), answer=answer);
			if not answer:
				self.__views[index].pollWidget().voteRequest.connect(
					self.__votePollRequest
				);

		# Photos
		if self.hasPhotos():
			photos = self.data(["photos"]);
			if photos:
				# First photo
				workerId = self._imageResolver.newJob(
					photos[0]["sizes"]["large"]
				);
				photoWidget = self.__views[index].photosView().photoWidget();
				photoWidget.set(url=photos[0]["sizes"]["large"]);
				self._imageResolver.worker(workerId).signals.result.connect(
					photoWidget.setPath
				);
				self._imageResolver.worker(workerId).signals.exception.connect(
					self.__photoFailed
				);
				self._imageResolver.worker(workerId).signals.exception.connect(
					photoWidget.setFailed
				);
				self._imageResolver.startWorker(workerId);
				photoWidget.requestRetry.connect(self.__retryImage);

				# Thumbs
				if len(photos) > 1:
					for photo in photos:
						thumbIndex = self.__views[index].photosView().addThumb(
							url=photo["sizes"]["medium"]
						);
						workerId = self._imageResolver.newJob(
							photo["sizes"]["medium"]
						);
						thumb = self.__views[index].photosView().thumb(
							thumbIndex
						);
						self._imageResolver.worker(workerId).signals.result.connect(
							thumb.setPath
						);
						self._imageResolver.worker(workerId).signals.exception.connect(
							self.__thumbFailed
						);
						self._imageResolver.worker(workerId).signals.exception.connect(
							thumb.setFailed
						);
						self._imageResolver.startWorker(workerId);

						thumb.requestRetry.connect(self.__retryImage);

						self.__views[index].photosView().thumb(thumbIndex).clicked.connect(
							self.__thumbClicked
						);


		# Created at
		self.__views[index].timeAgoWidget.setDatetime(
			self.data(["created_at"])
		);

		# Content
		if self.isReshare(): self.__resharePostController.__updateViews();
		else:
			if not self.__views[index].markdownView().hasImageResolver():
				self.__views[index].markdownView().setImageResolver(
					self._imageResolver
				);
				#self.__views[index].markdownView().anchorClicked.connect(self.);
			self.__views[index].markdownView().setMarkdown(
				self.data(["text"], "${NOT_SET}")
			);

		# Scope - interactions
		if not self._fromReshare:
			self.__views[index].scopeWidget.set(
				self.data(["public"], True),
				self.data(["provider_display_name"])
			);

			if not self._ownPost and self._public and self._reshared:
				self.__views[index].setReshared();
			if self._liked: self.__views[index].setLiked();
			self.__interactionController.updateView(index);

	def isReshare(self): return self._isReshare;
	def hasPhotos(self): return self._hasPhotos;
	def hasPoll(self): return self._hasPoll;
	def rootGuid(self): return self._rootGuid;
	def ownPost(self): return self._ownPost;

	@pyqtSlot(str)
	def __avatarFailed(self, err):
		qDebug("`{}`".format(err));

	@pyqtSlot(str)
	def __photoFailed(self, err):
		qDebug("`{}`".format(err));

	@pyqtSlot(str)
	def __thumbFailed(self, err):
		qDebug("`{}`".format(err));
