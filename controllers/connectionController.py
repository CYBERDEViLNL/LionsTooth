"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, QThreadPool, pyqtSignal, pyqtSlot, qDebug

import keyring

from diaspy.connection import Connection as DiaspyConnection
from diaspy.tagFollowings import TagFollowings as DiaspyTagFollowings

from core.requestsWorker import RequestsWorker
from core.imageResolver import imageResolver
from core.notify import Notify

from models.aspects import AspectsModel
from models.tags import TagsModel

class AvatarModel(QObject):
	"""Represents remote and local avatar paths. Also responsible for fetching
	the avatar from remote location.
	"""
	updated = pyqtSignal();

	def __init__(self, imageResolver):
		QObject.__init__(self);
		self.__imageResolver = imageResolver;
		self.__local = ""; # Local avatar path
		self.__remote = ""; # Remote avatar path

	def clear(self):
		self.__local = "";
		self.__remote = "";
		self.updated.emit();

	def remote(self):
		"""Returns remote path if set.
		"""
		if self.__remote: return self.__remote;

	def local(self):
		"""Returns local avatar path if set.
		"""
		if self.__local: return self.__local;

	@pyqtSlot(str)
	def __fetchFailed(self, err):
		qDebug("Fetch remote avatar failed.");

	@pyqtSlot(object)
	def __fetchSucceeded(self, path):
		if path != self.__local:
			self.__local = path;
			self.updated.emit();

	def fetch(self, remotePath):
		if remotePath != self.__remote:
			self.__remote = remotePath;
			workerId = self.__imageResolver.newJob(remotePath);
			self.__imageResolver.worker(workerId).signals.result.connect(
				self.__fetchSucceeded
			);
			self.__imageResolver.worker(workerId).signals.exception.connect(
				self.__fetchFailed
			);
			self.__imageResolver.startWorker(workerId);

class userDataModel(QObject):
	updated = pyqtSignal();

	def __init__(self, connectionWrapper, imageResolver, threadPool):
		QObject.__init__(self);

		self.__connectionWrapper = connectionWrapper;
		self.__threadPool = threadPool;

		self._avatar = AvatarModel(imageResolver);
		self._aspects = AspectsModel();
		self._tags = TagsModel(connectionWrapper, threadPool);

		self.__connectionWrapper.statusChanged.connect(self.__connectionStatusChanged);

	def __bool__(self):
		if self.__connectionWrapper: return True;
		return False;

	@pyqtSlot(int)
	def __connectionStatusChanged(self, status):
		# On logout
		if not status:
			self._aspects.clear();
			self._tags.clear();
			self._avatar.clear();
		self.updated.emit();

	@pyqtSlot(str)
	def __getUserDataFailed(self, err=""):
		"""Called when getUserData attempt failed.
		"""
		qDebug("[DEBUG] getUserData failed: `{0}`".format(err));

	@pyqtSlot()
	def __getUserDataSucceeded(self):
		"""Called when getUserData is succesfull.
		"""
		qDebug("[DEBUG] Got user data.");
		self._aspects.setAspects(
				self.__connectionWrapper.connection().userdata()['aspects']);
		self._avatar.fetch(self.__connectionWrapper.connection().userdata()['avatar']['medium']);
		self.updated.emit();

	def aspectsModel(self): return self._aspects;
	def tagsModel(self): return self._tags;
	def avatarModel(self): return self._avatar;

	def userData(self):
		if self.__connectionWrapper:
			return self.__connectionWrapper.connection().userdata();
		return {};

	def getUserData(self):
		"""Runs `diaspy.connection.Connection().getUserData()` in a thread.
		"""
		if self.__connectionWrapper:
			worker = RequestsWorker(
						job=self.__connectionWrapper.connection().getUserData);
			worker.signals.exception.connect(self.__getUserDataFailed);
			worker.signals.success.connect(self.__getUserDataSucceeded);
			self.__threadPool.start(worker);
		self._tags.get(); # Fetch followed tags.

class ConnectionWrapper(QObject):
	statusChanged = pyqtSignal(int); # <! 0 signed out, 1 signed in.
	def __init__(self, connection=None):
		QObject.__init__(self);
		self.__connection = connection;
		self._data = {}

	def __bool__(self):
		if self.__connection: return True
		return False

	def connection(self):
		"""Returns connection or None
		"""
		return self.__connection;

	def requestsKwargs(self): return self.data()["request_kwargs"];

	def resetData(self): self.setData({});
	def setData(self, data): self._data = data;
	def data(self): return self._data;

	def setConnection(self, connection, **requestKwargs):
		self.__connection = connection;

	def destroyConnection(self): self.__connection = None;

class ConnectionController(QObject):
	statusChanged = pyqtSignal(int); # <! 0 signed out, 1 signed in.
	def __init__(self, connectionSettings):
		QObject.__init__(self);

		self.connectionWrapper = ConnectionWrapper();
		self.imageResolver = imageResolver(
			"./cache/images/", self.connectionWrapper
		);
		self._threadPool = QThreadPool();
		self.__status = 0;

		self._userModel = userDataModel(self.connectionWrapper,
										self.imageResolver,
										self._threadPool);

		# Settings view and input
		self.__connectionSettings = connectionSettings;
		self.__connectionSettings.requestLogin.connect(self.__connect);
		self.__connectionSettings.requestLogout.connect(self.logout);

		self.statusChanged.connect(self.connectionWrapper.statusChanged);

	def __setConnection(self, pod, username, password, **requestsKwargs):
		"""Creates a new `diaspy.connection.Connection()` instance and sets 
		`self.__connection` to it.
		"""
		self.connectionWrapper.setConnection(DiaspyConnection(
												pod,
												username,
												password,
												**requestsKwargs));
		qDebug("[DEBUG] __setConnection().");

	# Failed
	@pyqtSlot(str)
	def __setConnectionFailed(self, err=""):
		"""Called creating a new `diaspy.connection.Connection()` instance 
		failed.
		"""
		qDebug("[DEBUG] Could not set Connection() object: `{0}`".format(err));
		self.connectionWrapper.resetData();
		self.__connectionSettings.loginFailed();

	@pyqtSlot(str)
	def __loginFailed(self, err=""):
		"""Called when login attempt failed.
		"""
		qDebug("[DEBUG] Login failed: `{0}`".format(err));
		self.__connectionSettings.loginFailed();

	@pyqtSlot(str)
	def __logoutFailed(self, err=""):
		"""Called when logout attempt failed.
		"""
		qDebug("[DEBUG] Logout failed: `{0}`".format(err));
		self.__connectionSettings.logoutFailed();

	# Success
	@pyqtSlot()
	def __setConnectionSucceeded(self): self.login();

	@pyqtSlot()
	def __loginSucceeded(self):
		"""Called when succesfully signed in.
		"""
		Notify.message("Succesfully signed in.");
		qDebug("[DEBUG] Login succeeded.");
		self.__status = 1;
		self.__connectionSettings.loginSucceeded();
		self.statusChanged.emit(self.__status);
		self._userModel.getUserData();

	@pyqtSlot()
	def __logoutSucceeded(self):
		"""Called when succesfully signed out.
		"""
		qDebug("[DEBUG] Logout succeeded.");
		Notify.message("Succesfully signed out.");
		self.__status = 0;
		self.__connectionSettings.logoutSucceeded();
		self.statusChanged.emit(self.__status);
		self.connectionWrapper.destroyConnection();

		self.connection = None;

	def user(self): return self._userModel;

	def setConnection(self, pod, username, password, **requestsKwargs):
		"""Runs `self.__setConnection()` in a thread.
		"""
		worker = RequestsWorker(job=self.__setConnection,
								pod=pod,
								username=username,
								password=password,
								**requestsKwargs);
		worker.signals.exception.connect(self.__setConnectionFailed);
		worker.signals.success.connect(self.__setConnectionSucceeded);
		self._threadPool.start(worker);

	def autoLogin(self): self.__connectionSettings.login();

	def __connect(self):
		data = self.__connectionSettings.data();
		self.connectionWrapper.setData(data); # will be used by ImageResolver
		self.setConnection(data["pod_url"],
						data["username"],
						keyring.get_password("LionsTooth", data['username']),
						 **data["request_kwargs"]);

	def login(self):
		"""Runs `diaspy.connection.Connection().podswitch()` in a thread.
		"""
		worker = RequestsWorker(job=self.connectionWrapper.connection().login);
		worker.signals.exception.connect(self.__loginFailed);
		worker.signals.success.connect(self.__loginSucceeded);
		self._threadPool.start(worker);

	def logout(self):
		"""Runs `diaspy.connection.Connection().logout()` in a thread.
		"""
		worker = RequestsWorker(job=self.connectionWrapper.connection().logout);
		worker.signals.exception.connect(self.__logoutFailed);
		worker.signals.success.connect(self.__logoutSucceeded);
		self._threadPool.start(worker);

