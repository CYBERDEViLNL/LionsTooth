"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QWidget, QShortcut, QScrollArea, QScroller, QStackedWidget, QVBoxLayout, QSizePolicy
from PyQt5.QtGui import QKeySequence
from PyQt5.QtCore import Qt, pyqtSignal, qDebug

from enum import Enum
from core import exceptions

#from pympler import tracker
#from pympler import muppy, summary

class PageProto(QScrollArea):
	def __init__(self, parent=None):
		QScrollArea.__init__(self, parent=parent);

		self.setWidgetResizable(True);

		self.contentWidget = QWidget();
		self.contentWidget.setObjectName("PageContent");

		layout = QVBoxLayout(self.contentWidget);
		layout.setContentsMargins(0, 15, 0, 0);
		layout.setSpacing(3);

		layout.setAlignment(Qt.AlignTop);

		self.contentWidget.setSizePolicy(QSizePolicy.Expanding,
						QSizePolicy.Expanding);

		self.setWidget(self.contentWidget);

		# Add TouchScreen like scrolling (by dragging up or down).
		# TODO This isn't working for all pages, only the first one grabbed.
		self.scroller = QScroller.scroller(self.viewport());
		self.scroller.grabGesture(
			self.viewport(), QScroller.LeftMouseButtonGesture
		);

	def setLayout(self, layout):
		"""Change the layout of the contentWidget
		"""
		self.contentWidget.setLayout(layout);

	def addWidget(self, widget, stretch=0, alignment=Qt.AlignTop):
		"""Add a widget to the contentWidget
		"""
		return self.contentWidget.layout().addWidget(
			widget, stretch, alignment
		);

	def insertWidget(self, index, widget):
		"""Insert a widget on a given index to the contentWidget
		"""
		return self.contentWidget.layout().insertWidget(index, widget);

	def resizeEvent(self, event):
		self.contentWidget.setMinimumWidth(self.viewport().size().width());
		self.contentWidget.setMaximumWidth(self.viewport().size().width());
		QScrollArea.resizeEvent(self, event);

class PageNames(Enum):
	streamPage			= "_streamPage";
	activityPage		= "_activityPage";
	mentionsPage		= "_mentionsPage";
	aspectsPage			= "_aspectsPage";
	tagsPage			= "_tagsPage";
	publicPage			= "_publicPage";
	profilePage			= "_profilePage";
	contactsPage		= "_contactsPage";
	notificationsPage	= "_notificationPage";
	conversationsPage	= "_conversationPage";
	searchPage			= "_searchPage";
	singlePostPage		= "_singlePostPage";
	settingsPage		= "_settingsPage";
	diasporaSettingsPage= "_diasporaSettingsPage";


class PageController(QStackedWidget):
	pageChanged = pyqtSignal(object); # PageNames item
	pageChangedParams = pyqtSignal(object, object);
	def  __init__(self, parent=None):
		QStackedWidget.__init__(self, parent=parent);
		self.__pages = {};
		self.__controllers = {};
		self.__indexMap = {}; #!< key: pageName, value: index
		self.__history = [];
		self.__maxHistory = 32;
		self.__currentHistoryIndex = -1;

		QShortcut(QKeySequence(Qt.SHIFT + Qt.Key_N), self, self.previous);
		QShortcut(QKeySequence(Qt.Key_N), self, self.next);

	def mousePressEvent(self, event):
		button = event.button();
		if button == Qt.XButton1: # back.
			self.previous();
		elif button == Qt.XButton2: # forward.
			self.next();

	def __historyChangedPage(self):
		if self.__history[self.__currentHistoryIndex]["params"]:
			self.pageChangedParams.emit(
					self.__history[self.__currentHistoryIndex]["pageName"],
					self.__history[self.__currentHistoryIndex]["params"]);
		else: self.pageChanged.emit(
					self.__history[self.__currentHistoryIndex]["pageName"]);

	def previous(self):
		if self.__currentHistoryIndex > 0:
			self.__currentHistoryIndex -= 1;
			self.setCurrentPage(**self.__history[self.__currentHistoryIndex]);
			self.__historyChangedPage();

	def next(self):
		historyLenght = len(self.__history);
		if historyLenght and self.__currentHistoryIndex < historyLenght-1:
			self.__currentHistoryIndex += 1;
			self.setCurrentPage(**self.__history[self.__currentHistoryIndex]);
			self.__historyChangedPage();

	def setCurrentPage(self, pageName, params=None, updateHistory=True):
		""" Changes page to page corresponding to pageName.

		TODO params should be passed to a controller not the view
		Possible arguments:
			- tell tagsPage to load data for tag x.
			- tell aspectsPage to load data for aspects [x,y,z].
			- tell searchPage to load content x
			..

		:param pageName: page name obtained from Enum pages
		:type pageName: str
		:param params: params for the page its controller.
		:type params: object (what the page needs.)
		:returns: None
		"""
		if pageName not in self.__indexMap:
			raise exceptions.PageError(
				"Page `{0}` not found in self.__indexMap.".format(pageName)
			);

		# First do the parameters then open it.
		if params:
			if pageName not in self.__controllers:
				raise exceptions.PageError(
					"Page `{0}` not found in self.__pages.".format(pageName)
				);

			if not self.__controllers[pageName]:
				raise exceptions.PageError(
					"Controller for page `{0}` not set.".format(pageName)
				);
			self.__controllers[pageName].setParams(params);

		if updateHistory:
			historyLength = len(self.__history) -1;

			if historyLength == self.__maxHistory: self.__history.pop(0);

			if self.__currentHistoryIndex < historyLength -1:
				if self.__currentHistoryIndex == -1:
					self.__history.clear();
				else:
					for index in range(self.__currentHistoryIndex,
										historyLength):
						self.__history.pop(self.__currentHistoryIndex);
			historyItem = {
				"pageName":pageName,
				"params":params,
				"updateHistory":False
			};
			self.__history.append(historyItem);
			self.__currentHistoryIndex = len(self.__history)-1;

		self.setCurrentIndex(self.__indexMap[pageName]);
		if pageName in self.__controllers and self.__controllers[pageName]:
			# current page widget its content controller
			self.__controllers[pageName].viewOpened();

	def page(self, pageName):
		""" Returns the page widget if pageName found.
		"""
		if pageName in self.__pages: return self.__pages[pageName];

	def pages(self):
		""" Returns the self.__pages dict with as key a member from the
		Enum PageNames and as value the coresponding page widget.
		"""
		return self.__pages;

	def setController(self, pageName, controller):
		if pageName not in self.__controllers:
				raise exceptions.PageError(
				"Page `{0}` not found in self.__controllers.".format(pageName));
		self.__controllers[pageName] = controller;

	def addPage(self, pageName, view, controller=None):
		if pageName not in PageNames:
			raise exceptions.PageError("Page `{0}` not found."
										.format(pageName));

		index = self.addWidget(view);
		self.__pages.update({pageName:view});
		self.__controllers.update({pageName:controller});
		self.__indexMap.update({pageName:index});
