"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, QThreadPool, pyqtSignal, pyqtSlot, qDebug

from diaspy.people import User as DiaspyUser

from core.requestsWorker import RequestsWorker
from controllers.pageController import PageNames
from controllers.postController import PostController

class ProfileController(QObject):
	"""Controlls profile view
	"""
	def __init__(self): QObject.__init__(self);

class HCardController(QObject):
	"""Controlls hover card view
	"""
	def __init__(self): QObject.__init__(self);

class User(QObject):
	"""A diaspora user
	"""
	updated = pyqtSignal();
	def __init__(self, guid, connectionWrapper):
		QObject.__init__(self);

		self._connectionWrapper = connectionWrapper;
		self._guid = guid;
		self._authorData = {};	# author data
		self._user = None;	# diaspy.people.User()

	def guid(self): return self._guid;
	def model(self): return self._user;

	"""
	def data(self, key, default=None):
		if self._user:
			if key in self._user.data: return self._user.data[key];
		elif key in self._authorData: return self._authorData[key];
		return default;
	"""
	def data(self, arg, default=None):
		argType = type(arg);
		if argType == str:
			if self._user:
				if arg in self._user.data: return self._user.data[arg];
			elif arg in self._authorData: return self._authorData[arg];
		elif argType == list:
			history = [];
			result = "";
			if self._user: result = self._user.data;
			else: result = self._authorData;
			for key in arg:
				if key in result:
					result = result[key];
					history.append(key);
				else:
					qDebug("[DEBUG] UserController.data() the following keys "
						"where requested `{0}` but key `{1}` does not exist."
						.format(arg, key)
					);
			return result;
		return default;

	def __initModel(self):
		if self._connectionWrapper:
			self._user = DiaspyUser(
				self._connectionWrapper.connection(),
				guid=self.guid(),
				handle=self.data("diaspora_id", ""),
				fetch=None,
				id=self.data("id", 0),
				data=None
			);
			self._user.fetchguid(fetch_stream=False);
			self.updated.emit();

	def diasporaId(self): return self.data("diaspora_id");

	def avatar(self, size="small"):
		avatar = self.data("avatar");
		if avatar: return avatar[size];

	def _updateAuthorData(self, data):
		""" TODO update self._user if inited
		"""
		for key in data:
			if key in self._authorData:
				if data[key] != self.data(key):
					self._authorData[key] = data[key];
					self.updated.emit();
			else:
				self._authorData[key] = data[key];
				self.updated.emit();

	def addAuthorData(self, data):
		"""Can be `author` data from a post or `people` data.
		"""
		if self._authorData: self._updateAuthorData(data);
		else: self._authorData = data;


class UserController(QObject):
	"""Controlls diaspy.people.User() objects and gives the view controller(s)
	the data is asks for, plus it tells the view controller(s) when their view
	needs a update.
	"""
	def __init__(self, connectionWrapper):
		QObject.__init__(self);

		self._connectionWrapper = connectionWrapper;
		self._users = {}; # <! key: guid, val: User()
		self._diasporaIdMap = {}; # <! key : diaspora_id, val: guid
		#self._threadPool = QThreadPool();

		self._connectionWrapper.statusChanged.connect(
			self.__connectionStatusChanged
		);

	@pyqtSlot(int)
	def __connectionStatusChanged(self, status):
		if not status:
			self._users.clear();
			self._diasporaIdMap.clear();

	def __contains__(self, guid): return guid in self._users;

	def _addUser(self, guid):
		if guid not in self._users:
			self._users.update({guid:User(guid, self._connectionWrapper)});
		else:
			qDebug("[DEBUG] UserController._addUser() guid is already in "
					"self._users"
			);

	def user(self, guid):
		"""Returns user by guid.
		"""
		if guid in self: return self._users[guid];

	def userDiasporaId(self, diasporaId):
		"""Returns user by Diaspora ID.
		"""
		if diasporaId in self._diasporaIdMap:
			return self.user(self._diasporaIdMap[diasporaId]);

	def addUser(self, guid, fetch=False): self._addUser(guid);
