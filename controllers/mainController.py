"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QLabel
from PyQt5.QtCore import QObject, QThreadPool, pyqtSignal, pyqtSlot, qInstallMessageHandler, QtDebugMsg, QtInfoMsg, QtWarningMsg, QtCriticalMsg, QtFatalMsg, qDebug

from core.notify import Notify

from controllers.pageController import PageController, PageNames
from controllers.connectionController import ConnectionController
from controllers.streamController import StreamController, AspectsStreamController, TagsStreamController, ActivityStreamController
from controllers.headerController import HeaderController
from controllers.navigationBarController import NavigationBarController
from controllers.postController import PostsManager
from controllers.userController import UserController
from controllers.profileController import ProfileController

from models.notifications import NotificationsModel

from gui.pages.settings import SettingsPage
from gui.pages.stream import StreamPage
from gui.pages.contacts import ContactsPage
from gui.pages.conversations import ConversationsPage
from gui.pages.notifications import NotificationsPage
from gui.pages.profile import ProfilePage
from gui.pages.search import SearchPage
from gui.pages.singlePost import SinglePostPage
from gui.pages.diasporaSettings import DiasporaSettingsPage
from gui.views.navigationBar import NavigationBarView
from gui.views.header import HeaderView
from gui.views.stream import TagsStreamView

from gui.widgets.notifier import Notifier, Notifications

from gui.mainWindow import MainWindow

from os import getcwd # to strip fullpath from debug/fatal output

class MainController(QObject):
	"""Main controller - Gateway for other controllers
	"""
	themeChangeRequest = pyqtSignal(str);
	def __init__(self, config, themes=[]):
		QObject.__init__(self);
		qInstallMessageHandler(self.qtMessageHandler); # Debug

		self.config = config;

		self.mainWindow = MainWindow();
		self.notifier = Notifier(parent=self.mainWindow);

		"""Application notifications
		"""
		Notify.installMessageHandler(self.notifyMessageHandler);

		"""Resize
		"""
		self.mainWindow.resize(self.config["Appearance"].getint("width", 400),
							self.config["Appearance"].getint("height", 500));
		self.mainWindow.resized.connect(self.mainWindoResized);

		"""Header
		"""
		self.headerView = HeaderView();
		self.headerView.notifyButton().clicked.connect(self.notifier.show);
		self.mainWindow.centralWidget().layout().addWidget(self.headerView);

		"""NavigationBar
		"""
		# Toolbar (TODO create and move to controller)
		self.navigationBarView = NavigationBarView(self.mainWindow);
		self.navigationBarView.setCollapseWidth(32);
		self.navigationBarView.setExpandWidth(200);
		self.navigationBarView.setMinimumHeight(
			self.mainWindow.size().height()
		);
		self.navigationBarView.setMaximumHeight(
			self.mainWindow.size().height()
		);
		self.navigationBarView.show();

		self.mainWindow.centralWidget().layout().setContentsMargins(
			self.navigationBarView.collapseWidth(), 0, 0, 0
		);

		"""Pages
		"""
		self.pageController = PageController(parent=self.mainWindow.centralWidget());

		# Add/init the pages (views)
		self.pageController.addPage(
			PageNames.streamPage, StreamPage("Stream")
		);

		self.pageController.addPage(
			PageNames.activityPage, StreamPage("Activity")
		);

		self.pageController.addPage(
			PageNames.mentionsPage, StreamPage("Mentions")
		);

		self.pageController.addPage(
			PageNames.aspectsPage, StreamPage("Aspects")
		);

		self.pageController.addPage(
			PageNames.tagsPage, StreamPage("Tags", view=TagsStreamView)
		);

		self.pageController.addPage(
			PageNames.publicPage, StreamPage("Public")
		);

		self.pageController.addPage(
			PageNames.profilePage, ProfilePage()
		);

		self.pageController.addPage(
			PageNames.contactsPage, ContactsPage()
		);

		self.pageController.addPage(
			PageNames.notificationsPage, NotificationsPage()
		);

		self.pageController.addPage(
			PageNames.conversationsPage, ConversationsPage()
		);

		self.pageController.addPage(
			PageNames.searchPage, SearchPage()
		);

		self.pageController.addPage(
			PageNames.singlePostPage, SinglePostPage()
		);

		self.pageController.addPage(
			PageNames.diasporaSettingsPage, DiasporaSettingsPage()
		);

		self.pageController.addPage(
			PageNames.settingsPage, SettingsPage(config=self.config)
		);


		# requestSaveConfig from settingsPage
		self.pageController.page(PageNames.settingsPage).requestSaveConfig.connect(self.config.save);

		# Set themes on settings page
		self.pageController.page(PageNames.settingsPage).generalSettings.setThemes(themes);
		self.pageController.page(PageNames.settingsPage).generalSettings.themeChangeRequest.connect(self.themeChangeRequest)


		self.mainWindow.centralWidget().layout().addWidget(self.pageController);


		"""Connection Controller
			Controls Connection Settings View and a
			`diaspy.connection.Connection()` instance when logged in.
		"""
		self.connectionController = ConnectionController(self.pageController.page(PageNames.settingsPage).connectionSettings);

		"""NavigationBar controller
		"""
		self.navigationBarController = NavigationBarController(self.navigationBarView, self.connectionController.user().aspectsModel(), self.connectionController.user().tagsModel());
		self.navigationBarController.pageRequest.connect(self.pageController.setCurrentPage);
		self.navigationBarController.pageRequestParams.connect(self.pageController.setCurrentPage);
		self.pageController.pageChanged.connect(self.navigationBarController.pageChanged);
		self.pageController.pageChangedParams.connect(self.navigationBarController.pageChanged);

		"""Notifications (Diaspora)
		"""
		self.notificationsModel = NotificationsModel(self.connectionController.connectionWrapper);
		self.notifications = Notifications(self.notificationsModel, self.connectionController.imageResolver, parent=self.mainWindow);
		self.notifications.anchorClicked.connect(self._routeAnchor);
		self.headerView.notificationsButton().clicked.connect(self.notifications.show);

		"""Header controller
		"""
		self.headerController = HeaderController(self.connectionController.user(), self.headerView, self.notificationsModel, self.connectionController.imageResolver);


		"""UserController
		"""
		self.userController = UserController(self.connectionController.connectionWrapper);

		"""Posts manager
		"""
		self.postsManager = PostsManager(self.connectionController.connectionWrapper, self.connectionController.imageResolver, self.userController, self.connectionController.user());
		self.postsManager.anchorClicked.connect(self._routeAnchor);

		"""Stream controllers
		"""
		self.streamStreamController = StreamController(
					self.connectionController.connectionWrapper,
					self.postsManager,
					self.pageController.page(PageNames.streamPage).streamView,
					streamModelName="Stream",
					userModel=self.connectionController.user());
		self.pageController.setController(PageNames.streamPage, self.streamStreamController);

		self.mentionsStreamController = StreamController(
					self.connectionController.connectionWrapper,
					self.postsManager,
					self.pageController.page(PageNames.mentionsPage).streamView,
					streamModelName="Mentions");
		self.pageController.setController(PageNames.mentionsPage, self.mentionsStreamController);

		self.publicStreamController = StreamController(
					self.connectionController.connectionWrapper,
					self.postsManager,
					self.pageController.page(PageNames.publicPage).streamView,
					streamModelName="Stream");
		self.pageController.setController(PageNames.publicPage, self.publicStreamController);

		self.aspectsStreamController = AspectsStreamController(
					self.connectionController.connectionWrapper,
					self.postsManager,
					self.pageController.page(PageNames.aspectsPage).streamView);
		self.pageController.setController(PageNames.aspectsPage, self.aspectsStreamController);

		self.activityStreamController = ActivityStreamController(
					self.connectionController.connectionWrapper,
					self.postsManager,
					self.pageController.page(PageNames.activityPage).streamView);
		self.pageController.setController(PageNames.activityPage, self.activityStreamController);

		self.tagsStreamController = TagsStreamController(
					self.connectionController.connectionWrapper,
					self.postsManager,
					self.connectionController.user().tagsModel(),
					self.pageController.page(PageNames.tagsPage).streamView);
		self.pageController.setController(PageNames.tagsPage, self.tagsStreamController);

		"""SinglePost controller
		"""

		"""Profile controller
		"""
		self.profileController = ProfileController(
								self.connectionController.connectionWrapper,
								self.postsManager,
								self.userController,
								self.pageController.page(PageNames.profilePage).profileView,
								self.pageController.page(PageNames.profilePage).streamView);
		self.pageController.setController(PageNames.profilePage,
										self.profileController);
		"""Contacts controller
		"""

		"""Conversations controller
		"""

		"""Contacts controller
		"""

		"""Notifications controller
		"""

		self.pageController.setCurrentPage(PageNames.settingsPage);

		if ("Connection" in self.config and
			self.config["Connection"].get("auto_login", "False") == "True"):
			self.connectionController.autoLogin();
		self.mainWindow.show();

		Notify.message("""<h3>Please be aware this project still is in a very early
stage, lacks features and will contain various bugs!</h3><br/><br/>Pod settings 
not verified yet. If this is your first time running LionsTooth you'd have to
 set a pod, username and password in the settings tab. Press connect after, if
  you want to connect. Have fun ;-)""");


	def _routeAnchor(self, anchor):
		qDebug(anchor);
		if len(anchor) > 6 and anchor[0:5] == "#tag:":
			tag = anchor[5:];
			self.pageController.setCurrentPage(PageNames.tagsPage, params=tag);
			# Let the navigation bar know
			self.navigationBarController.pageChangedParams(PageNames.tagsPage, tag);
		elif len(anchor) > 7 and anchor[0:7] == "/posts/":
			postId = anchor[7:];
			qDebug("Controller for singlePostPage not set yet.");
			self.pageController.setCurrentPage(PageNames.singlePostPage, params={"id":postId});
		elif len(anchor) > 18 and anchor[0:13] == "#diaspora_id:":
			diasporaId = anchor[13:];
			self.pageController.setCurrentPage(PageNames.profilePage, params={"diasporaId":diasporaId});
			# Let the navigation bar know
			self.navigationBarController.pageChanged(PageNames.profilePage);
		elif len(anchor) in range(40,255) and anchor[0:8] == "/people/":
			guid = anchor[8:];
			self.pageController.setCurrentPage(PageNames.profilePage, params={"guid":guid});


	def notifyMessageHandler(self, msg, type):
		if type == Notify.Type.Message: self.notifier.message("INFO", msg);
		elif type == Notify.Type.Warning: self.notifier.warning("WARNING", msg);
		elif type == Notify.Type.Error: self.notifier.error("ERROR", msg);

	def qtMessageHandler(self, type, context, msg):
		if type == QtDebugMsg:
			filepath = context.file[len(getcwd()):] # strip fullpath
			print("[DEBUG] {0}:{1} {2}() `{3}`".format(filepath, context.line, context.function, msg));
		else:
			print("[Qt-MSG] {}".format(msg));

	@pyqtSlot(int, int)
	def mainWindoResized(self, width, height):
		# Let the (vertical) navigation bar fit the height.
		self.navigationBarView.setMinimumHeight(self.mainWindow.size().height());
		self.navigationBarView.setMaximumHeight(self.mainWindow.size().height());

		self.config["Appearance"]["width"] = str(self.mainWindow.size().width());
		self.config["Appearance"]["height"] = str(self.mainWindow.size().height());
		self.config.save(); # TODO do this only on request or on exit.
