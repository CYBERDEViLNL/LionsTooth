"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, QThreadPool, pyqtSignal, pyqtSlot, qDebug

from diaspy import streams as DiaspyStreams

from core.requestsWorker import RequestsWorker
from controllers.pageController import PageNames
from controllers.postController import PostController

from gui.views.post import PostView

class ProfileController(QObject):
	"""Controller for profileView
	"""
	def __init__(self, connectionWrapper, postsManager, userController,
				profileView, streamView):
		QObject.__init__(self);

		self._connectionWrapper = connectionWrapper;
		self._postsManager = postsManager;
		self._userController = userController;

		self._profileView = profileView;
		self._streamView = streamView;

	def viewOpened(self):
		print("profile opened")

	def setParams(self, params):
		print("profile setParams {}".format(params));
		#if self._connectionWrapper and not ...
