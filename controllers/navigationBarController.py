"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, QThreadPool, pyqtSignal, pyqtSlot, qDebug

from core.translator import Translator
def translate(string): return Translator.translate(string, "navigationBar");
_ = translate;

from controllers.pageController import PageNames
from gui.views.navigationBar import NavigationBarAction, NavigationBarAspectsAction, NavigationBarTagsAction


class NavigationBarController(QObject):
	"""
	"""
	pageRequest = pyqtSignal(object);# <! Page name from PageNames
	pageRequestParams = pyqtSignal(object, object);	""" <! Page name from
													PageNames, params object
													whatever it needs to be.
													Only groups can have params
													"""
	def __init__(self, navigationBarView, aspectsModel, tagsModel):
		QObject.__init__(self);

		self.__navigationBarView = navigationBarView;
		self.__aspectsModel = aspectsModel;
		self.__tagsModel = tagsModel;
		

		# Connect signals
		tagsModel.added.connect(self.__addTag);
		tagsModel.deleted.connect(self.__tagDeleted);
		tagsModel.working.connect(self.__disableTagInteractions);
		tagsModel.finished.connect(self.__enableTagInteractions);
		self.__navigationBarView.pageRequest.connect(self.pageRequest);
		self.__navigationBarView.pageRequestParams.connect(self.pageRequestParams);
		self.__aspectsModel.updated.connect(self.__updateAspectsView);
		self.__tagsModel.updated.connect(self.__updateTagsView);

		"""From here we add stuff.. """
		self.__navigationBarView.newAction(
			PageNames.streamPage, _("Stream"), ":/icons/32x32/stream"
		);
		self.__navigationBarView.newAction(
			PageNames.mentionsPage, _("@Me"), ":/icons/32x32/mention"
		);

		"""NavigationBar Actions
		"""
		# Activity Group
		activityGroup = self.__navigationBarView.newGroup(
			PageNames.activityPage, _("Activity"), ":/icons/32x32/activity"
		);
		activityGroup.hideActions();
		likedAction = NavigationBarAction(name=PageNames.activityPage,
			text=_("Liked"), params="liked", parent=activityGroup
		);

		commentedAction = NavigationBarAction(name=PageNames.activityPage,
							text=_("Commented"), params="commented", parent=activityGroup);
		activityGroup.addAction(likedAction);
		activityGroup.addAction(commentedAction);

		# Aspects Group
		aspectsGroup = self.__navigationBarView.newGroup(
			PageNames.aspectsPage, _("Aspects"), ":/icons/32x32/aspects"
		);
		aspectsGroup.hideActions();
		self.aspectsAction = NavigationBarAspectsAction(
							name=PageNames.aspectsPage, parent=aspectsGroup);
		aspectsGroup.addAction(self.aspectsAction);
		aspectsGroup.setParams(self.aspectsAction.selectedIds);

		# Tags Group
		tagsGroup = self.__navigationBarView.newGroup(
			PageNames.tagsPage, _("Following Tags"), ":/icons/32x32/tag"
		);
		tagsGroup.hideActions();
		self.tagsAction = NavigationBarTagsAction(
							name=PageNames.tagsPage, parent=tagsGroup);
		tagsGroup.addAction(self.tagsAction);

		# Public Action
		self.__navigationBarView.newAction(
			PageNames.publicPage, _("Public"), ":/icons/32x32/public"
		);

		# Profile Action
		self.__navigationBarView.newAction(
			PageNames.profilePage, _("Profile"), ":/icons/32x32/profile"
		);

		# Contacts Action
		self.__navigationBarView.newAction(
			PageNames.contactsPage, _("Contacts"), ":/icons/32x32/contacts"
		);

		# Notifications Action
		self.__navigationBarView.newAction(
			PageNames.notificationsPage, _("Notifications"),
			":/icons/32x32/notification"
		);

		# Conversations Action
		self.__navigationBarView.newAction(
			PageNames.conversationsPage, _("Conversations"),
			":/icons/32x32/message"
		);

		# Diaspora Settings Action
		self.__navigationBarView.newAction(
			PageNames.diasporaSettingsPage, _("D* Settings"),
			":/icons/32x32/dsettings"
		);

		# Settings Action
		self.__navigationBarView.newAction(
			PageNames.settingsPage, _("Settings"), ":/icons/32x32/settings"
		);

	@pyqtSlot(object)
	def pageChanged(self, pageName):
		for action in self.__navigationBarView.actions():
			if action.name() == pageName:
				action.setActive();
				return;
		for group in self.__navigationBarView.groups():
			if group.name() == pageName: group.setActive();

	@pyqtSlot(object, object)
	def pageChangedParams(self, pageName, params):
		for group in self.__navigationBarView.groups():
			if group.name() == pageName: group.setActive(params);

	@pyqtSlot()
	def __enableTagInteractions(self): self.tagsAction.enableInteractions();

	@pyqtSlot()
	def __disableTagInteractions(self): self.tagsAction.disableInteractions();

	@pyqtSlot()
	def __deleteTag(self):
		qDebug("self.sender().name(): `{}`".format(self.sender().name()));
		self.__tagsModel.deleteTag(self.sender().name());

	def __updateAspectsView(self):
		if self.__aspectsModel:
			self.aspectsAction.setAspects(list(self.__aspectsModel));
		else: self.aspectsAction.reset();

	@pyqtSlot(int, str, int)
	def __addTag(self, id, name, count):
		tagItem = self.tagsAction.addTag(id, name, count);
		tagItem.deleteMe.connect(self.__deleteTag);

	@pyqtSlot(int)
	def __tagDeleted(self, index):
		self.tagsAction.removeTag(index);

	def __updateTagsView(self):
		"""Clear the view and adds tags.
		"""
		if not self.__tagsModel: self.tagsAction.clearTags();
		else:
			for tagData in self.__tagsModel:
				self.__addTag(tagData.id(), tagData.name(), tagData.count());
