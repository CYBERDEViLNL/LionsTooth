"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, QThreadPool, pyqtSignal, pyqtSlot, qDebug, Qt

from diaspy import streams as DiaspyStreams

from core.requestsWorker import RequestsWorker
from controllers.pageController import PageNames
from controllers.postController import PostController

from gui.views.post import PostView

class StreamController(QObject):
	"""Controller for streamView
	"""
	streamNameMap = {
		"Stream"		: DiaspyStreams.Stream,
		"Activity"		: DiaspyStreams.Activity,
		"Liked"			: DiaspyStreams.Liked,
		"Commented"		: DiaspyStreams.Commented,
		"Mentions"		: DiaspyStreams.Mentions,
		"Aspects"		: DiaspyStreams.Aspects,
		"FollowedTags"	: DiaspyStreams.FollowedTags,
		"Tag"			: DiaspyStreams.Tag,
		"Public"		: DiaspyStreams.Public
	};
	def __init__(self, connectionWrapper, postsManager, streamView,
						streamModelName="Stream", userModel=None):
		QObject.__init__(self);

		self._connectionWrapper = connectionWrapper;
		self.__postsManager = postsManager;
		self._streamName = streamModelName
		self._stream = None;
		self._streamView = streamView;
		self._threadPool = QThreadPool();
		self.__guids = [];
		self.__workers = {};

		self._userModel = userModel;

		self._connectionWrapper.statusChanged.connect(
			self.__connectionStatusChanged
		);

		self._streamView.requestMorePosts.connect(self.moreModel);
		self._streamView.requestNewPosts.connect(self.updateModel);

	def __connectionStatusChanged(self, status):
		"""Adds publisher on sign in
		Removes publisher on sign out
		"""
		if status:
			if self._userModel != None:
				self._streamView.addPublisher(self._userModel);
				self._streamView.publisher().publishRequest.connect(
					self.publish
				);
				self._streamView.publisher().photoUploadRequest.connect(
					self.__photoUploadRequest
				);
				self._streamView.publisher().photoDeleteRequest.connect(
					self.__photoDeleteRequest
				);
		else:
			if self.__workers:
				for name in self.__workers:
					try:self.__workers[name].signals.success.disconnect();
					except TypeError: pass; # signal not connected.
					try:self.__workers[name].signals.exception.disconnect();
					except TypeError: pass; # signal not connected.
					try:self.__workers[name].signals.result.disconnect();
					except TypeError: pass; # signal not connected.
					try: self.__workers[name].signals.finished.disconnect();
					except TypeError: pass; # signal not connected.
				self.__workers.clear();
			if self._streamView.publisher(): self._streamView.deletePublisher();
			# clear stream
			self._stream = None;
			self._streamView.clearPostViews();
			self._streamView.stopTopLoading();

	@pyqtSlot(str)
	def __photoDeleteRequestFailed(self, err): qDebug(err);

	@pyqtSlot(int)
	def __photoDeleteRequestSucceeded(self, photoId):
		self._streamView.publisher().photoDeleted(photoId);

	@pyqtSlot(int)
	def __photoDeleteRequest(self, id):
		worker = RequestsWorker(self._stream.deletephoto,
									id=id);
		worker.signals.exception.connect(self.__photoDeleteRequestFailed);
		worker.signals.success.connect(
			lambda photoId=id: self.__photoDeleteRequestSucceeded(photoId)
		);
		self._threadPool.start(worker);

	@pyqtSlot(str)
	def __photoUploadRequestFailed(self, err): qDebug(err);

	@pyqtSlot(object)
	def __photoUploadRequestSucceeded(self, photoId, filepath):
		self._streamView.publisher().photoUploaded(photoId, filepath);

	@pyqtSlot(str)
	def __photoUploadRequest(self, filepath):
		aspects = [];
		for aspectId in self._streamView.publisher().selectedAspects():
			aspects.append({"id":aspectId})
		worker = RequestsWorker(self._stream.photoupload,
									filename=filepath,
									aspects=aspects);
		worker.signals.exception.connect(self.__photoUploadRequestFailed);
		worker.signals.result.connect(
			lambda photoId, filepath=filepath:
				self.__photoUploadRequestSucceeded(photoId, filepath)
		);
		self._threadPool.start(worker);

	@pyqtSlot(str)
	def __publishFailed(self, err):
		qWarning("Failed to publish post: {}".format(err));
		self._streamView.publisher().published(failed=True);

	@pyqtSlot(object)
	def __publishSucceeded(self, diaspyPost):
		self._streamView.publisher().published();
		self.__postsManager.addPost(diaspyPost);
		self.__addNewPosts();

	def publish(self):
		text = self._streamView.publisher().markdownText();
		aspect_ids = self._streamView.publisher().selectedAspects();
		photos = self._streamView.publisher().uploadedPhotoIds();
		poll_question, poll_answers = self._streamView.publisher().pollData();
		if self._stream and not self._threadPool.activeThreadCount():
			worker = RequestsWorker(
				self._stream.post,
				text=text,
				aspect_ids=aspect_ids,
				photos=photos,
				poll_question=poll_question,
				poll_answers=poll_answers,
				provider_display_name="LionsTooth - development"
			);
			worker.signals.exception.connect(self.__publishFailed);
			worker.signals.result.connect(self.__publishSucceeded);

			self._threadPool.start(worker);
		else:
			qDebug("Please wait until stream finished loading.");

	def __getModel(self, *args, **kwargs):
		if self._connectionWrapper:
			self._stream = self.streamNameMap[self._streamName](
				self._connectionWrapper.connection(), *args, **kwargs
			);

	def __moreModel(self):
		if self._connectionWrapper and self._stream: self._stream.more();

	def __updateModel(self):
		if self._connectionWrapper and self._stream: self._stream.update();

	@pyqtSlot(str)
	def __updateModelFailed(self, err):
		self._streamView.stopTopLoading();

	@pyqtSlot()
	def __updateModelSucceeded(self):
		self._streamView.stopTopLoading();
		guids = self.__postsManager.addStream(self._stream);
		self.__addNewPosts();

	@pyqtSlot(str)
	def __workerDone(self, name):
		self.__workers.pop(name);

	def updateModel(self):
		#if not self._threadPool.activeThreadCount():
		if "update" not in self.__workers:
			worker = RequestsWorker(self.__updateModel);
			worker.signals.exception.connect(self.__updateModelFailed);
			worker.signals.success.connect(self.__updateModelSucceeded);
			worker.signals.finished.connect(
				lambda name="update": self.__workerDone(name)
			);

			self.__workers.update({"update":worker});

			self._streamView.startTopLoading();

			self._threadPool.start(worker);

	@pyqtSlot(str)
	def __moreModelFailed(self, err):
		self._streamView.stopBottomLoading();

	@pyqtSlot()
	def __moreModelSucceeded(self):
		self._streamView.stopBottomLoading();
		guids = self.__postsManager.addStream(self._stream);
		self.__addNewPosts();

	def moreModel(self):
		#if not self._threadPool.activeThreadCount():
		if "more" not in self.__workers:
			worker = RequestsWorker(self.__moreModel);
			worker.signals.exception.connect(self.__moreModelFailed);
			worker.signals.success.connect(self.__moreModelSucceeded);
			worker.signals.finished.connect(
				lambda name="more": self.__workerDone(name)
			);

			self.__workers.update({"more":worker});

			self._streamView.startBottomLoading();

			self._threadPool.start(worker);

	@pyqtSlot(str)
	def __postViewDestroyed(self, guid):
		self.__guids.pop(self.__guids.index(guid));
		if self._stream:
			# we don't want to re-add it, it's deleted with a reason.
			self._stream.deletePostGuid(guid);

	def __addNewPosts(self):
		if self._stream:
			for index, post in enumerate(self._stream):
				if post.guid not in self.__guids:
					self.__guids.append(post.guid);

					reshareKwargs = {};
					hasPhotos = self.__postsManager.postController(post.guid).hasPhotos();
					ownPost = self.__postsManager.postController(post.guid).ownPost();
					if self.__postsManager.postController(post.guid).isReshare():
						reshareController = self.__postsManager.postController(post.guid).reshareController();
						if reshareController.hasPhotos:
							reshareKwargs.update({"hasPhotos":True});
					postView = PostView(
						isReshare=self.__postsManager.postController(post.guid).isReshare(),
						hasPhotos=hasPhotos,
						ownPost=ownPost,
						reshareKwargs=reshareKwargs,
						parent=self._streamView
					);
					self.__postsManager.postController(post.guid).addView(
						postView
					);

					postView.destroyed.connect(
						lambda nullptr, guid=post.guid:
							self.__postViewDestroyed(guid)
					);

					self._streamView.addPostView(postView, index);

	@pyqtSlot(str)
	def __getModelFailed(self, err=""):
		qDebug(
			"StreamController `{0}` __getModel() F411: `{1}`"
			.format(self._streamName, err)
		);
		self._streamView.stopTopLoading();

	@pyqtSlot()
	def _getModelSucceeded(self):
		qDebug(
			"StreamController `{0}` __getModel() Success"
			.format(self._streamName)
		);
		self._streamView.stopTopLoading();
		guids = self.__postsManager.addStream(self._stream);
		self.__addNewPosts();

	@pyqtSlot()
	def viewOpened(self):
		if self._connectionWrapper and not self._stream:
			self.getModel(); # init the stream


	def setParams(self, params):
		"""..
		"""
		qDebug(
			"StreamController `{0}` setParams() `{1}`"
			.format(self._streamName, params)
		);

	def modelName(self): return self._streamName;

	def setNewModelName(self, streamName):
		if streamName in self.streamNameMap: self._streamName = streamName;
		else:
			qDebug("StreamController `{0}` setNewModelName() tried to"
				"set `{1}` but it isn't in self.streamNameMap"
				.format(self._streamName, streamName)
			);

	def getModel(self, *args, **kwargs):
		"""Runs `self.__getModel()` in a thread.
		"""
		#if not self._threadPool.activeThreadCount():
		if "get" not in self.__workers:
			worker = RequestsWorker(self.__getModel, *args, **kwargs);
			worker.signals.exception.connect(self.__getModelFailed);
			worker.signals.success.connect(self._getModelSucceeded);
			worker.signals.finished.connect(
				lambda name="get": self.__workerDone(name)
			);

			self.__workers.update({"get":worker});

			self._streamView.clearPostViews();
			self._stream = None;
			#self.__guids = [];
			self._streamView.startTopLoading();

			self._threadPool.start(worker);


class ActivityStreamController(StreamController):
	def __init__(self, connectionWrapper, postsManager, streamView):
		StreamController.__init__(self, connectionWrapper, postsManager,
												streamView, "Activity");
		self.__paramsPending = False;

	def setParams(self, params):
		self.__paramsPending = True;
		if (params == "liked" and
				(self.modelName() != "Liked" or not self._stream)):
			self.setNewModelName("Liked");
			self.getModel();
			#self._streamView.setTitle("Activity - Liked");
		elif (params == "commented" and
				(self.modelName() != "Commented" or not self._stream)):
			self.setNewModelName("Commented");
			self.getModel();
			#self._streamView.setTitle("Activity - Commented");

	@pyqtSlot()
	def viewOpened(self):
		if (self._connectionWrapper
				and not self.__paramsPending
				and self._streamName != "Activity"):
			self.setNewModelName("Activity");
			#self._streamView.setTitle("Activity");
			self.getModel();
		self.__paramsPending = False;
		StreamController.viewOpened(self);


class AspectsStreamController(StreamController):
	def __init__(self, connectionWrapper, postsManager, streamView):
		StreamController.__init__(self, connectionWrapper, postsManager,
												streamView, "Aspects");

	@pyqtSlot()
	def __filterSucceeded(self):
		qDebug("AspectsStreamController `{0}` __filterSucceeded()"
			.format(self._streamName)
		);
		self._streamView.stopTopLoading();
		self._getModelSucceeded();

	@pyqtSlot(str)
	def __filterFailed(self, err):
		qDebug("AspectsStreamController `{0}` __filterFailed() `{1}`"
			.format(self._streamName, err)
		);
		self._streamView.stopTopLoading();

	def __filter(self, aspectList):
		if self._connectionWrapper: self._stream.filter(aspectList);

	def filter(self, aspectList):
		if not self._threadPool.activeThreadCount():
			worker = RequestsWorker(self.__filter, aspectList=aspectList);
			worker.signals.exception.connect(self.__filterFailed);
			worker.signals.success.connect(self.__filterSucceeded);

			self._streamView.clearPostViews();
			self._streamView.startTopLoading();

			self._threadPool.start(worker);

	def setParams(self, params):
		self.filter(params);
		qDebug("AspectsStreamController `{0}` setParams() `{1}`"
				.format(self._streamName, params)
		);


class TagsStreamController(StreamController):
	def __init__(self, connectionWrapper, postsManager, tagsModel, tagsStreamView):
		StreamController.__init__(self, connectionWrapper, postsManager,
			tagsStreamView, "FollowedTags"
		);

		self._streamView.followButtonClicked.connect(self.__followButtonClicked);

		self.__paramsPending = False;
		self.__currentTagName = "";
		self.__tagsModel = tagsModel;
		self.__tagsModel.deleted.connect(self.__modelChanged);
		self.__tagsModel.added.connect(self.__modelChanged);
		self.__tagsModel.working.connect(self.__disableTagInteractions);
		self.__tagsModel.finished.connect(self.__enableTagInteractions);

	@pyqtSlot()
	def __enableTagInteractions(self): self._streamView.enableInteractions();

	@pyqtSlot()
	def __disableTagInteractions(self): self._streamView.disableInteractions();

	@pyqtSlot()
	def __modelChanged(self):
		if self._streamName == "Tag":
			self.__setViewHeader(self.__currentTagName);

	def __setViewHeader(self, tagName):
		"""set button to Following or Follow
		"""
		if self.__tagsModel.has(tagName):
			self._streamView.setTag(tagName, True);
		else: self._streamView.setTag(tagName);

	@pyqtSlot()
	def __setTagSucceeded(self):
		qDebug("TagsStreamController `{0}` __filterSucceeded()"
			.format(self._streamName)
		);
		self._streamView.stopTopLoading();
		self._getModelSucceeded();

	@pyqtSlot(str)
	def __setTagFailed(self, err):
		qDebug("TagsStreamController `{0}` __filterFailed() `{1}`"
			.format(self._streamName, err)
		);
		self._streamView.stopTopLoading();

	def __followButtonClicked(self):
		if self.__currentTagName:
			if self.__tagsModel.has(self.__currentTagName): self.__deleteTag();
			else: self.__followTag();
		else:
			qDebug("TagsStreamController `{0}` __followButtonClicked()"
				" tried to follow/delete a empty tag"
				.format(self._streamName)
			);

	def __followTag(self):
		self.__tagsModel.follow(self.__currentTagName);

	def __deleteTag(self):
		self.__tagsModel.deleteTag(self.__currentTagName);

	@pyqtSlot()
	def viewOpened(self):
		if (self._connectionWrapper
			and not self.__paramsPending
			and self._streamName != "FollowedTags"):
			self.setNewModelName("FollowedTags");
			self.__currentTagName = "";
			self.getModel();
			self._streamView.setTag();
			if self._connectionWrapper and not self._stream: self.getModel();
		self.__paramsPending = False;

	def setParams(self, tagName):
		""" TODO we have to wait on all threads that try to update old stuff
		before deleting it.
		"""
		self.__paramsPending = True;
		if self.__currentTagName != tagName:
			self.__currentTagName = tagName;
			self.setNewModelName("Tag");
			self.getModel(tagName);
			self.__setViewHeader(tagName);
		qDebug("TagsStreamController `{0}` setParams() `{1}`"
										.format(self._streamName, tagName));
