"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, QThreadPool, pyqtSignal, pyqtSlot, qDebug

from urllib.parse import urlparse
from os import path, makedirs
import hashlib
import requests
from time import sleep
from requests import ConnectionError
from requests.exceptions import SSLError

from core.requestsWorker import RequestsWorker
from core.exceptions import ImageResolveError

class imageResolver(QObject):
	"""Two instances of this will be created, namely a avatar instance and a
	post content alike instance.
	"""
	gotImage = pyqtSignal(str); # <! image path
	def __init__(self, path, connectionWrapper, **requestKwargs):
		QObject.__init__(self);

		self.__path = path;

		self._requestsSession = requests.Session();
		self._requestKwargs = requestKwargs;
		self.__connectionWrapper = connectionWrapper;

		# 10MB, TODO add option in settings to change this value.
		self.__maxSize = 10485760;

		self.__supportedTypes = ['image/png','image/jpeg','image/gif'];
		self.__threadPool = QThreadPool();
		self.__workers = {};
		self.__hashCue = [];

		self.__connectionWrapper.statusChanged.connect(
			self.__connectionStatusChanged
		);

	def __bool__(self):
		if self.__connectionWrapper: return True;
		return False;

	@pyqtSlot(int)
	def __connectionStatusChanged(self, status):
		if status:
			self.__requestKwargs = self.__connectionWrapper.requestsKwargs();

	@pyqtSlot()
	def __workerFinished(self):
		if self.sender().id() in self.__workers:
			self.__workers.pop(self.sender().id());

	def __setCacheFiles(self): pass;

	def __fetchImage(self, parsedUrl, filePath):
		# Create image url
		imgUrl = "";
		scheme = "http"; # used for requestsKwargs
		if not parsedUrl.scheme:
			imgUrl += "https://";
			scheme = "https";
		else:
			imgUrl += parsedUrl.scheme + "://";
			scheme = parsedUrl.scheme;
		if not parsedUrl.netloc:
			pod = urlparse(self.__connectionWrapper.connection().pod);
			imgUrl += pod.netloc + "/";
		else: imgUrl += parsedUrl.netloc;

		if not parsedUrl.path:
			qDebug("No path..");
		else: imgUrl += parsedUrl.path;

		# Get info about the image by doing a HEAD request.
		# NOTE: Camo doesn't support HEAD
		if not self.__connectionWrapper.connection().camo():
			# Check content-type and content-size
			try:
				imageHeadResponse = self._requestsSession.head(imgUrl, **self.__requestKwargs);
			except Exception as err:
				raise(ImageResolveError("Could not fetch image headers (Camo "
										"Disabled){0}".format(err)));

			if "Content-Type" not in imageHeadResponse.headers:
				raise(ImageResolveError("Could not get content-type for "
										"image: `{0}`".format(imgUrl)));

			if (imageHeadResponse.headers["Content-Type"].lower() not in
													self.__supportedTypes):
				raise(ImageResolveError("Header response: unsupported image type: `{0}`"
						.format(imageHeadResponse.headers["Content-Type"])));

			if "Content-Length" not in imageHeadResponse.headers:
				raise(ImageResolveError("Header response: Could not get content-length for "
										"image: `{0}`".format(imgUrl)));

			if int(imageHeadResponse.headers["Content-Length"]) > self.__maxSize:
				raise(ImageResolveError("Header response: Requested image is bigger then the"
										" maximum image size: `{0}`"
						.format(imageHeadResponse.headers['Content-Length'])));

		# Get image data
		response = self._requestsSession.get(imgUrl, **self.__requestKwargs);

		# Check if image
		if "Content-Type" not in response.headers:
			raise(ImageResolveError(
				"Image response: Could not get content-type for "
				"image: `{0}`".format(imgUrl))
			);
		if (response.headers["Content-Type"].lower() not in
		self.__supportedTypes):
			raise(ImageResolveError(
				"Image response: unsupported image type: `{0}`"
				.format(response.headers["Content-Type"]))
			);

		# Check if dir path exists
		dirPath = "/".join(filePath.split("/")[0:-1]);
		# If dir doesn't exist, create it.
		if not path.isdir(dirPath): makedirs(dirPath);

		# Write image
		imgFile = open(filePath, "wb");
		imgFile.write(response.content);
		imgFile.close();

		return filePath;

	def __localize(self, url):
		parsedUrl = urlparse(url);
		hashObject = hashlib.md5(parsedUrl.geturl().encode("utf-8"));
		hashStr = hashObject.hexdigest();
		del hashObject;
		filePath = "{}/{}/{}".format(self.__path, parsedUrl.netloc, hashStr);
		if path.exists(filePath): return filePath;
		if hashStr not in self.__hashCue:
			self.__hashCue.append(hashStr);
			try:
				self.__fetchImage(parsedUrl, filePath);
			except Exception as err:
				self.__hashCue.pop(self.__hashCue.index(hashStr));
				raise(ImageResolveError(err));
			self.__hashCue.pop(self.__hashCue.index(hashStr));
		else:
			count = 0;
			timeout = self.__requestKwargs.get("timeout", 30);
			while hashStr in self.__hashCue:
				sleep(1);
				count += 1;
				if count == timeout: return;
		return filePath;

	@pyqtSlot(str)
	def __failed(self, err):
		qDebug("[DEBUG] imageResolver().__failed() `{0}`".format(err));

	def newJob(self, url):
		"""
		"""
		worker = RequestsWorker(self.__localize, url);
		self.__workers.update({worker.id():worker});
		worker.signals.exception.connect(self.__failed);
		return worker.id();

	def worker(self, uuid):
		"""Returns worker to given uuid.
		"""
		if uuid in self.__workers: return self.__workers[uuid];
		else:
			qDebug("[DEBUG] ImageResolver.worker() did receive a call"
				" too lookup `{0}` but it's not there.".format(uuid)
			);

	def startWorker(self, uuid):
		"""Starts worker with given uuid.
		"""
		if uuid in self.__workers: 
			self.__threadPool.start(self.__workers[uuid]);
		else:
			qDebug("[DEBUG] ImageResolver.startWorker() did receive a call"
				" too lookup `{0}` but it's not there.".format(uuid)
			);
