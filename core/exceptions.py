"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
#import warnings

class LionsToothError(Exception):
	"""Base exception for all errors
	raised by LionsTooth.
	"""
	pass

class ThemeError(LionsToothError):
	"""Exception raised when something related to theming goes wrong.
	"""
	pass

class PageError(LionsToothError):
	"""Exception raised when something related to a page goes wrong.
	"""
	pass

class ImageResolveError(LionsToothError):
	"""Exception raised when something related to a image resolving goes wrong.
	"""
	pass
