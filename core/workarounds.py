"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys
"""Python 3.5, regex uses .group(). Python 3.6 uses substitution.
"""
python35 = False;
if sys.version_info[1] < 6: python35 = True;
class Workarounds():
	@staticmethod
	def groupsToSubstitution(matchObj):
		
		newMatchObj = []
		if python35:
			if type(matchObj) is tuple:
				return matchObj
			else:
				newMatchObj.append( matchObj.group(0) )
				for m in matchObj.groups():
					newMatchObj.append( m )
		else:
			return matchObj
		return newMatchObj
