"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QRunnable, QObject, pyqtSignal, pyqtSlot

"""ReadTimeout requires minimal version of python3-requests 2.11 
https://pypi.python.org/pypi/requests/2.11.1 """
import requests
version = requests.__version__.split('.')
if int(version[0]) == 2 and int(version[1]) >= 11:
	from requests import ConnectionError, ReadTimeout
else:
	from requests import ConnectionError
	class ReadTimeout(Exception): pass

import diaspy
import sys
import uuid

class RequestsWorkerSignals(QObject):
	finished = pyqtSignal();
	success = pyqtSignal();
	exception = pyqtSignal(str);
	result = pyqtSignal(object); # When we want a new object, this will return it.

class RequestsWorker(QRunnable):
	"""This class represents the worker that will be used to do requests, like
	fetching a `diaspy.streams.Stream()`, a image or interacting with a 
	`diaspy.models.Post()` .. etc.

	# `diaspy.connection.Connection()`
		* Login
		* Logout
		* Fetch/Update userdata

	# `diaspy.streams.Generic()`
		* Fetch/Update a stream.
		* `Post()` interactions.

	etc...

	"""
	def __init__(self, job=None, *args, **kwargs):
		QRunnable.__init__(self);

		self.__uuid = uuid.uuid4().hex;
		self.__args = args; # arguments
		self.__kwargs = kwargs; # keyworded arguments.
		self.__method = job; # method to call.

		self.signals = RequestsWorkerSignals();

	def id(self): return self.__uuid;

	@pyqtSlot()
	def run(self):
		try: result = self.__method(*self.__args, **self.__kwargs);
		except (ConnectionError, ReadTimeout) as err:
			self.signals.exception.emit("Connection error: {}".format(err));
		except diaspy.errors.DiaspyError as err:
			self.signals.exception.emit("Diaspy error: {}".format(err));
		except Exception as err:
			exctype, value = sys.exc_info()[:2]
			self.signals.exception.emit("[Error]: `{}` `{}`"
				.format(err, exctype)
			);
		else:
			self.signals.success.emit();
			self.signals.result.emit(result);
		finally: self.signals.finished.emit();
