"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from datetime import datetime, timedelta
class Formatter():
	@staticmethod
	def tago(date_str):
		date_a = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S.%fZ")
		time_ago = datetime.utcnow() - date_a
		years = divmod(time_ago.days, 365)

		month_length = 31 # TODO

		if years[0] == 1: return "{} year ago".format(years[0]);
		elif years[0] > 1: return "{} years ago".format(years[0]);

		if years[1] == 1:
			return "{} day ago".format(years[1]);
		elif years[1] > 1 and years[1] <month_length:
			return "{} days ago".format(years[1]);
		elif years[1] > month_length and years[1] < month_length*2:
			return "{} month ago".format(int(years[1]/month_length));
		elif years[1] > month_length*2:
			return "{} months ago".format(int(years[1]/month_length));

		hours = round(
			((time_ago - timedelta(days=time_ago.days)).seconds) / 3600
		);

		if hours == 1: return "{} hour ago".format(hours);
		elif hours > 1: return "{} hours ago".format(hours);

		minuntes = round(time_ago.seconds / 60);
		if minuntes > 1: return "{} minutes ago".format(round(minuntes));
		else: return "just now.";

	@staticmethod
	def f_timestamp(timestamp):
		""" Formatted timestamp
		"""
		created_at = datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ");
		return datetime.strftime(created_at, "%Y-%m-%d %H:%M:%S");
