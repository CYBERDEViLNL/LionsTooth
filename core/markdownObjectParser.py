"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import re
from core.workarounds import Workarounds

class MarkdownObjectParser():
	""" Objects patterns """
	url_patterns = {};
	url_patterns.update({"scheme"	: r"https?://"});
	url_patterns.update({"netloc"	: r"([a-zA-Z0-9_\-]+\.)+[a-zA-Z]{2,13}"});
	url_patterns.update({"params"	: r"[a-zA-Z0-9\.\/_\-\:\%\=\&\?\,\@\#\$\!\^\*\(\)\+]+"});
	url_patterns.update({"url"	: r"({})({})/({})".format(url_patterns["scheme"], url_patterns["netloc"], url_patterns["params"])});

	pattern = {}
	pattern.update({'Image': r'\!\[(.*?)\]\(({0})( [\"\'](.+)[\"\'])?\)'
		.format(url_patterns['url'])}
	);
	pattern.update({"ImageLink"	: r"\[{0}\]\((https?://[a-zA-Z0-9\.\/_\-\:\%\=\&\?\,\@\#\$\!\^\*\(\)\+]+)( [\"\'](.+)[\"\'])?\)".format(pattern["Image"])});
	pattern.update({"Image"		: r"(?<!\[){}".format(pattern["Image"])});
	pattern.update({"CodeBlock"	: r"[`]{3,4}(.*?)[`]{3,4}"});

	@staticmethod
	def parse(string):
		"""Parses Images, Images with links and Code blocks, other will be
		returned as Text for the `real` markdown parser its job. We do this
		because we want GIF support and a scrollbar for CodeBlock's.
		"""
		string = MarkdownObjectParser.replaceReferences(string);
		matches = []; #[[start,end,match,pattern_name], ..]
		for name in MarkdownObjectParser.pattern:
			if name == "CodeBlock":
				p = re.compile(MarkdownObjectParser.pattern[name],
														re.I|re.DOTALL|re.M);
			else: p = re.compile(MarkdownObjectParser.pattern[name],re.I);
			for m in p.finditer(string):
				newObj = Workarounds.groupsToSubstitution(m);
				matches.append([m.start(), len(m.group())+m.start(), newObj, name]);

		objects = [];
		lastMatchEnd = 0;
		for data in sorted(matches):
			text = string[lastMatchEnd:data[0]];
			if len(text.strip()): objects.append({"type":'Text',"content":text.rstrip()});

			if data[3] == 'Image':
				objects.append({"type":data[3], "url":data[2][2],
								"alt":data[2][1], "title":data[2][7]});
			elif data[3] == 'ImageLink':
				objects.append({"type":data[3], "url":data[2][2],
								"alt":data[2][7], "title":data[2][8],
								"unknown0":data[2][1], "link":data[2][9],
								"unknown1":data[2][10]});
			elif data[3] == 'CodeBlock' and data[2][1].strip():
				objects.append({"type":data[3], "content":data[2][1]});

			lastMatchEnd = data[1];

		text = string[lastMatchEnd:];
		if len(text.strip()): objects.append({"type":'Text',"content":text.rstrip()});
		return objects;

	@staticmethod
	def replaceReferences(text):
		references = re.findall(r'(\[([a-zA-Z0-9]+)\]\: (.*?)[\s\n\r]+)',
															text, re.M);
		if references:
			for ref in references:
				refGroups = Workarounds.groupsToSubstitution(ref);
				text = text.replace(refGroups[0], '');
				text = text.replace('[{}]'.format(refGroups[1]), '({})'
														.format(refGroups[2]));
		return text
