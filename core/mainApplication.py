"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal

#import os # check if file exists for theme
import importlib
import signal

from core.translator import Translator

from core import exceptions
from core.config import Config
from os import path, listdir


class MainApplication(QtWidgets.QApplication):
	themeChanged = pyqtSignal();

	def __init__(self, argv):
		# Respond to KeyboardInterrupt.
		signal.signal(signal.SIGINT, signal.SIG_DFL);

		QtWidgets.QApplication.__init__(self, argv);

		self.__themesPath = "data/themes";
		self.__configFilePath = 'config.ini'
		self.__themes = [];
		self.__rc = None;

		self.config = Config(self.__configFilePath);
		if "General" not in self.config: self.config.add_section("General");
		if "Appearance" not in self.config: self.config.add_section("Appearance");
		if "Connection" not in self.config: self.config.add_section("Connection");

		# Theme
		self.getThemes();
		self.currentTheme = self.config["General"].get("theme", "default");
		self.setTheme(self.currentTheme);

		# Language
		if "General" in self.config:
			lang = self.config["General"].get("language", "");
			if lang: Translator.setLanguage(lang);


	def themes(self): return self.__themes;

	def getThemes(self):
		if not path.isdir(self.__themesPath):
			raise exceptions.ThemeError("Theme path `{0}` not found."
										.format(self.__themesPath))

		for name in listdir(self.__themesPath):
			if name.startswith('.') or name.startswith('_'): continue;
			themePath = path.join(self.__themesPath, name);
			if path.isdir(themePath):
				qssPath = "{0}/{1}.qss".format(themePath,name);
				rcPath = "{0}/icons.py".format(themePath);
				if path.isfile(qssPath) and path.isfile(rcPath):
					# TODO verify files?
					self.__themes.append(name);
				else:
					raise exceptions.ThemeError("No qss or icons.py "
									"found for theme `{0}` in `{1}`."
									.format(name, self.__themesPath))

	def setTheme(self,themeName):
		if themeName not in self.__themes:
			raise exceptions.ThemeError("Theme `{0}` not found."
										.format(themeName))

		qss = "";
		qssPath = "{0}/{1}/{1}.qss".format(self.__themesPath,themeName);
		fd=open(qssPath,"r");
		qss=fd.read();

		if self.__rc: self.__rc.qCleanupResources();
		# TODO make path dynamic
		self.__rc=importlib.import_module("data.themes.{0}.icons"
											.format(themeName))
		self.__rc.qInitResources();

		# Apply stylesheet.
		self.setStyleSheet(qss);

		# Emit that the theme is changes so childeren can polish.
		self.themeChanged.emit();
