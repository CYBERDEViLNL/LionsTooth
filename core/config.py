"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import configparser

class Config(configparser.ConfigParser):
	def __init__(self, path):
		configparser.ConfigParser.__init__(self);
		self["Connection"] = {};
		self["Appearance"] = {};
		self["Cache"] = {};
		self["Timers"] = {};
		self.read(path);
		self.__path = path;

	def save(self):
		with open(self.__path, 'w') as configfile:
			self.write(configfile);
