"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import markdown

from markdown.inlinepatterns import Pattern
from markdown.extensions import Extension
from markdown.util import etree
from markdown.blockparser import BlockParser
from markdown.blockprocessors import BlockProcessor, HashHeaderProcessor
from markdown.treeprocessors import Treeprocessor
from markdown import util
import re

"""
https://python-markdown.github.io/extensions/api/
"""
class MentionPattern(Pattern):
	def __init__(self, pattern, mentionedPeople):
		Pattern.__init__(self, pattern);
		self.mentionedPeople = mentionedPeople;

	def handleMatch(self, m):
		b = etree.Element('b');
		b.text = "@";
		a = etree.SubElement(b, 'a');

		if m.group(3): txt = m.group(3);
		else: txt = m.group(4);

		guid=None;
		for people in self.mentionedPeople:
			if people['diaspora_id'].lower() == m.group(4).lower():
				guid = people['guid'];

		# AtomicString doesn't parse text
		a.text = markdown.util.AtomicString(txt);
		if guid:
			a.set("href", "#people_guid:{}".format(
				markdown.util.AtomicString(guid))
			);
		else:
			a.set("href", "#diaspora_id:{}".format(
				markdown.util.AtomicString(m.group(4)))
			);
		return b;

class TagPattern(Pattern):
	def __init__(self, pattern):
		Pattern.__init__(self, pattern);

	def handleMatch(self, m):
		b = etree.Element('b');
		a = etree.SubElement(b, 'a');
		# AtomicString doesn't parse text
		a.text = markdown.util.AtomicString("#{}".format(m.group(2)));
		a.set("href", "#tag:{}".format(markdown.util.AtomicString(m.group(2))));
		return b;

class DiasporaMD(Extension):
	def __init__(self, mentionedPeople={}, **kwargs):
		Extension.__init__(self, **kwargs);
		self.mentionedPeople = mentionedPeople;

		self.pattern = {};
		self.pattern.update({'author_name'	: r'.+?[^;]' });# TODO
		self.pattern.update({'diaspora_id'	: r'[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-\.]+\.[a-zA-Z]{2,10}'});# TODO
		self.pattern.update({'mention'		: r'\@\{((%(author_name)s)\s?\;\s?)?(%(diaspora_id)s)\}' % {'author_name':self.pattern['author_name'], 'diaspora_id':self.pattern['diaspora_id']}});
		self.pattern.update({'tag'			: r"(\s|^)#([\w0-9\_\-]{1,255})"});

	def extendMarkdown(self, md, md_globals):
		mentionPattern = MentionPattern(self.pattern['mention'], self.mentionedPeople);
		tagPattern = TagPattern(self.pattern['tag']);

		md.inlinePatterns.add('mention', mentionPattern, '<reference');
		md.inlinePatterns.add('hashtag', tagPattern, '<reference');
		md.parser.blockprocessors['hashheader'].RE = re.compile(r'(^|\n)(?P<level>#{1,6} )(?P<header>.*?)#*(\n|$)'); # hashheader now requires a space between the # and the text
