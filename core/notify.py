"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from enum import Enum

messageHandler = None; # module global

class Notify():
	class Type(Enum):
		Message		= 0;
		Warning		= 1;
		Error		= 2;

	@staticmethod
	def handle(msg, type):
		global messageHandler;
		if messageHandler: messageHandler(msg, type);

	@staticmethod
	def message(msg): Notify.handle(msg, Notify.Type.Message);

	@staticmethod
	def warning(msg): Notify.handle(msg, Notify.Type.Warning);

	@staticmethod
	def error(msg): Notify.handle(msg, Notify.Type.Error);

	@staticmethod
	def installMessageHandler(method):
		global messageHandler;
		messageHandler = method;
