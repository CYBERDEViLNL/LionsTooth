"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, qDebug

from diaspy.tagFollowings import TagFollowings as DiaspyTagFollowings

from core.requestsWorker import RequestsWorker

class TagsModel(QObject):
	"""Will controll followed tags.
	"""
	updated = pyqtSignal(); # emitted on new model
	added = pyqtSignal(int, str, int); # emitted when tag added. id, name, count
	deleted = pyqtSignal(int); # emitted when tag deleted
	working = pyqtSignal(); # controllers should be listening for this and disable actions where needed.
	finished = pyqtSignal(); # controllers can enable their tag actions again.
	def __init__(self, connectionWrapper, threadPool):
		QObject.__init__(self);
		self.__connectionWrapper = connectionWrapper;
		self.__tagFollowings = []; # diaspy.tagFollowings.TagFollowings()
		self.__threadPool = threadPool;
		self.__indexNameMap = {};

	def clear(self):
		# TODO check if there isnt a active thread.
		del self.__tagFollowings;
		self.__tagFollowings = [];
		self.__indexNameMap = {};
		self.updated.emit();

	def __bool__(self):
		if self.__tagFollowings: return True;
		return False;

	def __iter__(self): return iter(self.__tagFollowings);
	def __getitem__(self, t): return self.__tagFollowings[t];

	def __get(self):
		"""Creates a diaspy.tagFollowings.TagFollowings() instance.
		"""
		if self.__connectionWrapper:
			self.__tagFollowings = DiaspyTagFollowings(
										self.__connectionWrapper.connection());

	def has(self, tagName):
		"""Returns True if we have the tag (are following it)
		"""
		if tagName in self.__indexNameMap: return True
		return False

	def __updateIndexNameMap(self):
		for index, followedTag in enumerate(self.__tagFollowings):
			self.__indexNameMap.update({followedTag.name():index});

	@pyqtSlot(str)
	def __getFailed(self, err):
		qDebug("[DEBUG] TagsModel.__getFailed() `{0}`".format(err));
		self.finished.emit();

	@pyqtSlot(str)
	def __followFailed(self, err):
		qDebug("[DEBUG] TagsModel.__followFailed() `{0}`".format(err));
		self.finished.emit();

	@pyqtSlot(str)
	def __updateFailed(self, err):
		qDebug("[DEBUG] TagsModel.__updateFailed() `{0}`".format(err));
		self.finished.emit();

	@pyqtSlot(str)
	def __deleteFailed(self, err):
		qDebug("[DEBUG] TagsModel.__deleteFailed() `{0}`".format(err));
		self.finished.emit();

	@pyqtSlot()
	def __getSucceeded(self):
		# Create a index-name map
		self.__updateIndexNameMap();
		self.updated.emit();
		self.finished.emit();

	@pyqtSlot(object)
	def __followSucceeded(self, result):
		# TODO add the followed tag to this model
		index = len(self.__indexNameMap);
		self.__indexNameMap.update(
							{result.name() : index});
		self.added.emit(result.id(), result.name(), result.count());
		self.finished.emit();

	@pyqtSlot()
	def __updateSucceeded(self):
		# TODO do a diff and store only new tags and deleted tags somewhere.
		# This can happen from another client.
		self.updated.emit();
		self.finished.emit();

	@pyqtSlot(object)
	def __deleteSucceeded(self, index):
		qDebug("[DEBUG] TagsModel.__deleteSucceeded()");

		for k, v in self.__indexNameMap.items():
			if v == index:
				self.__indexNameMap.pop(k);
				break;
		#k = list(self.__indexNameMap.keys())[int(index)];
		#self.__indexNameMap.pop(k);
		self.deleted.emit(int(index));
		self.finished.emit();
		#self.updated.emit();

	def __deleteTag(self, tagName):
		index = self.__indexNameMap[tagName];
		self.__tagFollowings[index].delete();
		return index

	def deleteTag(self, tagName):
		print("[DEBUG] TagsModel.deleteTag()");
		if self.__tagFollowings:
			if tagName in self.__indexNameMap:
				worker = RequestsWorker(job=self.__deleteTag, tagName=tagName);
				worker.signals.exception.connect(self.__deleteFailed);
				worker.signals.result.connect(self.__deleteSucceeded);
				self.working.emit();
				self.__threadPool.start(worker);
			else:
				qDebug("[DEBUG] TagsModel.deleteTag() tried to delete\n"
					"a tag that isn't in self.__indexNameMap.");
				print(tagName)
				print(self.__indexNameMap)
		else:
			qDebug("[DEBUG] TagsModel.deleteTag() tried to delete\n"
					"a tag, but tagFollowings hasn't been set yet.");

	def follow(self, tagName):
		if self.__tagFollowings:
			worker = RequestsWorker(job=self.__tagFollowings.follow, name=tagName);
			worker.signals.exception.connect(self.__followFailed);
			worker.signals.result.connect(self.__followSucceeded);
			self.working.emit();
			self.__threadPool.start(worker);
		else:
			qDebug("[DEBUG] TagsModel.follow() tried to follow\n"
					"a tag, but tagFollowings hasn't been set yet.");

	def update(self):
		"""Updates TagFollowings in a thread.
		"""
		if self.__tagFollowings:
			worker = RequestsWorker(job=self.__tagFollowings.fetch);
			worker.signals.exception.connect(self.__updateFailed);
			worker.signals.success.connect(self.__updateSucceeded);
			self.working.emit();
			self.__threadPool.start(worker);
		else:
			qDebug("[DEBUG] TagsModel.update() tried to \n"
					"refetch tagFollowings, but it hasn't been set yet.");

	def get(self):
		"""Runs self.__setTagFollowings() in a thread.
		"""
		if not self.__tagFollowings:
			worker = RequestsWorker(job=self.__get);
			worker.signals.exception.connect(self.__getFailed);
			worker.signals.success.connect(self.__getSucceeded);
			self.working.emit();
			self.__threadPool.start(worker);
		else:
			qDebug("[DEBUG] TagsModel.get() tried to set\n"
					" __tagFollowings, but it already has been set.");
