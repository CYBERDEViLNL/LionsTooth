"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, QThreadPool, qDebug

from diaspy.notifications import Notifications as DiaspyNotifications

from core.requestsWorker import RequestsWorker

from bs4 import BeautifulSoup

class NotificationsModel(QObject):
	"""Will manage notifications.
	"""
	updated = pyqtSignal(); # emitted on new model
	unreadCountChanged = pyqtSignal();
	notificationToggled = pyqtSignal(int);
	def __init__(self, connectionWrapper):
		QObject.__init__(self);
		self.__connectionWrapper = connectionWrapper;
		self.__notifications = []; # diaspy.notifications.Notifications()
		self.__threadPool = QThreadPool();

		self.__unreadCount = 0;

		self.__connectionWrapper.statusChanged.connect(self.__connectionStatusChanged);

	@pyqtSlot(int)
	def __connectionStatusChanged(self, status):
		# On logout
		if not status:
			del self.__notifications;
			self.__notifications = [];
		else: self.get();
			
		self.updated.emit();

	def __len__(self):
		if self.__notifications:
			return len(self.__notifications);
		return 0;

	def __bool__(self):
		if self.__notifications: return True;
		return False;

	def __iter__(self):
		if self.__notifications:
			return iter(self.__notifications);
		else: return iter([]);

	def __getitem__(self, n):
		if self.__notifications:
			return self.__notifications[n];

	def unreadCount(self): return self.__unreadCount;

	def _updateUnreadCount(self):
		if self.__unreadCount != self.__notifications.data()["unread_count"]:
			self.__unreadCount = self.__notifications.data()["unread_count"];
			self.unreadCountChanged.emit();

	def clear(self):
		# TODO check if there isnt a active thread.
		del self.__notifications;
		self.__notifications = [];
		self.updated.emit();

	def parseHtml(self, html):
		"""
		Diaspora returns a notification in HTML, we are going to use it
		but first lets remove some elements that we will set on the 
		people page's profile from diaspy data.
		"""
		soup = BeautifulSoup(html, 'lxml');
		img = soup.find('img', {"class": "avatar"});
		avatarUrl = "";
		people = {};

		""" remove timeago """
		timeago = soup.find('time', {"class": "timeago"});
		timeagoDiv = timeago.find_parent('div');
		timeagoDiv.decompose();

		""" remove unread toggle """
		unreadToggle = soup.find('div', {"class": "unread-toggle"});
		unreadToggle.decompose();

		""" avatar """
		if img.has_attr('src'):
			avatarUrl = img['src'];

		""" remove avatar """
		avatar = soup.find('div', {"class": ["media-object", "pull-left"]});
		avatar.decompose();

		""" people """
		links = soup.findAll('a', {"class": "hovercardable"});
		for link in links:
			if link.has_attr('data-hovercard'):
				guid = link['href'].rsplit('/', 1)[1];
				people.update( { guid: link.text } );
		return (avatarUrl, people, str(soup));

	def addParsedContent(self, notification, avatarUrl, people, html):
		notification._data.update({"avatar":avatarUrl});
		notification._data.update({"people":people});
		notification._data.update({"parsed_html":html});

	def _updateParsedContents(self):
		updated = False;
		for notification in self.__notifications:
			if "parsed_html" not in notification._data:
				parsedContent = self.parseHtml(notification._data["note_html"]);
				self.addParsedContent(notification, *parsedContent);
				updated = True;
		return updated;

	@pyqtSlot(str)
	def __getFailed(self, err):
		qDebug("Fetching notifications failed: `{}`".format(err));

	@pyqtSlot()
	def __getSucceeded(self):
		self._updateUnreadCount();
		if self._updateParsedContents(): self.updated.emit();

	def __get(self):
		if self.__connectionWrapper:
			self.__notifications = DiaspyNotifications(
										self.__connectionWrapper.connection());
	def get(self):
		if not self.__notifications and not self.__threadPool.activeThreadCount():
			worker = RequestsWorker(job=self.__get);
			worker.signals.exception.connect(self.__getFailed);
			worker.signals.success.connect(self.__getSucceeded);
			self.__threadPool.start(worker);
		else:
			qDebug("[DEBUG] tried to set __notifications, but it already has been set.");

	@pyqtSlot(str)
	def __moreFailed(self, err):
		qDebug("Fetching more notifications failed: `{}`".format(err));

	@pyqtSlot()
	def __moreSucceeded(self):
		self._updateUnreadCount();
		if self._updateParsedContents(): self.updated.emit();

	def __more(self):
		if self.__connectionWrapper: self.__notifications.more();

	def more(self):
		if self.__notifications and not self.__threadPool.activeThreadCount():
			worker = RequestsWorker(job=self.__more);
			worker.signals.exception.connect(self.__moreFailed);
			worker.signals.success.connect(self.__moreSucceeded);
			self.__threadPool.start(worker);

	@pyqtSlot(str)
	def __updateFailed(self, err):
		qDebug("Fetching update notifications failed: `{}`".format(err));

	@pyqtSlot()
	def __updateSucceeded(self):
		self._updateUnreadCount();
		if self._updateParsedContents(): self.updated.emit();

	def __update(self):
		if self.__connectionWrapper: self.__notifications.update();

	def update(self):
		if self.__notifications and not self.__threadPool.activeThreadCount():
			worker = RequestsWorker(job=self.__update);
			worker.signals.exception.connect(self.__updateFailed);
			worker.signals.success.connect(self.__updateSucceeded);
			self.__threadPool.start(worker);

	@pyqtSlot(str)
	def __toggleSeenFailed(self, err):
		qDebug("Fetching update notifications failed: `{}`".format(err));

	@pyqtSlot(object)
	def __toggleSeenSucceeded(self, id):
		self.notificationToggled.emit(id);
		self._updateUnreadCount();

	def __toggleSeen(self, id):
		for notification in self.__notifications:
			if notification.id == id:
				if notification.unread:
					notification.mark(unread=False);
					self.__notifications.data()["unread_count"] -= 1;
				else:
					notification.mark(unread=True);
					self.__notifications.data()["unread_count"] += 1;
				self.unreadCountChanged.emit();
				return notification.id;

	def toggleSeen(self, id):
		if self.__notifications and not self.__threadPool.activeThreadCount():
			worker = RequestsWorker(job=self.__toggleSeen, id=id);
			worker.signals.exception.connect(self.__toggleSeenFailed);
			worker.signals.result.connect(self.__toggleSeenSucceeded);
			self.__threadPool.start(worker);
