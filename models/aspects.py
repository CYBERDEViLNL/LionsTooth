"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import QObject, pyqtSignal

class AspectsModel(QObject):
	"""Will controll aspects data.
	"""
	updated = pyqtSignal();
	def __init__(self):
		QObject.__init__(self);
		self.__aspects = [];

	def clear(self):
		# TODO check if there isnt a active thread.
		self.__aspects.clear();
		self.updated.emit();

	def __len__(self): return len(self.__aspects);

	def __bool__(self):
		if self.__aspects: return True
		return False

	def __iter__(self): return iter(self.__aspects);
	def __getitem__(self, t): return self.__aspects[t];

	def setAspects(self, aspects=[]):
		updated = False;
		if not aspects: # if empty list, reset.
			self.__aspects.clear();
			updated = True;
		else:
			for aspect in aspects:
				if aspect not in self.__aspects:
					self.__aspects.append(aspect);
					updated = True;
		if updated: self.updated.emit();
