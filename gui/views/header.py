"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QFrame, QLabel, QPushButton, QHBoxLayout, QSizePolicy, QLineEdit
from PyQt5.QtCore import pyqtSignal, Qt, QSize
from PyQt5.QtGui import QPixmap, QIcon

from gui.views.user import UserNameView
from gui.widgets.avatar import AvatarWidget

from core.translator import Translator
def translate(string): return Translator.translate(string, "header");
_ = translate;

class CountButton(QPushButton):
	def  __init__(self, iconPath, parent=None):
		QPushButton.__init__(self, parent=parent);
		icon = QIcon();
		icon.addPixmap(QPixmap(iconPath), QIcon.Normal, QIcon.Off)
		self.setIconSize(QSize(32, 32));
		self.setIcon(icon);

		self._countWidget = QLabel("10", self);
		self._countWidget.setObjectName("count");
		self._countWidget.setSizePolicy(QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum));
		self._countWidget.hide();

	def setCount(self, count):
		if count:
			self._countWidget.show();
			self._countWidget.setText(str(count));
			x = 32;
			x -= self._countWidget.size().width() / 2;
			y = 0;
			self._countWidget.move(x,y);
		else:
			self._countWidget.hide();

class HeaderView(QFrame):
	"""
	pageRequest
	object:	page name
	object:	whatever the page needs as parameters or None.
	"""
	pageRequest = pyqtSignal(object,object);
	def  __init__(self, parent=None):
		QFrame.__init__(self, parent=parent);

		self.__avatarHeight = 50;

		layout = QHBoxLayout(self);
		layout.setContentsMargins(0, 0, 0, 0);
		layout.setSpacing(0);

		self.leftLayout = QHBoxLayout();
		self.leftLayout.setAlignment(Qt.AlignLeft);
		layout.addLayout(self.leftLayout);

		self.rightLayout = QHBoxLayout();
		self.rightLayout.setAlignment(Qt.AlignRight);
		self.rightLayout.setSpacing(3);
		layout.addLayout(self.rightLayout);

		sizePolicy = QSizePolicy(
					QSizePolicy.Maximum,
					QSizePolicy.Maximum);

		"""leftLayout
		"""
		self._avatarWidget = AvatarWidget(parent=self);
		self.leftLayout.addWidget(self._avatarWidget);

		self._nameWidget = UserNameView(_("Signed out."), parent=self);
		self._nameWidget.setSizePolicy(sizePolicy);
		self.leftLayout.addWidget(self._nameWidget, 1);

		"""rightLayout
		 - Info button
		 - Notifications button
		 - Conversations button
		 - Search input field
		"""
		self._notifyButton = CountButton(":/icons/32x32/info", parent=self);
		self.rightLayout.addWidget(self._notifyButton);

		self.notificationsWidget = CountButton(":/icons/32x32/notification", parent=self);
		self.rightLayout.addWidget(self.notificationsWidget);

		self.conversationsWidget = CountButton(":/icons/32x32/message", parent=self);
		self.rightLayout.addWidget(self.conversationsWidget);

		self.searchInputWidget = QLineEdit(self);
		self.searchInputWidget.setPlaceholderText(_("Search .."));
		self.rightLayout.addWidget(self.searchInputWidget);

	def updateAvatar(self, path):
		"""Expects local path
		"""
		self._avatarWidget.setPath(path);

	def setMessageCount(self, count):
		self.conversationsWidget.setCount(count);

	def setNotificationsCount(self, count):
		self.notificationsWidget.setCount(count);

	def avatarWidget(self): return self._avatarWidget;
	def nameWidget(self): return self._nameWidget;
	def notificationsButton(self): return self.notificationsWidget;
	def conversationsButton(self): return self.conversationsWidget;
	def notifyButton(self): return self._notifyButton;
