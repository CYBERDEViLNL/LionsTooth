"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot, QSize

from controllers.pageController import PageNames

class NavigationBarActionProto(QtWidgets.QFrame):
	activated = pyqtSignal();
	pageRequest = pyqtSignal(object); # <! Page name from PageNames
	pageRequestParams = pyqtSignal(object, object);
	def  __init__(self, name, params=None, parent=None):
		"""
			:param name:
			:type name:

			:param params: Reference to a method wich will return parameters when needed.
			:type params:

			:param parent:
			:type parent:
		"""
		QtWidgets.QWidget.__init__(self, parent);

		self.__actionName = name;
		self.__params = params;

		sizePolicy = QtWidgets.QSizePolicy(
					QtWidgets.QSizePolicy.Expanding,
					QtWidgets.QSizePolicy.Preferred);
		self.setSizePolicy(sizePolicy);

		self.setMinimumHeight(32);
		self.setMaximumHeight(16777215);
		self.setMouseTracking(True);
		self.setAutoFillBackground(True);

	def name(self): return self.__actionName;
	def params(self): return self.__params;
	def setParams(self, params): self.__params = params;
	def setActive(self): self.activated.emit();

	def collapse(self): pass;
	def expand(self): pass;

	def repolish(self):
		self.style().unpolish(self);
		self.style().polish(self);
		# .. additional code.

	def changeState(self, state):
		if state: self.expand();
		else: self.collapse();

	def isChecked(self): return self.property("checked");

	def setChecked(self, state):
		if state: self.setProperty("checked", True);
		else: self.setProperty("checked", False);
		self.repolish();

	def mousePressEvent(self, event):
		if event.button() == Qt.LeftButton: self.activated.emit();

	def enterEvent(self, event):
		self.setProperty("hover", True);
		self.repolish();

	def leaveEvent(self, event):
		self.setProperty("hover", False);
		self.repolish();

class NavigationBarAction(NavigationBarActionProto):
	""" Default action with a label on the left and a icon on the right
	"""
	def  __init__(self, name, text="", iconPath=None, params=None, parent=None):
		"""
			:param name:
			:type name:

			:param text:
			:type text:

			:param icon:
			:type icon:

			:param params: Reference to a method wich will return parameters when needed.
			:type params:

			:param parent:
			:type parent:
		"""
		NavigationBarActionProto.__init__(self, name, parent=None);

		self.__params = params;

		self.topLayout = QtWidgets.QHBoxLayout(self);
		self.topLayout.setContentsMargins(0, 0, 0, 0);
		self.topLayout.setSpacing(0);

		self.__labelWidget = QtWidgets.QLabel(self);
		self.__pixmapWidget = QtWidgets.QPushButton(self);
		self.__pixmapWidget.setFlat(True);
		self.__pixmapWidget.clicked.connect(self.__pageRequest);
		

		if text: self.setText(text);
		if iconPath: self.setIconPath(iconPath);

		self.topLayout.addWidget(self.__labelWidget, 1, Qt.AlignLeft);
		self.topLayout.addWidget(self.__pixmapWidget, 0, Qt.AlignRight);

		self.setAutoFillBackground(True);

	def params(self): return self.__params;

	def __pageRequest(self):
		if self.params():
			self.pageRequestParams.emit(self.name(), self.params());
		else: self.pageRequest.emit(self.name());

	def mousePressEvent(self, event):
		NavigationBarActionProto.mousePressEvent(self, event);
		if event.button() == Qt.LeftButton:
			self.__pageRequest();

	def repolish(self):
		NavigationBarActionProto.repolish(self);
		self.__labelWidget.style().unpolish(self.__labelWidget);
		self.__labelWidget.style().polish(self.__labelWidget);
		self.__pixmapWidget.style().unpolish(self.__pixmapWidget);
		self.__pixmapWidget.style().polish(self.__pixmapWidget);

	def expand(self): self.__labelWidget.show();
	def collapse(self): self.__labelWidget.hide();

	def setText(self, text):
		""" Set label text

		:param text: text to set on the label.
		:type text: str
		:returns: None
		"""
		self.__labelWidget.setText(text);
		self.__pixmapWidget.setToolTip(text);

	def setIconPath(self, iconPath):
		""" Set the icon.

		:param icon: icon path
		:type icon: str
		:returns: None
		"""
		icon = QIcon();
		icon.addPixmap(QPixmap(iconPath), QIcon.Normal, QIcon.Off)
		self.__pixmapWidget.setIconSize(QSize(32, 32));
		self.__pixmapWidget.setIcon(icon);

class CheckableAspectItem(QtWidgets.QCheckBox):
	dataUpdated = pyqtSignal();
	def __init__(self, data, parent=None):
		QtWidgets.QCheckBox.__init__(self, parent)

		self.__data = data;

		self.setChecked(data["selected"]);
		self.setText(data["name"]);
		self.setTristate(False);

		sizePolicy = QtWidgets.QSizePolicy(
					QtWidgets.QSizePolicy.Maximum,
					QtWidgets.QSizePolicy.Maximum);
		self.setSizePolicy(sizePolicy);

		self.stateChanged.connect(self.__stateChanged);

	def id(self): return data["id"];
	def name(self): return data["name"];

	@pyqtSlot()
	def __stateChanged(self):
		if self.checkState(): self.__data["selected"] = True;
		else: self.__data["selected"] = False;
		self.dataUpdated.emit();

class NavigationBarAspectsAction(NavigationBarActionProto):
	""" Aspects action
	"""
	def  __init__(self, name, data=None, parent=None):
		NavigationBarActionProto.__init__(self, name, parent=None);

		QtWidgets.QVBoxLayout(self);
		self.layout().setContentsMargins(0, 0, 0, 0);
		self.layout().setSpacing(0);

		self.setMinimumHeight(0);

		self.__filterButton = QtWidgets.QPushButton("Filter", self);
		self.__filterButton.clicked.connect(self.__filterButtonClicked);
		self.__filterButton.hide();

		self.__data = [];
		self.__items = [];
		self.__checkedIds = []; # selectedIds() backup
		if data: self.setAspects(data);

	def selectedIds(self):
		""" Returns a list with aspect ids
		"""
		list = [];
		for d in self.__data:
			if d["selected"]: list.append(d["id"]);
		return list;

	def collapse(self):
		for item in self.__items: item.hide();
		self.__filterButton.hide();

	def expand(self):
		for item in self.__items: item.show();
		self.__filterButton.show();

	@pyqtSlot()
	def __filterButtonClicked(self):
		self.__checkedIds = self.selectedIds();
		self.__filterButton.hide();
		self.pageRequestParams.emit(PageNames.aspectsPage, self.selectedIds());

	@pyqtSlot()
	def __aspectsUpdated(self):
		if self.selectedIds() != self.__checkedIds: self.__filterButton.show();
		else: self.__filterButton.hide();

	def reset(self):
		for i in reversed(range(self.layout().count())):
			item = self.layout().itemAt(i);
			if item:
				if type(item.widget()) is CheckableAspectItem:
					item.widget().setParent(None);
				else: # The update button
					self.layout().takeAt(i);
		self.__items.clear();
		self.__checkedIds = self.selectedIds();

	def setAspects(self, aspects):
		self.reset();
		for aspect in aspects:
			aspectItem = CheckableAspectItem(aspect, self);
			self.__items.append(aspectItem);
			aspectItem.dataUpdated.connect(self.__aspectsUpdated);
			self.layout().addWidget(aspectItem);
		self.__data = aspects; # need this for return values of selectedIds()
		self.__checkedIds = self.selectedIds();
		self.layout().addWidget(self.__filterButton);

class DeletableTagItem(QtWidgets.QWidget):
	deleteMe = pyqtSignal(); # <! tag id
	clicked = pyqtSignal();
	def __init__(self, id, name, count, parent=None):
		QtWidgets.QWidget.__init__(self, parent=parent);

		self.__name = name;
		self.__id = id;
		self.__enabled = True;

		QtWidgets.QHBoxLayout(self);
		self.layout().setContentsMargins(0, 0, 0, 0);
		self.layout().setSpacing(0);

		self.nameLabel = QtWidgets.QLabel("#{0}".format(name), self);
		self.layout().addWidget(self.nameLabel, 0, Qt.AlignLeft);

		self.setCount(count);

		self.deleteButton = QtWidgets.QPushButton("X", self);
		self.deleteButton.setMaximumWidth(32);
		self.deleteButton.setToolTip("Stop following #{0}".format(name));
		sizePolicy = QtWidgets.QSizePolicy(
					QtWidgets.QSizePolicy.Maximum,
					QtWidgets.QSizePolicy.Maximum);
		self.deleteButton.setSizePolicy(sizePolicy);
		self.deleteButton.clicked.connect(self.__deleteMe);
		self.layout().addWidget(self.deleteButton, 0, Qt.AlignRight);

	@pyqtSlot()
	def __deleteMe(self):
		# TODO Prompt `Are you sure?`
		self.deleteMe.emit();

	@pyqtSlot()
	def __clicked(self): self.clicked.emit();

	def disable(self):
		self.__enabled = False;
		self.deleteButton.setEnabled(False);

	def enable(self):
		self.__enabled = True;
		self.deleteButton.setEnabled(True);

	def mousePressEvent(self, event):
		if event.button() == Qt.LeftButton and self.__enabled:
			self.clicked.emit();

	def id(self): return self.__id;
	def name(self): return self.__name;
	def setCount(self, count):
		self.nameLabel.setToolTip("Tag count: {0}".format(count));

class NavigationBarTagsAction(NavigationBarActionProto):
	""" Tags action
	"""
	def  __init__(self, name, model=None, parent=None):
		NavigationBarActionProto.__init__(self, name, parent=None);

		self.__items = [];

		QtWidgets.QVBoxLayout(self);
		self.layout().setContentsMargins(0, 0, 0, 0);
		self.layout().setSpacing(0);

		self.setMinimumHeight(0);

		"""
		[ INPUT - #newtag ] [ BUTTON - Add ]
		#tag1		X
		#tag2		X

		TagsController should handle this view TODO
		"""

	def __pageRequest(self, tagName):
		self.pageRequestParams.emit(PageNames.tagsPage, tagName);

	@pyqtSlot()
	def __tagClicked(self): self.__pageRequest(self.sender().name());

	@pyqtSlot()
	def disableInteractions(self):
		for item in self.__items: item.disable();

	@pyqtSlot()
	def enableInteractions(self):
		for item in self.__items: item.enable();

	def collapse(self):
		for item in self.__items: item.hide();

	def expand(self):
		for item in self.__items: item.show();

	def addTag(self, id, name, count):
		"""
		"""
		tagItem = DeletableTagItem(id, name, count, self);
		tagItem.clicked.connect(self.__tagClicked);
		self.__items.append(tagItem);
		self.layout().addWidget(tagItem);
		return tagItem;

	def removeTag(self, index):
		"""
		"""
		self.__items[index].setParent(None);
		self.__items.pop(index);

	def clearTags(self):
		for item in self.__items:
			item.setParent(None);
		self.__items.clear();

class NavigationBarGroup(QtWidgets.QWidget):
	""" NavigationBar group wich holds a NavigationBarAction as first item.
	Additional actions can be added.
	"""
	activated = pyqtSignal();	""" when a action gets activated, used
										for view side. """
	pageRequest = pyqtSignal(object);
	pageRequestParams = pyqtSignal(object, object);
	def  __init__(self, name, text="", icon=None, params=None, parent=None):
		"""
			:param name:
			:type name:

			:param text:
			:type text:

			:param icon:
			:type icon:

			:param params: Reference to a method wich will return parameters when needed.
			:type params:

			:param parent:
			:type parent:
		"""
		QtWidgets.QWidget.__init__(self, parent);

		sizePolicy = QtWidgets.QSizePolicy(
					QtWidgets.QSizePolicy.Expanding,
					QtWidgets.QSizePolicy.Preferred);
		self.setSizePolicy(sizePolicy);

		self.__actionName = name;
		self.__params = params;

		QtWidgets.QVBoxLayout(self);
		# right-margin 16 for the scrollbar
		self.layout().setContentsMargins(0, 0, 0, 0);
		self.layout().setSpacing(0);
		self.__actions = [];
		self.__actionsVisible = True;
		self.__selectedWidget = None;

		self.__topWidget = NavigationBarAction(name=name, text=text, iconPath=icon,
												parent=self);
		self.__topWidget.activated.connect(self.__topWidgetClicked);
		self.__topWidget.pageRequest.connect(self.pageRequest);
		self.__topWidget.setObjectName("NavigationBarGroupTopAction");

		self.__toggleButton = QtWidgets.QPushButton("^", self.__topWidget);
		self.__toggleButton.setObjectName("toggleButton");
		self.__toggleButton.setMaximumWidth(16);
		self.__toggleButton.setCheckable(True);
		self.__toggleButton.setProperty("state", True);
		self.__toggleButton.toggled.connect(self.__toggleState);
		self.__topWidget.topLayout.insertWidget(1, self.__toggleButton, 0, Qt.AlignRight);
		self.layout().addWidget(self.__topWidget);

	def name(self): return self.__actionName;
	def params(self): return self.__params; # TODO delete arguments from view and add them to the corresponding controller.
	def setParams(self, params): self.__params = params;

	def setActive(self, params=None):
		# TODO give params to childeren
		self.activated.emit();

	def repolish(self):
		self.__toggleButton.style().unpolish(self.__toggleButton);
		self.__toggleButton.style().polish(self.__toggleButton);

	@pyqtSlot()
	def __toggleState(self):
		if self.__toggleButton.isChecked(): self.hideActions();
		else: self.showActions();

	def addAction(self, action):
		self.__actions.append(action);
		action.activated.connect(self.__widgetActivated);
		action.pageRequest.connect(self.pageRequest);
		action.pageRequestParams.connect(self.pageRequestParams);
		action.setProperty("child", True);
		self.layout().addWidget(action);
		if not self.__actionsVisible: action.hide();

	def hideActions(self):
		if self.__toggleButton.isChecked():
			self.__toggleButton.setText("v");
			self.__toggleButton.setProperty("state", False);
		else:
			self.__toggleButton.setText("^");
			self.__toggleButton.setProperty("state", True);
			self.__toggleButton.setChecked(True);
		self.repolish();

		for action in self.__actions: action.hide();
		self.__actionsVisible = False;

	def showActions(self):
		if self.__toggleButton.isChecked():
			self.__toggleButton.setText("v");
			self.__toggleButton.setProperty("state", False);
			self.__toggleButton.setChecked(False);
		else:
			self.__toggleButton.setText("^");
			self.__toggleButton.setProperty("state", True);
		self.repolish();

		for action in self.__actions: action.show();
		self.__actionsVisible = True;

	@pyqtSlot()
	def __topWidgetClicked(self):
		self.__topWidget.setChecked(True);
		if self.__selectedWidget:
			self.__selectedWidget.setChecked(False);
			self.__selectedWidget = None;
		self.setProperty("active", True);
		self.__topWidget.repolish();
		self.activated.emit();

	@pyqtSlot()
	def __widgetActivated(self):
		if self.__selectedWidget:
			self.__selectedWidget.setChecked(False);
		self.__selectedWidget = self.sender();
		self.__selectedWidget.setChecked(True);
		self.setProperty("active", True);
		self.__topWidget.repolish();
		self.activated.emit();

	def activate(self):
		if self.__selectedWidget:
			self.__selectedWidget.setChecked(True);
		self.__topWidget.setChecked(True);
		self.setProperty("active", True);
		self.__topWidget.repolish();

	def deactivate(self):
		if self.__selectedWidget:
			self.__selectedWidget.setChecked(False);
			self.__selectedWidget = None;
		self.__topWidget.setChecked(False);
		self.setProperty("active", False);
		self.__topWidget.repolish();

	def changeState(self, state):
		for actionWidget in self.__actions:
			if state: actionWidget.expand();
			else: actionWidget.collapse();
		if state:
			self.__topWidget.expand();
			self.__toggleButton.show();
		else:
			self.__topWidget.collapse();
			self.__toggleButton.hide();

class NavigationBarView(QtWidgets.QScrollArea):
	"""
	pageRequest
	object:	page name
	object:	whatever the page needs as parameters or None.
	"""
	pageRequest = pyqtSignal(object);
	pageRequestParams = pyqtSignal(object, object);
	def  __init__(self, parent=None):
		QtWidgets.QScrollArea.__init__(self, parent=parent);

		self._toolbar = QtWidgets.QWidget(self);
		self._toolbar.setObjectName("NavigationBar");
		QtWidgets.QVBoxLayout(self._toolbar);
		self._toolbar.layout().setContentsMargins(0, 0, 0, 0);
		self._toolbar.layout().setSpacing(0);
		self._toolbar.layout().setAlignment(Qt.AlignTop);

		self.__actions = [];
		self.__groups = [];
		self.__activeGroup = None;
		self.__selectedWidget = None;
		self.__expanded = True;
		self.__minWidth = 32;
		self.__maxWidth = 200;

		self.toggleButton = QtWidgets.QPushButton("Close",self._toolbar);
		self.toggleButton.setObjectName("ToggleButton");
		self._toolbar.layout().addWidget(self.toggleButton);
		self.toggleButton.clicked.connect(self.toggleState);

		self.setWidgetResizable(True);
		self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff);
		self.setWidget(self._toolbar);

		self.__scrollBarWidth = self.verticalScrollBar().width();

		self.expand();

	def actions(self): return self.__actions;
	def groups(self): return self.__groups;

	@pyqtSlot()
	def __widgetActivated(self):
		if self.__selectedWidget:
			self.__selectedWidget.setChecked(False);
		self.__selectedWidget = self.sender();
		self.__selectedWidget.setChecked(True);

		# If any group active, deactivate
		if self.__activeGroup:
			self.__activeGroup.deactivate();
			self.__activeGroup = None;

	@pyqtSlot()
	def __groupActivated(self):
		if self.__activeGroup and self.__activeGroup != self.sender():
			self.__activeGroup.deactivate();
		self.__activeGroup = self.sender();

		self.__activeGroup.activate();

		# If any non group widget is active then deselect.
		if self.__selectedWidget:
			self.__selectedWidget.setChecked(False);
			self.__selectedWidget = None;

	def toggleState(self):
		if self.__expanded: self.collapse();
		else:
			self.expand();
			self.setFocus();

	def setCollapseWidth(self, width): self.__minWidth = width;
	def setExpandWidth(self, width): self.__maxWidth = width;
	def collapseWidth(self): return self.__minWidth;

	def collapse(self):
		self._toolbar.setMinimumWidth(self.__minWidth);
		self._toolbar.setMaximumWidth(self.__minWidth);
		self.setMinimumWidth(self.__minWidth);
		self.setMaximumWidth(self.__minWidth);
		self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff);
		self.toggleButton.setText("+");
		for group in self.__groups: group.changeState(False);
		for action in self.__actions: action.changeState(False);
		self.__expanded = False;

	def expand(self):
		self._toolbar.setMinimumWidth(self.__maxWidth-self.__scrollBarWidth);
		self._toolbar.setMaximumWidth(self.__maxWidth);
		self.setMinimumWidth(self.__maxWidth);
		self.setMaximumWidth(self.__maxWidth);
		self.toggleButton.setText("-");
		self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded);
		for group in self.__groups: group.changeState(True);
		for action in self.__actions: action.changeState(True);
		self.__expanded = True;

	def newAction(self, name, text, icon=None):
		widget = NavigationBarAction(name=name, text=text, iconPath=icon, parent=self._toolbar);
		self.__actions.append(widget);
		widget.activated.connect(self.__widgetActivated);
		widget.pageRequest.connect(self.pageRequest);
		widget.pageRequest.connect(self.__widgetActivated);
		self._toolbar.layout().addWidget(widget);
		return widget;

	def newGroup(self, name, text="", icon=None):
		newGroup = NavigationBarGroup(name, text, icon, self._toolbar);
		newGroup.activated.connect(self.__groupActivated);
		newGroup.pageRequest.connect(self.pageRequest);
		newGroup.pageRequest.connect(self.__groupActivated);
		newGroup.pageRequestParams.connect(self.pageRequestParams);
		newGroup.pageRequestParams.connect(self.__groupActivated);
		self.__groups.append(newGroup);
		self._toolbar.layout().addWidget(newGroup);
		return newGroup;

	def groups(self): return self.__groups;
