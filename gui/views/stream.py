"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QHBoxLayout, QVBoxLayout, QSizePolicy, QMenu
from PyQt5.QtCore import pyqtSignal, Qt, qDebug

from PyQt5.QtWidgets import QVBoxLayout, QLabel

from gui.widgets.publisher import PostPublisher

class StreamView(QWidget):
	requestMorePosts = pyqtSignal();
	requestNewPosts = pyqtSignal();
	def  __init__(self, aspectsModel=None, parent=None):
		QWidget.__init__(self, parent=parent);

		layout = QVBoxLayout(self);
		layout.setContentsMargins(15, 0, 15, 0);
		layout.setSpacing(15);

		self.__postViews = [];
		self._publisher = None;
		self._indexOffset = 0;

		self.__topLoadingWidget = None;
		self.__bottomLoadingWidget = None;

		self.__sizePolicy = QSizePolicy(
					QSizePolicy.Maximum,
					QSizePolicy.Maximum);

		# TODO TMP?
		self._contextMenu = QMenu(self);
		self._contextMenu.addAction("Update stream", self.requestNewPosts);
		self._contextMenu.addAction("Clear stream", self.clearPostViews);


	def publisher(self): return self._publisher;

	def deletePublisher(self):
		self._publisher.deleteLater();
		self._indexOffset -= 1;
		self._publisher = None;

	def addPublisher(self, userModel):
		self._indexOffset += 1;
		self._publisher = PostPublisher(userModel, parent=self);
		self.layout().insertWidget(self._indexOffset, self._publisher);

	def mousePressEvent(self, event):
		if event.button() == Qt.RightButton:
			self._contextMenu.exec_(event.globalPos());

	def post(self, index):
		if index > -1 and index < len(self.__postViews):
			return self.__postViews[index];

	def posts(self): return self.__postViews;

	def clearPostViews(self):
		for view in self.__postViews:
			view.deleteLater();
			view.setParent(None);

	def delPost(self, view):
		self.__postViews.pop(self.__postViews.index(view));

	def addPostView(self, postView, index=None):
		if index != None:
			index = index + self._indexOffset;
			self.__postViews.insert(index, postView);
			self.layout().insertWidget(index, postView);
		else:
			self.__postViews.append(postView);
			self.layout().addWidget(postView);
			index = len(self.__postViews) - 1;
		postView.destroyed.connect(lambda nullptr, view=postView: self.delPost(view), Qt.UniqueConnection);

	def startTopLoading(self):
		if not self.__topLoadingWidget:
			self.__topLoadingWidget = QLabel("Loading, please wait..", self);
			self.layout().insertWidget(self._indexOffset, self.__topLoadingWidget, 0, Qt.AlignCenter);

	def stopTopLoading(self):
		if self.__topLoadingWidget:
			self.__topLoadingWidget.setParent(None);
			self.__topLoadingWidget = None;

	def startBottomLoading(self):
		if not self.__bottomLoadingWidget:
			self.__bottomLoadingWidget = QLabel("Loading, please wait..", self);
			self.layout().addWidget(self.__bottomLoadingWidget, 0, Qt.AlignCenter);

	def stopBottomLoading(self):
		if self.__bottomLoadingWidget:
			self.__bottomLoadingWidget.setParent(None);
			self.__bottomLoadingWidget = None;

class TagsStreamView(StreamView):
	followButtonClicked = pyqtSignal();
	def __init__(self, parent=None):
		StreamView.__init__(self, parent=parent);

		self.__headerLayout = QHBoxLayout();
		self.layout().addLayout(self.__headerLayout);

		self.__titleWidget = QLabel(self);
		self.__titleWidget.setWordWrap(True);
		self.__headerLayout.addWidget(self.__titleWidget);

		self.__followTagButton = QPushButton(self);
		self.__followTagButton.setObjectName("TagsFollowButton");
		self.__followTagButton.clicked.connect(self.followButtonClicked);
		self.__followTagButton.hide();
		self.__headerLayout.addWidget(self.__followTagButton, 0, Qt.AlignRight);
		self._indexOffset += 1;

	def disableInteractions(self):
		self.__followTagButton.setEnabled(False);

	def enableInteractions(self):
		self.__followTagButton.setEnabled(True);

	def setTag(self, tag=None, following=False):
		if tag:
			self.setTitle("#{0}".format(tag));
			if following:
				self.__followTagButton.setText("Following #{0}".format(tag));
				self.__followTagButton.setProperty("following", True);
			else:
				self.__followTagButton.setText("Follow #{0}".format(tag));
				self.__followTagButton.setProperty("following", False);
			self.__followTagButton.style().unpolish(self.__followTagButton);
			self.__followTagButton.style().polish(self.__followTagButton);
			self.__followTagButton.show();
		else:
			self.setTitle("Tags");
			self.__followTagButton.hide();

	def setTitle(self, title):
		self.__titleWidget.setText("<h2>{0}</h2>".format(title));
