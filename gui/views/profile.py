"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QFrame, QLabel, QPushButton, QHBoxLayout, QVBoxLayout, QSizePolicy
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt

from PyQt5.QtWidgets import QVBoxLayout, QLabel
from PyQt5.QtGui import QPixmap

from gui.views.user import UserNameView
from gui.widgets.markdownViewer import MarkdownViewer
from gui.widgets.avatar import AvatarWidget
from gui.widgets.image import Image

class ProfileView(QFrame):
	anchorClicked = pyqtSignal(str);
	def __init__(self, parent=None):
		QFrame.__init__(self, parent=parent);

		"""
				AVATAR
		Name
		DiasporaID
		[Tags]

		Label:Bio
		MarkdownViewer

		Label:Location
		Location

		Label: Birthsday
		Birthsday

		

		

		"""
