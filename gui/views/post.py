"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QShortcut, QWidget, QFrame, QLabel, QPushButton, QHBoxLayout, QVBoxLayout, QGridLayout, QSizePolicy, QMenu, QScrollArea, QSpacerItem
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QSize
from PyQt5.QtGui import QPixmap, QIcon, QKeySequence

from core.translator import Translator
def translate(string): return Translator.translate(string, "post");
_ = translate;

from gui.views.user import UserNameView
from gui.widgets.markdownViewer import MarkdownViewer
from gui.widgets.avatar import AvatarWidget
from gui.widgets.image import Image
from gui.widgets.poll import Poll
from gui.widgets.layouts import FlowLayout
from gui.widgets.publisher import MinimalPublisher


class CommentView(QFrame):
	anchorClicked = pyqtSignal(str);
	deleteRequest = pyqtSignal();
	def __init__(self, ownComment=False, parent=None):
		QFrame.__init__(self, parent=parent);

		layout = QHBoxLayout(self); # avatar - contentLayout
		layout.setContentsMargins(0, 0, 0, 0);
		layout.setSpacing(0);

		self._avatar = AvatarWidget(parent=self);
		layout.addWidget(self._avatar, 0, Qt.AlignTop);

		contentLayout = QVBoxLayout(); # contentTopLayout | content | timeago
		contentLayout.setContentsMargins(0, 0, 0, 0);
		contentLayout.setSpacing(0);
		layout.addLayout(contentLayout);

		contentTopLayout = QHBoxLayout(); # username - options
		contentTopLayout.setContentsMargins(15, 0, 0, 0);
		contentTopLayout.setSpacing(0);
		contentLayout.addLayout(contentTopLayout);

		self._authorNameWidget = UserNameView(parent=self);
		contentTopLayout.addWidget(self._authorNameWidget, 1);

		self._deleteButton = None;
		if ownComment:
			self._deleteButton = OptionButton(parent=self);
			self._deleteButton.setToolTip(_("Delete"));
			icon = QIcon();
			icon.addPixmap(QPixmap(":/icons/24x24/delete"), QIcon.Normal, QIcon.Off)
			self._deleteButton.setIconSize(QSize(24, 24));
			self._deleteButton.setIcon(icon);
			self._deleteButton.clicked.connect(self.__deleteButtonClicked);
			contentTopLayout.addWidget(self._deleteButton, 0, Qt.AlignRight);
		else:
			self._reportButton = OptionButton(parent=self);
			self._reportButton.setToolTip(_("Report"));
			icon = QIcon();
			icon.addPixmap(QPixmap(":/icons/24x24/report"), QIcon.Normal, QIcon.Off)
			self._reportButton.setIconSize(QSize(24, 24));
			self._reportButton.setIcon(icon);
			contentTopLayout.addWidget(self._reportButton, 0, Qt.AlignRight);

		self._markdownView = MarkdownViewer();
		self._markdownView.anchorClicked.connect(self.anchorClicked);
		contentLayout.addWidget(self._markdownView);

		self._timeAgoWidget = TimeAgoWidget();
		contentLayout.addWidget(self._timeAgoWidget);

	def __deleteButtonClicked(self):
		# TODO Confirm
		self.deleteRequest.emit();

	def avatarWidget(self): return self._avatar;
	def authorNameWidget(self): return self._authorNameWidget;
	def timeAgoWidget(self): return self._timeAgoWidget;
	def markdownView(self): return self._markdownView;

class CommentsView(QFrame):
	anchorClicked = pyqtSignal(str);
	collapseStateChanged = pyqtSignal(bool); # state
	def __init__(self, parent=None):
		QFrame.__init__(self, parent=parent);

		QVBoxLayout(self);
		self.layout().setContentsMargins(0, 0, 0, 0);
		self.layout().setSpacing(0);
		self._commentsLayout = QVBoxLayout();
		self._commentsLayout.setContentsMargins(0, 0, 0, 0);
		self._commentsLayout.setSpacing(3);
		self._items = [];
		self._count = 0;
		self._collapseState = True;
		self._toggleButton = QPushButton("show more/less", self);
		self._toggleButton.setObjectName("ToggleButton");
		self._toggleButton.clicked.connect(self._toggleCollapseState);
		sizePolicy = QSizePolicy(
					QSizePolicy.Maximum,
					QSizePolicy.Maximum);
		self._toggleButton.setSizePolicy(sizePolicy);
		self.layout().addLayout(self._commentsLayout);
		self.layout().addWidget(self._toggleButton, 0, Qt.AlignRight);

	def comments(self): return self._items;

	def comment(self, index): return self._items[index];

	def delCommentView(self, commentView):
		self.delCommentIndex(self._items.index(commentView));

	def delCommentIndex(self, index):
		self._items[index].deleteLater();
		del self._items[index];

	def addComment(self, index=None, ownComment=False):
		comment = CommentView(ownComment=ownComment, parent=self);
		if index != None:
			self._items.insert(index, comment);
			self._commentsLayout.insertWidget(index, comment);
		else:
			self._items.append(comment);
			self._commentsLayout.addWidget(comment);
			index = len(self._items) - 1;
		comment.anchorClicked.connect(self.anchorClicked);
		return index;

	def _toggleCollapseState(self):
		if self.collapseState(): self._expand()
		else: self._collapse();

	def _expand(self):
		self._collapseState = False;
		self.collapseStateChanged.emit(False);

	def _collapse(self):
		self._collapseState = True;
		self.collapseStateChanged.emit(True);

	def collapseState(self): return self._collapseState;

	def updateCollapseButton(self, collapseCount=3):
		count = len(self._items);
		if self._count > collapseCount and count:
			diff = self._count - count;
			if diff:
				self._toggleButton.setText(_("Show {0} more comments.").format(diff));
			else: self._toggleButton.setText(_("Show less comments."));
			self._toggleButton.show();
		else:
			self._toggleButton.hide();

	def setCount(self, count):
		self._count = count;
		self.updateCollapseButton();

class AvatarListView(QFrame):
	requestMore = pyqtSignal(); # TODO maybe better name for this.
	def __init__(self, parent=None):
		QFrame.__init__(self, parent=parent);
		FlowLayout(horizontalSpacing=3, verticalSpacing=3, parent=self);

		sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum);
		sizePolicy.setVerticalStretch(1);
		sizePolicy.setHorizontalStretch(1);
		self.setSizePolicy(sizePolicy);

		self._showButton = QPushButton("+", self);
		self.layout().addWidget(self._showButton);
		self._showButton.clicked.connect(self.requestMore);

	def hide(self):
		for index in range(0,self.layout().count()):
			self.layout().itemAt(index).widget().hide();

	def show(self):
		for index in range(0,self.layout().count()):
			self.layout().itemAt(index).widget().show();

	def avatar(self, index):
		return self.layout().itemAt(index).widget();

	def addAvatar(self):
		if self._showButton:
			self._showButton.setParent(None);
			self._showButton = None;
		avatar = AvatarWidget(maxHeight=32, parent=self);
		self.layout().addWidget(avatar);
		return self.layout().count() - 1;

class Interaction(QWidget):
	requestMore = pyqtSignal(); # TODO maybe better name for this.
	def __init__(self, text, avatars=True, parent=None):
		QWidget.__init__(self, parent=parent);

		FlowLayout(horizontalSpacing=3, verticalSpacing=3, parent=self);

		self.__text = text;
		self.__count = 0;
		self.__avatarItems = [];
		self.__avatarsAllowed = avatars;
		self.__labelWidget = QLabel(text, parent=self);
		self.__labelWidget.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum);

		self._showButton = None;
		self.layout().addWidget(self.__labelWidget);

	def avatar(self, index): return self.__avatarItems[index];

	@pyqtSlot(int)
	def __delAvatar(self, index): self.__avatarItems.pop(index);

	def addAvatar(self):
		if self._showButton:
			self._showButton.setParent(None);
			self._showButton = None;
		avatar = AvatarWidget(maxHeight=32, parent=self);
		
		self.__avatarItems.append(avatar);
		index = len(self.__avatarItems) - 1;
		avatar.destroyed.connect(lambda nullptr: self.__delAvatar(index));
		self.layout().addWidget(avatar);
		return index;

	def requestMoreFailed(self):
		# TODO this isn't called yet.
		self._showButton.setEnabled(True);

	def __requestMore(self):
		self._showButton.setEnabled(False);
		self.requestMore.emit();

	def updateCount(self, count):
		if (self.__avatarsAllowed and int(count) > 0
			and not self.__avatarItems and not self._showButton):
			self._showButton = QPushButton("+", self);
			self._showButton.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum);
			self._showButton.setMinimumWidth(16);
			self._showButton.setMaximumWidth(16);
			self._showButton.setMinimumHeight(16);
			self._showButton.setMaximumHeight(16);
			self.layout().addWidget(self._showButton);
			self._showButton.clicked.connect(self.__requestMore);
		self.__count = count;
		text = "{0} {1}".format(self.__count, self.__text);
		self.__labelWidget.setText(text);

	def hide(self):
		self.__labelWidget.hide();
		if self.__avatarItems:
			for avatar in self.__avatarItems: avatar.hide();

	def show(self):
		self.__labelWidget.show();
		if self.__avatarItems:
			for avatar in self.__avatarItems: avatar.show();

class InteractionsView(QWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent=parent);

		"""
		 _VBoxLayout_____________________________
		|  _HBoxLayout___________________________
		| | RESHARES - COUNT - [AVATAR, AVATAR]
		| |______________________________________
		|  _HBoxLayout___________________________
		| | LIKES - COUNT - [AVATAR, AVATAR]
		| |______________________________________
		|  _HBoxLayout___________________________
		| | COMMENTS - COUNT - [AVATAR, AVATAR]
		| |______________________________________
		"""
		layout = QVBoxLayout(self);
		layout.setContentsMargins(15, 0, 15, 0);
		layout.setSpacing(15);

		self.__reshares = Interaction(_("person(s) reshared this."), parent=self);
		layout.addWidget(self.__reshares,1);
		self.__likes = Interaction(_("person(s) liked this."), parent=self);
		layout.addWidget(self.__likes,1);
		self.__comments = Interaction(_("comments."), avatars=False, parent=self);
		layout.addWidget(self.__comments,1);

		self.__likes.hide();
		self.__reshares.hide();
		self.__comments.hide();

	def likeAvatars(self): return self.__likes;
	def reshareAvatars(self): return self.__reshares;

	def setLikeCount(self, count):
		if int(count):
			self.__likes.updateCount(count);
			self.__likes.show();
		else: self.__likes.hide();
		
	def setReshareCount(self, count):
		if int(count):
			self.__reshares.updateCount(count);
			self.__reshares.show();
		else: self.__reshares.hide();

	def setCommentCount(self, count):
		if int(count):
			self.__comments.updateCount(count);
			self.__comments.show();
		else: self.__comments.hide();

class InteractionButton(QPushButton):
	def __init__(self, text="", parent=None):
		QPushButton.__init__(self, text, parent);
		sizePolicy = QSizePolicy(
					QSizePolicy.Maximum,
					QSizePolicy.Maximum);
		self.setSizePolicy(sizePolicy);

class OptionButton(QPushButton):
	def __init__(self, text="", parent=None):
		QPushButton.__init__(self, text, parent);
		sizePolicy = QSizePolicy(
					QSizePolicy.Maximum,
					QSizePolicy.Maximum);
		self.setSizePolicy(sizePolicy);
		self.setMaximumWidth(32);
		self.setMaximumHeight(32);
		self.setFlat(True);

class OptionsLayout(QHBoxLayout):
	def __init__(self, deleteAble=False, parent=None):
		QHBoxLayout.__init__(self);

		self._participationButton = OptionButton(parent=parent); # toggle notifications for this post.
		icon = QIcon();
		icon.addPixmap(QPixmap(":/icons/24x24/notification_post"), QIcon.Normal, QIcon.Off);
		self._participationButton.setIconSize(QSize(24, 24));
		self._participationButton.setIcon(icon);
		self.addWidget(self._participationButton, 0);

		self._reportButton = OptionButton(parent=parent); # report this post.
		icon = QIcon();
		icon.addPixmap(QPixmap(":/icons/24x24/report"), QIcon.Normal, QIcon.Off);
		self._reportButton.setIconSize(QSize(24, 24));
		self._reportButton.setIcon(icon);
		self.addWidget(self._reportButton, 0);

		self._blockButton = OptionButton(parent=parent); # block post author
		icon = QIcon();
		icon.addPixmap(QPixmap(":/icons/24x24/block"), QIcon.Normal, QIcon.Off);
		self._blockButton.setIconSize(QSize(24, 24));
		self._blockButton.setIcon(icon);
		self.addWidget(self._blockButton, 0);

		self._hideButton = OptionButton(parent=parent); # hide post from stream
		icon = QIcon();
		icon.addPixmap(QPixmap(":/icons/24x24/hide"), QIcon.Normal, QIcon.Off);
		self._hideButton.setIconSize(QSize(24, 24));
		self._hideButton.setIcon(icon);
		self.addWidget(self._hideButton, 0);

	def setParticipated(self, state=True):
		icon = QIcon();
		if state:
			icon.addPixmap(QPixmap(":/icons/24x24/notification_post"), QIcon.Normal, QIcon.Off);
		else:
			icon.addPixmap(QPixmap(":/icons/24x24/notification_post_active"), QIcon.Normal, QIcon.Off);

		self._participationButton.setIconSize(QSize(24, 24));
		self._participationButton.setIcon(icon);

	def participationButton(self): return self._participationButton;
	def reportButton(self): return self._reportButton;
	def blockButton(self): return self._blockButton;
	def hideButton(self): return self._hideButton;

from core.formatter import Formatter
class TimeAgoWidget(QLabel):
	def __init__(self, parent=None):
		QLabel.__init__(self, parent);

	def setDatetime(self, datetime):
		self.setText(Formatter.tago(datetime));
		self.setToolTip(datetime);

class ScopeLabel(QLabel):
	def __init__(self, public=True, providerName="", parent=None):
		QLabel.__init__(self, parent);
		self._public = public;
		self._providerName = providerName;
		self.__updateLabel();

	def __updateLabel(self):
		str = "";
		if self._public: str += _("Public");
		else: str += _("Private");
		if self._providerName: str+= _(" via {0}").format(self._providerName);
		self.setText(str);

	def __setProviderName(self, name):
		self._providerName = name;

	def __setPublic(self, state):
		self._public = state;

	def set(self, public=True, providerName=""):
		self._public = public;
		self._providerName = providerName;
		self.__updateLabel();

class Thumb(Image):
	clicked = pyqtSignal();
	def __init__(self, index, path="", title="", alt="", url="", fixedHeight=100, disableAnimation=True, parent=None):
		Image.__init__(self, path=path, title=title, alt=alt, url=url, fixedHeight=fixedHeight, disableAnimation=disableAnimation, parent=parent);
		self.setStyleSheet("background-color: orange;");
		self._index = index;

	def index(self): return self._index;

	def enterEvent(self, event):
		self.setCursor(Qt.PointingHandCursor);
		self.setProperty("hover", True);
		self.style().unpolish(self);
		self.style().polish(self);

	def leaveEvent(self, event):
		self.setCursor(Qt.ArrowCursor);
		self.setProperty("hover", False);
		self.style().unpolish(self);
		self.style().polish(self);

	def mousePressEvent(self, event):
		if event.button() == Qt.LeftButton: self.clicked.emit();

class PhotoBrowserView(QFrame):
	anchorClicked = pyqtSignal(str);
	def __init__(self, parent=None):
		QFrame.__init__(self, parent=parent);
		"""	 ___________________
			|<-->  Photo  <-->
			|___________________
			|Avatar |	Name
			|		|	TimeAgo
			|___________________
			[	thumbs			]
		"""

		self._thumbs = [];

		layout = QVBoxLayout(self);
		#metaLayout = QHBoxLayout(); # avatar - metaSubLayout
		#metaSubLayout = QVBoxLayout(); # Name | TimeAgo
		centerPhotoLayout = QHBoxLayout(); # spacer - photo - spacer
		self._photo = Image(parent=self);
		#self._avatar = AvatarWidget(parent=self);
		#self._authorNameWidget = UserNameView(parent=self);
		#self._timeAgoWidget = TimeAgoWidget(parent=self);
		self._thumbArea = None;
		self._thumbWidget = None;
		self._thumbHeight = 100;

		layout.setAlignment(Qt.AlignHCenter);
		self._photo.setSizePolicy(QSizePolicy.Expanding,
									QSizePolicy.Preferred);

		layout.addLayout(centerPhotoLayout);
		#layout.addLayout(metaLayout);
		
		centerPhotoLayout.addSpacerItem(QSpacerItem(1,1,QSizePolicy.Preferred,QSizePolicy.Preferred));
		centerPhotoLayout.addWidget(self._photo);
		centerPhotoLayout.addSpacerItem(QSpacerItem(1,1,QSizePolicy.Preferred,QSizePolicy.Preferred));
		#metaLayout.addWidget(self._avatar);
		#metaLayout.addLayout(metaSubLayout);
		#metaSubLayout.addWidget(self._authorNameWidget);
		#metaSubLayout.addWidget(self._timeAgoWidget);

	def __initThumbs(self):
		centerThumbsLayout = QHBoxLayout(); # spacer - thumbArea - spacer
		self._thumbArea = QScrollArea(parent=self);
		self._thumbArea.setFrameShape(QFrame.NoFrame);
		self._thumbArea.setAutoFillBackground(True);
		self._thumbArea.setWidgetResizable(True);
		self._thumbArea.setSizePolicy(QSizePolicy.Preferred,
										QSizePolicy.Preferred);
		height = self._thumbHeight;
		height += self._thumbArea.horizontalScrollBar().height();
		self._thumbArea.setMinimumHeight(height);
		self._thumbArea.setMaximumHeight(height);
		self._thumbWidget = QWidget(parent=self._thumbArea);
		self._thumbWidget.setAutoFillBackground(True);
		self._thumbWidget.setSizePolicy(QSizePolicy.Expanding,
										QSizePolicy.Fixed);

		thumbLayout = QHBoxLayout(self._thumbWidget);
		thumbLayout.setContentsMargins(0, 0, 0, 0);
		thumbLayout.setSpacing(10);
		thumbLayout.setAlignment(Qt.AlignCenter);
		self.layout().addLayout(centerThumbsLayout);
		centerThumbsLayout.addSpacerItem(QSpacerItem(1,1,QSizePolicy.Preferred,QSizePolicy.Preferred));
		centerThumbsLayout.addWidget(self._thumbArea, 1);
		centerThumbsLayout.addSpacerItem(QSpacerItem(1,1,QSizePolicy.Preferred,QSizePolicy.Preferred));
		self._thumbArea.setWidget(self._thumbWidget);

	def addThumb(self, url=""):
		"""Returns thumb index.
		"""
		if not self._thumbArea: self.__initThumbs();
		index = len(self._thumbs);
		thumb = Thumb(index, url=url, fixedHeight=self._thumbHeight, disableAnimation=True, parent=self._thumbWidget);
		self._thumbs.append(thumb);
		self._thumbWidget.layout().addWidget(thumb);
		return index;

	def thumb(self, index): return self._thumbs[index];
	def photoWidget(self): return self._photo;
	#def avatarWidget(self): return self._avatar;
	#def authorNameWidget(self): return self._authorNameWidget;
	#def timeAgoWidget(self): return self._timeAgoWidget;

	def stopAnimations(self):
		if self.photoWidget().isAnimation(): self.photoWidget().stopAnimation();

	def startAnimations(self):
		if self.photoWidget().isAnimation(): self.photoWidget().startAnimation();


class PostView(QFrame):
	requestUpdate = pyqtSignal();
	anchorClicked = pyqtSignal(str); # str
	def __init__(self, isReshare=False, fromReshare=False, hasPhotos=False, ownPost=False, reshareKwargs={}, parent=None):
		QWidget.__init__(self, parent=parent);
		self.__displayed = True;
		self.setAutoFillBackground(True);
		"""
		 _HBoxLayout_____________________________________
		| AVATAR |  _VBoxLayout__________________________	contentLayout
		|		 | |  _HBoxLayout________________________		contentTopLayout
		|		 | | |AUTHOR_NAME - TIME - OPTIONS
		|		 | | |___________________________________
		|		 | | POST_CONTENT
		|		 | |  _HBoxLayout________________________		feedbackLayout
		|		 | | |SCOPE - InteractionControlsView
		|		 | | |___________________________________
		|		 | |  _VBoxLayout_____________________________	InteractionsView
		|		 | | |  _HBoxLayout___________________________
		|		 | | | | | RESHARES - COUNT - [AVATAR, AVATAR]
		|		 | | | | |______________________________________
		|		 | | | |  _HBoxLayout___________________________
		|		 | | | | | LIKES - COUNT - [AVATAR, AVATAR]
		|		 | | | | |______________________________________
		|		 | | | |  _HBoxLayout___________________________
		|		 | | | | | COMMENTS - COUNT - [AVATAR, AVATAR]
		|		 | | | | |______________________________________
		|		 | |  _VBoxLayout________________________		commentsLayout
		|		 | | |COMMENTS
		|		 | | |
		
		"""
		layout = QHBoxLayout(self);
		layout.setContentsMargins(3, 3, 3, 3);
		layout.setSpacing(3);

		# Avatar on the left
		sizePolicy = QSizePolicy(
					QSizePolicy.Maximum,
					QSizePolicy.Maximum);
		self._avatarWidget = AvatarWidget(parent=self);
		self._avatarWidget.anchorClicked.connect(self.anchorClicked);
		layout.addWidget(self._avatarWidget, 0, Qt.AlignTop);

		# Content layout on the right
		self._contentLayout = QVBoxLayout();
		self._contentLayout.setContentsMargins(0, 0, 0, 0);
		self._contentLayout.setSpacing(0);
		layout.addLayout(self._contentLayout);

		# Top layout (contentheader)
		self.__contentTopLayout = QHBoxLayout();
		self.__contentTopLayout.setContentsMargins(0, 0, 0, 0);
		self.__contentTopLayout.setSpacing(0);
		self._contentLayout.addLayout(self.__contentTopLayout);

		self.authorNameWidget = UserNameView(parent=self);
		self.__contentTopLayout.addWidget(self.authorNameWidget, 1);

		self.timeAgoWidget = TimeAgoWidget(parent=self);
		self.__contentTopLayout.addWidget(self.timeAgoWidget);

		self._deleteButton = None;
		self._optionsLayout = None;

		self._reshareView = None;
		self._markdownViewer = None;
		self._photosView = None;
		self._poll = None;
		self._publisher = None;
		self._fromReshare = fromReshare;

		if hasPhotos:
			self._photosView = PhotoBrowserView();
			self._photosView.anchorClicked.connect(self.anchorClicked);
			self._contentLayout.addWidget(self._photosView);

		if isReshare:
			self._reshareView = PostView(fromReshare=True, **reshareKwargs);
			self._reshareView.anchorClicked.connect(self.anchorClicked);
			self._contentLayout.addWidget(self._reshareView);
		else:
			self._markdownViewer = MarkdownViewer(parent=self);
			self._markdownViewer.anchorClicked.connect(self.anchorClicked);
			#self._markdownViewer.setWordWrap(True);
			self._contentLayout.addWidget(self._markdownViewer);

		# Feedback layout
		if not fromReshare:
			if ownPost:
				self._deleteButton = OptionButton(parent=parent); # delete post
				icon = QIcon();
				icon.addPixmap(QPixmap(":/icons/24x24/delete"), QIcon.Normal, QIcon.Off)
				self._deleteButton.setIconSize(QSize(24, 24));
				self._deleteButton.setIcon(icon);
				self.__contentTopLayout.addWidget(self._deleteButton, 0);
			else:
				self._optionsLayout = OptionsLayout();
				self.__contentTopLayout.addLayout(self._optionsLayout);

			self.__feedbackLayout = QHBoxLayout();
			self._contentLayout.addLayout(self.__feedbackLayout);

			self.scopeWidget = ScopeLabel(parent=self);
			self.__feedbackLayout.addWidget(self.scopeWidget);

			self._likeButton = InteractionButton(_("Like"), parent=self);
			self.__feedbackLayout.addWidget(self._likeButton);
		
			self._reshareButton = InteractionButton(_("Reshare"), parent=self);
			self.__feedbackLayout.addWidget(self._reshareButton);

			self._commentButton = InteractionButton(_("Comment"), parent=self);
			self.__feedbackLayout.addWidget(self._commentButton);

			# InteractionsView
			self.__interactionsView = None;
			self.__interactionsView = InteractionsView();
			self._contentLayout.addWidget(self.__interactionsView);

			self.__commentsView = CommentsView();
			self.__commentsView.anchorClicked.connect(self.anchorClicked);
			self._contentLayout.addWidget(self.__commentsView);

			# Context menu
			self._contextMenu = QMenu(self);
			self._contextMenu.addAction(_("Update post"), self.__requestUpdate);
			#self._contextMenu.addAction("Hide post", self.hide);

		self.__animationsToggle = False;

	def toggleState(self):
		""" Toggles collapse/nsfw
		"""
		if self.markdownView():
			if self.markdownView().collapsed(): self.markdownView().expand();
			else: self.markdownView().collapse();

	def toggleAnimations(self):
		if self.__animationsToggle:
			if self.markdownView(): self.markdownView().stopAnimations();
			if self.photosView(): self.photosView().stopAnimations();
			self.__animationsToggle = False;
		else:
			if self.markdownView(): self.markdownView().startAnimations();
			if self.photosView(): self.photosView().startAnimations();
			self.__animationsToggle = True;

	def publisher(self): return self._publisher;

	def deletePublisher(self):
		self._publisher.deleteLater();
		self._publisher = None;

	def addPublisher(self, userModel):
		self._publisher = MinimalPublisher(userModel, parent=self);
		self._contentLayout.addWidget(self._publisher);

	def setSelected(self, state):
		self.setProperty("selected", state);
		self.style().unpolish(self);
		self.style().polish(self);
		#self.setFocus();

	def delete(self):
		self.deleteLater();
		self.setParent(None);

	@pyqtSlot()
	def __requestUpdate(self): self.requestUpdate.emit();

	def __onContextMenu(self, event):
		self._contextMenu.exec_(event.globalPos());

	def mousePressEvent(self, event):
		if event.button() == Qt.RightButton:
			if not self._fromReshare: self.__onContextMenu(event);

	def setLiked(self, state=True):
		if state: self._likeButton.setText(_("Liked"));
		else: self._likeButton.setText(_("Like"));

	def setReshared(self, state=True):
		if state: self._reshareButton.hide();
		else: self._reshareButton.show();

	def setParticipated(self, state=True):
		if self._optionsLayout: self._optionsLayout.setParticipated(state);

	def setDisplayed(self, state):
		"""If True then this view is visible in the viewport
		"""
		self.__displayed = state;
		if not state:
			if self.markdownView(): self.markdownView().stopAnimations();
			if self.photosView(): self.photosView().stopAnimations();
		if self.reshareView(): self.reshareView().setDisplayed(state);


	def addPoll(self, pollData, answer=None):
		self._poll = Poll(pollData, answer=answer, parent=self);
		self._contentLayout.insertWidget(1, self._poll);

	def pollWidget(self): return self._poll;


	def interactionsView(self): return self.__interactionsView;
	def commentsView(self): return self.__commentsView;
	def photosView(self): return self._photosView;

	def updateInteractionData(self, data):
		"""Update view by data
		"""
		self.interactionsView.updateData(data["interactions"]);

	def isReshare(self):
		if self.reshareView(): return True;
		return False;

	def reshareView(self): return self._reshareView;
	def markdownView(self): return self._markdownViewer;

	def avatarWidget(self): return self._avatarWidget;

	# Option buttons
	def deleteButton(self): return self._deleteButton;
	def participationButton(self):
		if self._optionsLayout:
			return self._optionsLayout.participationButton();
	def reportButton(self):
		if self._optionsLayout:
			return self._optionsLayout.reportButton();
	def hideButton(self):
		if self._optionsLayout:
			return self._optionsLayout.hideButton();
	def blockButton(self):
		if self._optionsLayout:
			return self._optionsLayout.blockButton();

	# Feedback buttons
	def likeButton(self): return self._likeButton;
	def reshareButton(self): return self._reshareButton;
	def commentButton(self): return self._commentButton;
