"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QWidget

from gui.widgets.wraptext import LineWrappedText

class UserNameView(LineWrappedText):
	def __init__(self, text="", parent=None):
		LineWrappedText.__init__(self, text=text, parent=parent);

	def setText(self, text):
		self.setToolTip(text);
		LineWrappedText.setText(self, text);

	def enterEvent(self, event):
		LineWrappedText.enterEvent(self, event);

	def leaveEvent(self, event):
		LineWrappedText.leaveEvent(self, event);

class UserAvatarView(QWidget):
	
	def __init__(self, parent=None):
		QWidget.__init__(self, parent=parent);

class HoverCardView(QWidget):
	
	def __init__(self, parent=None):
		QWidget.__init__(self, parent=parent);
