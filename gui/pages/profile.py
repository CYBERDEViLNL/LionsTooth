"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QLabel
from PyQt5.QtCore import Qt

from controllers.pageController import PageProto

from gui.views.stream import StreamView
from gui.views.profile import ProfileView

class ProfilePage(PageProto):
	def __init__(self, parent=None):
		PageProto.__init__(self, parent=parent);

		self.titleWidget = QLabel("<h2>Profile Page</h2>", self);
		self.addWidget(self.titleWidget, 0, Qt.AlignCenter);

		tmp = QLabel("<h2>Underdestruction, comming soon ;-)</h2>", self);
		self.addWidget(tmp, 0, Qt.AlignCenter);


		self.profileView = ProfileView();
		self.addWidget(self.profileView);

		self.streamView = StreamView();
		self.addWidget(self.streamView);

		self.verticalScrollBar().valueChanged.connect(self.__scrollChanged);

	def __scrollChanged(self, value):
		onePercent =  self.verticalScrollBar().maximum() / 100;
		if onePercent: # don't want ZeroDivisionError
			if round(value / onePercent) > 95: pass;#self.more_posts()

		# Stop animations and start/stop timers
		for view in self.streamView.posts():
			if not view.visibleRegion().boundingRect():
				view.setDisplayed(False);
			else: view.setDisplayed(True);
