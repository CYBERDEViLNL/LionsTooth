"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt
from PyQt5.QtWidgets import QWidget, QComboBox, QCheckBox, QSpinBox, QHBoxLayout, QLineEdit, QGroupBox, QSizePolicy, QFormLayout, QLabel, QPushButton

import keyring # python-dbus and/or python-secretstorage is also a requirement https://keyring.readthedocs.io/en/latest/
import re
from enum import Enum

import importlib # to check if socks module is installed.

from core.translator import Translator
def _(string): return Translator.translate(string, "settings");

from controllers.pageController import PageProto

class ConnectionSettingsFields(Enum):
	username	= "username";
	password	= "password";
	pod			= "pod";

class ValidationPatterns(Enum):
	username = r"^[a-zA-Z0-9_]{2,255}$";
	email = r"^([a-zA-Z0-9_\-]{1,100}(\.)?)+\@([a-zA-Z0-9_\-]{1,100}\.)+[a-zA-Z]{2,13}$";
	password = r"^.{6,255}$";
	pod = r"^([a-zA-Z0-9_\-]{1,100}\.)+[a-zA-Z]{2,13}$"; # TODO

class UrlWidget(QWidget):
	urlChanged = pyqtSignal();
	def __init__(self, schemes=['https', 'http'], parent=None):
		QWidget.__init__(self, parent=parent);

		self.__schemes = [];

		layout = QHBoxLayout(self);
		self.layout().setContentsMargins(0, 0, 0, 0);

		self.__schemeWidget = QComboBox(self);
		self.setSchemes(schemes);
		self.__schemeWidget.currentIndexChanged.connect(self.__schemeChanged);
		layout.addWidget(self.__schemeWidget);

		self.__netlocWidget = QLineEdit(self);
		self.__netlocWidget.setPlaceholderText(_("pod.example.com"));
		self.__netlocWidget.textEdited.connect(self.__netlocChanged);
		layout.addWidget(self.__netlocWidget);

	def netlocValid(self):
		return self.__netlocWidget.property("valid");

	def __validateNetloc(self, text):
		test = re.match(ValidationPatterns.pod.value, text);
		if test or self.__isIp(text): return True;
		return False;

	def __isIp(self, text):
		ipSegments = text.split(".");
		if len(ipSegments) == 4:
			if (ipSegments[0].isnumeric() and int(ipSegments[0]) in range(1,255)
				and ipSegments[1].isnumeric() and int(ipSegments[1]) in range(0,255)
				and ipSegments[2].isnumeric() and int(ipSegments[2]) in range(0,255)
				and ipSegments[3].isnumeric() and int(ipSegments[3]) in range(1,255)):
				return True;
		return False

	def __schemeChanged(self, index):
		self.urlChanged.emit();

	def __netlocChanged(self, text):
		if self.__validateNetloc(text):
			self.__netlocWidget.setProperty("valid", True);
		else: self.__netlocWidget.setProperty("valid", False);
		self.__netlocWidget.style().unpolish(self.__netlocWidget);
		self.__netlocWidget.style().polish(self.__netlocWidget);
		self.urlChanged.emit();

	def netloc(self): return self.__netlocWidget.text();
	def scheme(self): return self.__schemeWidget.currentText();

	def url(self): return "{0}://{1}".format(self.scheme(), self.netloc());

	def setNetloc(self, netloc):
		self.__netlocWidget.setText(netloc);
		if self.__validateNetloc(netloc):
			self.__netlocWidget.setProperty("valid", True);

	def setSchemes(self, schemeList):
		self.__schemes = schemeList;
		self.__schemeWidget.clear();
		self.__schemeWidget.addItems(self.__schemes);

	def setScheme(self, scheme):
		if scheme in self.__schemes:
			self.__schemeWidget.setCurrentIndex(self.__schemes.index(scheme));

class ConnectionSettings(QGroupBox):
	requestSaveConfig = pyqtSignal();
	requestLogin = pyqtSignal();
	requestLogout = pyqtSignal();

	def __init__(self, config, parent=None):
		QGroupBox.__init__(self, parent=parent);

		self.__config = config; # ConfigParser SectionProxy object.

		self.setTitle(_("Connection Settings"));

		self.setSizePolicy(QSizePolicy.Expanding,
						QSizePolicy.Maximum);

		layout = QFormLayout(self);
		layout.setLabelAlignment(Qt.AlignRight)
		self.layout().setContentsMargins(0, 0, 0, 0);

		# Pod
		self.__podEdit = UrlWidget(parent=self);
		if self.__config.get("scheme", False):
			self.__podEdit.setScheme(self.__config.get("scheme", ""));
		if self.__config.get("netloc", False):
			self.__podEdit.setNetloc(self.__config.get("netloc", ""));
		self.layout().setWidget(0, QFormLayout.LabelRole,
								QLabel(_("Pod"), self));
		self.layout().setWidget(0, QFormLayout.FieldRole,
								self.__podEdit);
		self.__openPodListButton = QPushButton(_("Open pod list"), self);
		self.layout().setWidget(1, QFormLayout.FieldRole,
								self.__openPodListButton);

		# Username
		self.__usernameEdit = QLineEdit(self);
		self.__usernameEdit.setPlaceholderText("foo");
		if self.__config.get("username", False):
			self.__usernameEdit.setProperty("valid", True);
			self.__usernameEdit.setText(self.__config.get("username", ""));
		self.layout().setWidget(2, QFormLayout.LabelRole,
								QLabel(_("Username"), self));
		self.layout().setWidget(2, QFormLayout.FieldRole,
								self.__usernameEdit);

		# Password
		self.__passwordEdit = QLineEdit(self);
		self.__passwordEdit.setEchoMode(QLineEdit.Password);
		self.__passwordEdit.setPlaceholderText(_("Super secret password 123"));
		if self.__config.get("username", False): # Maybe move this to connectionController? and give this class a bool param on init to achieve the same.
			password = keyring.get_password("LionsTooth", self.__config['username']);
			if password: self.__passwordEdit.setProperty("valid", True);
			del password;
		self.layout().setWidget(3, QFormLayout.LabelRole,
								QLabel(_("Password"), self));
		self.layout().setWidget(3, QFormLayout.FieldRole,
								self.__passwordEdit);

		# Auto login
		self.__autoLoginCheckBox = QCheckBox(_("Automatic login"), self);
		self.__autoLoginCheckBox.setTristate(False);
		self.layout().setWidget(4, QFormLayout.FieldRole,
								self.__autoLoginCheckBox);
		self.__autoLoginCheckBox.setChecked(self.parseBool(
									self.__config.get("auto_login", "False")));

		self.__connectButton = QPushButton(_("Login"), self);
		self.layout().setWidget(5, QFormLayout.FieldRole,
								self.__connectButton);

		# Ignore SSL errors (should use for testing only!)
		self.__acceptUnsafeCheckBox = QCheckBox(_("Ignore SSL errors (Use for testing only!)"), self);
		self.__acceptUnsafeCheckBox.setTristate(False);
		self.__acceptUnsafeCheckBox.setChecked(not self.parseBool(
								self.__config.get("verify_ssl", "True")));
		self.layout().setWidget(6, QFormLayout.FieldRole,
								self.__acceptUnsafeCheckBox);

		# Save
		self.__saveButton = QPushButton(_("Save"), self);
		self.__saveButton.hide();
		self.layout().setWidget(7, QFormLayout.FieldRole,
								self.__saveButton);

		"""Advanced connection settings
		"""
		# Proxy
		self.__proxyEnableCheckBox = QCheckBox(_("Enable"), self);
		self.__proxyEnableCheckBox.setTristate(False);
		self.__proxyEnableCheckBox.setChecked(self.parseBool(
									self.__config.get("proxy_enabled", "False")));
		schemes = ["http", "https"];
		if importlib.util.find_spec("socks"): schemes = ["socks5h", "socks5"] + schemes;
		self.__proxyUrlWidget = UrlWidget(schemes=schemes, parent=self);
		self.__proxyPortWidget = QSpinBox(self);
		self.__proxyPortWidget.setMaximum(30000);
		self.__proxyPortWidget.setValue(8080);
		self.__proxyOnlyUnsafeCheckBox = QCheckBox(_("Proxy unsafe only"), self);
		self.__proxyOnlyUnsafeCheckBox.setTristate(False);
		self.__proxyOnlyUnsafeCheckBox.setToolTip(
			_("Proxy only http requests. If this is checked all https requests"
			" will NOT be proxied."));
		self.__proxyOnlyUnsafeCheckBox.setChecked(self.parseBool(
								self.__config.get("proxy_unsafe_only", "False")));

		self.__proxyUrlWidget.setScheme(self.__config.get("proxy_scheme", "https"));
		self.__proxyUrlWidget.setNetloc(self.__config.get("proxy_netloc", ""));
		self.__proxyPortWidget.setValue(int(self.__config.get("proxy_port", 8080)));

		self.layout().setWidget(8, QFormLayout.LabelRole,
								QLabel("Proxy", self));
		self.layout().setWidget(8, QFormLayout.FieldRole,
								self.__proxyEnableCheckBox);
		self.layout().setWidget(9, QFormLayout.LabelRole,
								QLabel("Address", self));
		self.layout().setWidget(9, QFormLayout.FieldRole,
								self.__proxyUrlWidget);
		self.layout().setWidget(10, QFormLayout.LabelRole,
								QLabel("Port", self));
		self.layout().setWidget(10, QFormLayout.FieldRole,
								self.__proxyPortWidget);
		self.layout().setWidget(11, QFormLayout.FieldRole,
								self.__proxyOnlyUnsafeCheckBox);

		# Timeout
		self.layout().setWidget(12, QFormLayout.LabelRole,
								QLabel(_("Timeout"), self));

		self.__timeoutWidget = QSpinBox(self);
		self.__timeoutWidget.setMaximum(300);
		self.__timeoutWidget.setValue(int(self.__config.get("timeout", 30)));

		self.layout().setWidget(12, QFormLayout.FieldRole,
								self.__timeoutWidget);

		# Connect signals
		self.__podEdit.urlChanged.connect(self.__podEditChanged);
		self.__usernameEdit.textEdited.connect(self.__usernameEditChanged);
		self.__passwordEdit.textEdited.connect(self.__passwordEditChanged);
		self.__proxyEnableCheckBox.stateChanged.connect(self.__proxyStateChanged);
		self.__proxyOnlyUnsafeCheckBox.stateChanged.connect(self.__fieldChanged);
		self.__proxyPortWidget.valueChanged.connect(self.__fieldChanged);
		self.__saveButton.clicked.connect(self.__saveSettings);
		self.__connectButton.clicked.connect(self.login);
		self.__proxyUrlWidget.urlChanged.connect(self.__fieldChanged);
		self.__timeoutWidget.valueChanged.connect(self.__fieldChanged);
		self.__autoLoginCheckBox.stateChanged.connect(self.__fieldChanged);
		self.__acceptUnsafeCheckBox.stateChanged.connect(self.__fieldChanged);

	def parseBool(self, str): return str.lower() == "true";

	def validEmail(self, text):
		test = re.match(ValidationPatterns.email.value, text);
		if test:return True;
		return False;

	def validUsername(self, text):
		test = re.match(ValidationPatterns.username.value, text);
		if test:return True;
		return False;

	def validPassword(self, text):
		test = re.match(ValidationPatterns.password.value, text);
		if test:return True;
		return False;

	def allValid(self):
		if (self.__usernameEdit.property("valid")
		and self.__podEdit.netlocValid()
		and self.__passwordEdit.property("valid")):
			if self.__proxyEnableCheckBox.isChecked():
				if not self.__proxyUrlWidget.netlocValid(): return False;
			return True;
		return False;

	@pyqtSlot(int)
	def __proxyStateChanged(self, state):
		for row in [7,8,9]:
			for role in [QFormLayout.LabelRole, QFormLayout.FieldRole]:
				item = self.layout().itemAt(row, role);
				if item:
					widget = item.widget();
					if widget:
						if state: widget.show();
						else: widget.hide();
		self.__fieldChanged();

	@pyqtSlot()
	def __usernameEditChanged(self):
		if (self.validUsername(self.__usernameEdit.text()) or
			self.validEmail(self.__usernameEdit.text())):
			self.__usernameEdit.setProperty("valid", True);
		else: self.__usernameEdit.setProperty("valid", False);
		self.__usernameEdit.style().unpolish(self.__usernameEdit);
		self.__usernameEdit.style().polish(self.__usernameEdit);
		self.__fieldChanged();

	@pyqtSlot()
	def __passwordEditChanged(self):
		if self.validPassword(self.__passwordEdit.text()):
			self.__passwordEdit.setProperty("valid", True);
		else: self.__passwordEdit.setProperty("valid", False);
		self.__passwordEdit.style().unpolish(self.__passwordEdit);
		self.__passwordEdit.style().polish(self.__passwordEdit);
		self.__fieldChanged();

	@pyqtSlot()
	def __podEditChanged(self): self.__fieldChanged();

	@pyqtSlot()
	def __fieldChanged(self):
		"""Sets the save button to visible if every field has valid input, else
		hides it.
		"""
		if self.allValid(): self.__saveButton.show();
		else: self.__saveButton.hide();

	@pyqtSlot()
	def __saveSettings(self):
		self.__config['scheme'] = self.__podEdit.scheme();
		self.__config['netloc'] = self.__podEdit.netloc();
		self.__config['username'] = self.__usernameEdit.text();
		self.__config["auto_login"] = str(self.__autoLoginCheckBox.isChecked());
		self.__config["verify_ssl"] = str(not self.__acceptUnsafeCheckBox.isChecked());

		if self.__proxyUrlWidget.netloc():
			self.__config["proxy_enabled"] = str(self.__proxyEnableCheckBox.isChecked());
			self.__config["proxy_scheme"] = self.__proxyUrlWidget.scheme();
			self.__config["proxy_netloc"] = str(self.__proxyUrlWidget.netloc());
			self.__config["proxy_port"] = str(self.__proxyPortWidget.value());
			self.__config["proxy_unsafe_only"] = str(self.__proxyOnlyUnsafeCheckBox.isChecked());

		self.__config["timeout"] = str(self.__timeoutWidget.value());

		# Maybe do this only after succesfull connection?
		# Some user with their passwords.. :-P

		self.__saveButton.hide();

		# If password already valid and didn't change.
		if self.__passwordEdit.text() and self.__passwordEdit.property("valid"):
			keyring.set_password("LionsTooth", self.__config['username'], self.__passwordEdit.text());
		self.requestSaveConfig.emit();

	def data(self, pod=True):
		"""Returns a tuple with data needed for setting connection and login.
		"""
		data = {
			"pod_url"			: "",
			"pod_scheme"		: "",
			"username"			: "",
			"request_kwargs"	: {}
		}
		data["pod_url"] = self.__podEdit.url();
		data["pod_scheme"] = self.__podEdit.scheme();
		data["username"] = self.__usernameEdit.text();
		if self.__proxyEnableCheckBox.isChecked():
			data["request_kwargs"].update({"proxies":{}});
			proxyUrl = "{0}:{1}".format(self.__proxyUrlWidget.url(),
									self.__proxyPortWidget.value());
			data["request_kwargs"]["proxies"].update({"http":proxyUrl});
			if not self.__proxyOnlyUnsafeCheckBox.isChecked():
				data["request_kwargs"]["proxies"].update({"https":proxyUrl});
		data["request_kwargs"].update({"timeout":self.__timeoutWidget.value()});
		data["request_kwargs"].update({"verify":not self.__acceptUnsafeCheckBox.isChecked()});
		return data;

	def login(self):
		self.__connectButton.setText(_("Signing in.."));
		self.__connectButton.setEnabled(False);
		self.__setState(False);
		self.requestLogin.emit();

	def logout(self):
		self.__connectButton.setEnabled(False);
		self.requestLogout.emit();

	@pyqtSlot()
	def loginSucceeded(self):
		self.__connectButton.setText(_("Logout"));
		self.__connectButton.setEnabled(True);
		self.__connectButton.clicked.disconnect(self.login);
		self.__connectButton.clicked.connect(self.logout);
		self.__setState(False);

	@pyqtSlot()
	def loginFailed(self):
		self.__connectButton.setText(_("Login"));
		self.__connectButton.setEnabled(True);
		self.__setState(True);

	@pyqtSlot()
	def logoutSucceeded(self):
		self.__connectButton.setText(_("Login"));
		self.__connectButton.setEnabled(True);
		self.__connectButton.clicked.disconnect(self.logout);
		self.__connectButton.clicked.connect(self.login);
		self.__setState(True);

	@pyqtSlot()
	def logoutFailed(self):
		self.__connectButton.setText(_("Logout"));
		self.__connectButton.setEnabled(True);

	def __setState(self, state):
		"""Enables/disabled settings on state
		"""
		widgets = ([self.__podEdit, self.__usernameEdit, self.__passwordEdit,
					self.__proxyEnableCheckBox, self.__proxyOnlyUnsafeCheckBox,
					self.__proxyPortWidget, self.__saveButton,
					self.__proxyUrlWidget, self.__timeoutWidget,
					self.__openPodListButton, self.__autoLoginCheckBox])
		if state:
			for widget in widgets: widget.setEnabled(True);
		else:
			for widget in widgets: widget.setEnabled(False);

class General(QGroupBox):
	requestSaveConfig = pyqtSignal();
	themeChangeRequest = pyqtSignal(str);
	def __init__(self, config, parent=None):
		QGroupBox.__init__(self, parent=parent);

		self._config = config;
		self.setTitle(_("General Settings"));

		self.setSizePolicy(QSizePolicy.Expanding,
						QSizePolicy.Maximum);

		QFormLayout(self);

		# Language
		self._languageComboBox = QComboBox(parent=self);
	
		for lang in Translator.Lang:
			self._languageComboBox.addItem(lang.name, lang.value);

		selectedValue = config.get("language", ''); # '' is default for English.
		self._languageComboBox.setCurrentIndex(
			self._languageComboBox.findData(selectedValue)
		);

		self.layout().setWidget(0, QFormLayout.LabelRole,
								QLabel(_("Language"), self));
		self.layout().setWidget(0, QFormLayout.FieldRole,
								self._languageComboBox);


		# Themes
		self._themeComboBox = QComboBox(parent=self);
		self.layout().setWidget(1, QFormLayout.LabelRole,
								QLabel(_("Theme"), self));
		self.layout().setWidget(1, QFormLayout.FieldRole,
								self._themeComboBox);

		# Connect signals
		self._languageComboBox.currentIndexChanged.connect(self._langChanged);

	def setThemes(self, themes):
		for name in themes:
			self._themeComboBox.addItem(name, name);
		selectedValue = self._config.get("theme", 'default'); # '' is default for English.
		self._themeComboBox.setCurrentIndex(
			self._themeComboBox.findData(selectedValue)
		);
		self._themeComboBox.currentIndexChanged.connect(self.__themeChanged);

	@pyqtSlot(int)
	def __themeChanged(self, index):
		self._config["theme"] = self._themeComboBox.currentData(Qt.UserRole);
		self.themeChangeRequest.emit(self._themeComboBox.currentData(Qt.UserRole));
		self.requestSaveConfig.emit();

	@pyqtSlot(int)
	def _langChanged(self, index):
		self._config["language"] = self._languageComboBox.currentData(Qt.UserRole);
		Translator.setLanguage(self._languageComboBox.currentData(Qt.UserRole));
		self.requestSaveConfig.emit();

class SettingsPage(PageProto):
	requestSaveConfig = pyqtSignal();
	def __init__(self, config, parent=None):
		PageProto.__init__(self, parent=parent);

		self.config = config;

		self.connectionSettings = ConnectionSettings(
								self.config["Connection"],
								parent=None);
		self.connectionSettings.requestSaveConfig.connect(self.requestSaveConfig);
		self.addWidget(self.connectionSettings);

		self.generalSettings = General(self.config["General"]);
		self.generalSettings.requestSaveConfig.connect(self.requestSaveConfig);
		self.addWidget(self.generalSettings);
