"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QLabel
from PyQt5.QtCore import Qt

from controllers.pageController import PageProto

class NotificationsPage(PageProto):
	def __init__(self, parent=None):
		PageProto.__init__(self, parent=parent);

		#self._notificationsModel = NotificationsModel;

		self.titleWidget = QLabel("<h2>NotificationsPage</h2>", self);
		self.addWidget(self.titleWidget, 0, Qt.AlignCenter);

		tmp = QLabel("<h2>Underdestruction, comming soon ;-)</h2>", self);
		self.addWidget(tmp, 0, Qt.AlignCenter);
