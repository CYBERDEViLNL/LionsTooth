"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QLabel, QShortcut
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtCore import Qt

from controllers.pageController import PageProto
from gui.views.stream import StreamView


class StreamPage(PageProto):
	def __init__(self, title="Unknown", view=StreamView, aspectsModel=None, parent=None):
		PageProto.__init__(self, parent=parent);


		self._selectedPostIndex = None;

		self.title = title
		self.titleWidget = QLabel(self);
		if title != "Unknown":
			self.setHeader(title);
			self.addWidget(self.titleWidget);

		self.streamView = view(parent=self);
		self._updateButton = QPushButton("Update", self);
		self._updateButton.clicked.connect(self.streamView.requestNewPosts.emit);
		self.addWidget(self._updateButton, 0, Qt.AlignLeft);
		self.addWidget(self.streamView);

		self.verticalScrollBar().valueChanged.connect(self.__scrollChanged);

		QShortcut(QKeySequence(Qt.Key_J), self, self.nextPostToTop); # Down
		QShortcut(QKeySequence(Qt.Key_K), self, self.previousPostToTop); # Up
		QShortcut(QKeySequence(Qt.Key_L), self, self.toggleAnimations);
		QShortcut(QKeySequence(Qt.Key_Space), self, self.toggleState);

	def toggleState(self):
		if self._selectedPostIndex != None:
			post = self.streamView.post(self._selectedPostIndex);
			post.toggleState();

	def toggleAnimations(self):
		if self._selectedPostIndex != None:
			post = self.streamView.post(self._selectedPostIndex);
			post.toggleAnimations();

	def postToTop(self): pass;

	def nextPostToTop(self):
		visiblePostIndexes = [];
		topMargin = 44;
		if self._selectedPostIndex != None:
			nextPost = self.streamView.post(self._selectedPostIndex+1);
			if nextPost:
				self.streamView.post(self._selectedPostIndex).setSelected(False);
				nextPost.setSelected(True);
				self._selectedPostIndex += 1;
				self.verticalScrollBar().setValue(nextPost.pos().y() + topMargin);
		else:
			for index, view in enumerate(self.streamView.posts()):
				if view.visibleRegion().boundingRect():
					visiblePostIndexes.append(index);
			if visiblePostIndexes:
				postView = self.streamView.post(visiblePostIndexes[0]);
				if postView:
					# Next post to top.
					postView.setSelected(True);
					self.verticalScrollBar().setValue(postView.pos().y() + topMargin);
					self._selectedPostIndex = visiblePostIndexes[0];

	def previousPostToTop(self):
		visiblePostIndexes = [];
		topMargin = 44;
		if self._selectedPostIndex != None:
			previousPost = self.streamView.post(self._selectedPostIndex-1);
			if previousPost:
				self.streamView.post(self._selectedPostIndex).setSelected(False);
				previousPost.setSelected(True);
				self._selectedPostIndex -= 1;
				self.verticalScrollBar().setValue(previousPost.pos().y() + topMargin);
		else:
			for index, view in enumerate(self.streamView.posts()):
				if view.visibleRegion().boundingRect():
					visiblePostIndexes.append(index);
			if visiblePostIndexes:
				postView = self.streamView.post(visiblePostIndexes[0]);
				if postView:
					postView.setSelected(True);
					self.verticalScrollBar().setValue(postView.pos().y() + topMargin);
					self._selectedPostIndex = visiblePostIndexes[0];

	def __scrollChanged(self, value):
		onePercent =  self.verticalScrollBar().maximum() / 100;
		if onePercent: # don't want ZeroDivisionError
			if round(value / onePercent) > 90: self.streamView.requestMorePosts.emit()

		# Stop animations and start/stop timers
		for view in self.streamView.posts():
			if not view.visibleRegion().boundingRect():
				view.setDisplayed(False);
				if view.property("selected"):
					view.setSelected(False);
					self._selectedPostIndex = None;
			else: view.setDisplayed(True);

	def setHeader(self, title):
		self.titleWidget.setText("<h2>{}</h2>".format(title));
