"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QMainWindow, QWidget, QVBoxLayout
from PyQt5.QtCore import pyqtSignal

class MainWindow(QMainWindow):
	resized = pyqtSignal(int, int); # <! width, height
	def  __init__(self, parent=None):
		QMainWindow.__init__(self, parent=parent);
		self.setWindowTitle("LionsTooth");

		# Central widget
		self._centralWidget = QWidget(self);
		self.centralLayout = QVBoxLayout(self._centralWidget);
		self.centralLayout.setSpacing(0);

		self.setCentralWidget(self._centralWidget);

	def resizeEvent(self, event):
		QMainWindow.resizeEvent(self, event);
		self.resized.emit(self.size().width(), self.size().height());
