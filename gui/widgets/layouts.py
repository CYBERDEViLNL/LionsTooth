"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QWidget, QLayout, QStyle, QSizePolicy
from PyQt5.QtCore import Qt, QRect, QSize, QPoint

class FlowLayout(QLayout):
	"""
	Pyton implementation of this:
	http://doc.qt.io/qt-5/qtwidgets-layouts-flowlayout-example.html
	"""
	def __init__(self, margin=0, horizontalSpacing=0, verticalSpacing=0, parent=None):
		if parent: QLayout.__init__(self, parent);
		else: QLayout.__init__(self);

		self.__items = [];
		self._margin = margin;
		self._horizontalSpacing = horizontalSpacing;
		self._verticalSpacing = verticalSpacing;

		self.setContentsMargins(margin, margin, margin, margin);
		self.destroyed.connect(self._del);

	def _del(self):
		while self.count():
			item = self.takeAt(0);
			del item;

	def addItem(self, item): self.__items.append(item);

	def horizontalSpacing(self):
		if self._horizontalSpacing >= 0: return self._horizontalSpacing;
		else: return self.smartSpacing(QStyle.PM_LayoutHorizontalSpacing);

	def verticalSpacing(self):
		if self._verticalSpacing >= 0: return self._verticalSpacing;
		else: return self.smartSpacing(QStyle.PM_LayoutVerticalSpacing);

	def count(self): return len(self.__items);

	def itemAt(self, index):
		if (index) < len(self.__items):
			return self.__items[index];

	def takeAt(self, index):
		if index >= 0 and index < len(self.__items):
			return self.__items.pop(index);
		else: return 0;

	def expandingDirections(self): return Qt.Horizontal;

	def hasHeightForWidth(self): return True;

	def heightForWidth(self, width):
		return self.doLayout(QRect(0, 0, width, 0), True);

	def setGeometry(self, rect):
		QLayout.setGeometry(self, rect);
		self.doLayout(rect, False);

	def sizeHint(self):
		return self.minimumSize();

	def minimumSize(self):
		size = QSize();
		for item in self.__items:
			size = size.expandedTo(item.minimumSize());
		size = size.expandedTo(QSize(2*self._margin, 2*self._margin));
		#size += QSize(2*self._margin, 2*self._margin);
		return size;

	def doLayout(self, rect, testOnly=False):
		left, top, right, bottom = self.getContentsMargins();
		effectiveRect = rect.adjusted(+left, +top, -right, -bottom);
		x = effectiveRect.x();
		y = effectiveRect.y();
		lineHeight = 0;
		for item in self.__items:
			widget = item.widget();

			spaceX = self.horizontalSpacing();
			if spaceX == -1:
				spaceX = widget.style().layoutSpacing(
				QSizePolicy.PushButton, QSizePolicy.PushButton, Qt.Horizontal);

			spaceY = self.verticalSpacing();
			if spaceY == -1:
				spaceY = widget.style().layoutSpacing(
				QSizePolicy.PushButton, QSizePolicy.PushButton, Qt.Vertical);

			nextX = x + item.sizeHint().width() + spaceX;
			if nextX - spaceX > effectiveRect.right() and lineHeight > 0:
				x = effectiveRect.x();
				y += lineHeight + spaceY;
				nextX = x + item.sizeHint().width() + spaceX;
				lineHeight = 0;

			if not testOnly:
				item.setGeometry(QRect(QPoint(x, y), item.sizeHint()));

			x = nextX;
			lineHeight = max(lineHeight, item.sizeHint().height());

		return y + lineHeight - rect.y() + bottom;

	def smartSpacing(self, pixelMetric):
		parent = self.parent();
		if not parent: return -1;
		elif parent.isWidgetType():
			parentWidget = static_cast<QWidget>(parent);
			return style().pixelMetric(pixelMetric, 0, parentWidget);
		else: return static_cast<QLayout>(parent).spacing();
