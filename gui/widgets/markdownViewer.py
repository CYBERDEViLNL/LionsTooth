"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QTextBrowser, QSizePolicy, QLabel, QTextEdit, QApplication, QMenu, QPushButton, QScrollBar
from PyQt5.QtGui import QTextDocument, QTextOption, QColor
from PyQt5.QtCore import Qt, QThreadPool, pyqtSignal, pyqtSlot, pyqtProperty, qDebug

from gui.widgets.image import Image

class HtmlContent(QTextBrowser):
	#anchorClicked = pyqtSignal(str)# str: anchor (the link)
	def __init__(self, html="", parent=None):
		QTextEdit.__init__(self, parent);

		""" document """
		self._document = QTextDocument();
		self._document.setDocumentMargin(0);

		self._styleSheet = "a {color:#FF0000;}";
		self._html = html;
		self._customHeight = 0;
		self._realHeight = 0;

		""" self """
		sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred);
		self.setSizePolicy(sizePolicy);

		self.setTextInteractionFlags(Qt.LinksAccessibleByKeyboard
									|Qt.LinksAccessibleByMouse);
		self.setFrameShape(QFrame.NoFrame);

		self.setReadOnly(True);
		self.setWordWrapMode(QTextOption.WrapAtWordBoundaryOrAnywhere);

		""" If this is True and QTextBrowser can't open the link, it will
		remove the text.
		"""
		self.setOpenLinks(False);

		self.setDocument(self._document);

		self.viewport().setMouseTracking(True);
		self.viewport().setCursor(Qt.ArrowCursor);

		"""NOTE: If qproperty-styleSheet is not defined and this is
		commented out, then its not shown. Maybe build a fallback.
		If this isn't commented then it will update twice if
		qproperty-styleSheet is set.
		self._updateStyleSheet();
		"""
		self._updateStyleSheet();

	def wheelEvent(self, event): pass; # do nothing.

	def _updateStyleSheet(self):
		self._document.setDefaultStyleSheet(self._styleSheet);
		self._document.setHtml(self._html);

	@pyqtProperty(str)
	def styleSheet(self): return self._styleSheet;

	@styleSheet.setter
	def styleSheet(self, css):
		self._styleSheet = css;
		self._updateStyleSheet();

	@pyqtSlot()
	def __copyHTML(self):
		QApplication.clipboard().setText(self.toHtml());

	def realHeight(self): return self._realHeight; # document height

	def contextMenuEvent(self, event):
		contextMenu = self.createStandardContextMenu();
		if contextMenu:
			contextMenu.addAction("Copy HTML", self.__copyHTML);
			contextMenu.exec(event.globalPos());

	def setCustomHeight(self, height):
		self._customHeight = height;
		if height:
			self.setMinimumHeight(height);
			self.setMaximumHeight(height);
		else:
			self.setMinimumHeight(self._realHeight);
			self.setMaximumHeight(self._realHeight);
		self.verticalScrollBar().triggerAction(QScrollBar.SliderToMinimum)

	def resizeEvent(self, event):
		self._document.setTextWidth(event.size().width());
		height = self._document.size().height();
		self._realHeight = height;
		if not self._customHeight:
			self.setMinimumHeight(height);
			self.setMaximumHeight(height);

	def setHtml(self, html): self._document.setHtml(html);

from PyQt5.QtWidgets import QFrame, QVBoxLayout

from core.markdownObjectParser import MarkdownObjectParser

from core.markdownExt import DiasporaMD
import markdown
class MarkdownViewer(QFrame):
	anchorClicked = pyqtSignal(str); # anchor
	def __init__(self, doCollapse=True, parent=None):
		QFrame.__init__(self, parent=parent);

		layout = QVBoxLayout(self);

		self._items = [];
		self._objectParser = MarkdownObjectParser();
		self._markdown = markdown.Markdown(extensions=[
											DiasporaMD(mentionedPeople={}),
											'markdown.extensions.nl2br',
											'markdown.extensions.tables']);
		self.__imageResolver = None;
		self._collapseHeight = 500;
		self._collapseState = False;
		self._collapseButton = None;
		self._doCollapse = doCollapse;

	def collapsed(self): return self._collapseState;
	def collapse(self): self.__collapse();
	def expand(self): self.__expand();

	def __collapseButtonNeeded(self):
		if not self._collapseState:
			itemsHeight = 0;
			for index, item in enumerate(self._items):
				itemHeight = 0;
				if type(item) == HtmlContent: itemHeight = item.realHeight();
				else: itemHeight = item.height();
				itemsHeight += itemHeight;

			if itemsHeight > self._collapseHeight and not self._collapseButton:
				self._collapseButton = QPushButton("Show more", self);
				self._collapseButton.setObjectName("CollapseButton");
				self._collapseButton.clicked.connect(self.toggleCollapseState);
				self.layout().addWidget(self._collapseButton);
				if not self._collapseState: self.__collapse();
			elif self._collapseButton:
				self._collapseButton.setParent(None);
				self._collapseButton = None;

	def resizeEvent(self, event):
		if self._doCollapse:
			if (event.size().width() != event.oldSize().width()):
				self.__collapseButtonNeeded();

	@pyqtSlot()
	def toggleCollapseState(self):
		if self._collapseState: self.__expand();
		else: self.__collapse();

	def __collapse(self):
		# Only show first item
		for i in range(1, len(self._items)): self._items[i].hide();
		if self._items:
			collapseHeight = self._collapseHeight;
			if self._collapseButton:
				collapseHeight += self._collapseButton.height();
			if (type(self._items[0]) == HtmlContent and
			self._items[0].realHeight() > collapseHeight):
				self._items[0].setCustomHeight(collapseHeight);

		if self._collapseButton: self._collapseButton.setText("Show more");
		self._collapseState = True;

	def __expand(self):
		# show all items
		for i in range(1, len(self._items)): self._items[i].show();
		if self._items:
			if type(self._items[0]) == HtmlContent:
				self._items[0].setCustomHeight(0);
		if self._collapseButton: self._collapseButton.setText("Show less");
		self._collapseState = False;

	def startAnimations(self):
		for item in self._items:
			if type(item) == Image and item.isAnimation(): item.startAnimation();

	def stopAnimations(self):
		for item in self._items:
			if type(item) == Image and item.isAnimation(): item.stopAnimation();

	def hasImageResolver(self):
		if self.__imageResolver: return True;
		return False;

	def setImageResolver(self, imageResolver):
		self.__imageResolver = imageResolver;

	def clear(self):
		"""Clear all objects.
		"""
		for item in self._items:
			item.setParent(None);
			item.deleteLater();
		self._items.clear();

	def setMarkdown(self, markdown):
		"""Parses markdown and creates objects for it to display.
		"""
		self.clear();
		objects = self._objectParser.parse(markdown);
		for obj in objects:
			if obj["type"] == 'CodeBlock':
				self.__addCoreBlock(obj["content"]);
			elif obj["type"] == 'Text':
				self.__addHtml(obj["content"]);
			elif obj["type"] == 'Image':
				self.__addImage(obj["url"], alt=obj["alt"], title=obj["title"]);
			elif obj["type"] == 'ImageLink':
				self.__addImage(
					obj["url"],
					alt=obj["alt"],
					title=obj["title"],
					link=obj["link"]
				);
				qDebug("ImageLink {}".format(obj));
		if self._doCollapse: self.__collapseButtonNeeded();

	def __addHtml(self, markdown):
		html = self._markdown.convert(markdown);
		widget = HtmlContent(html);
		widget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff);
		widget.anchorClicked.connect(self.__anchorClicked);
		self._items.append(widget);
		self.layout().addWidget(widget, 0, Qt.AlignTop);

	def __anchorClicked(self, quri): self.anchorClicked.emit(quri.toString());

	@pyqtSlot(str)
	def __resolvingImageFailed(self, err):
		qDebug("[DEBUG] MarkdownViewer.__resolvingImageFailed() `{0}`"
				.format(err));

	@pyqtSlot()
	def __retryImage(self):
		qDebug("retryImage");
		if self.__imageResolver:
			image = self.sender();
			workerId = self.__imageResolver.newJob(image.url());
			self.__imageResolver.worker(workerId).signals.result.connect(
				image.setPath
			);
			self.__imageResolver.worker(workerId).signals.exception.connect(
				self.__resolvingImageFailed
			);
			self.__imageResolver.worker(workerId).signals.exception.connect(
				image.setFailed
			);
			self.__imageResolver.startWorker(workerId);
		else: qDebug("retryImage, no resolver.");

	def __addImage(self, url, alt="", title="", link=""):
		image = Image(url=url, title=title, alt=alt, hyperlink=link, parent=self);
		image.requestRetry.connect(self.__retryImage);
		self._items.append(image);
		index = len(self._items) - 1;

		if self.__imageResolver:
			workerId = self.__imageResolver.newJob(url);
			self.__imageResolver.worker(workerId).signals.result.connect(
				image.setPath
			);
			self.__imageResolver.worker(workerId).signals.exception.connect(
				self.__resolvingImageFailed
			);
			self.__imageResolver.worker(workerId).signals.exception.connect(
				image.setFailed
			);
			self.__imageResolver.startWorker(workerId);

		self.layout().addWidget(image, 0, Qt.AlignTop);

	def __addCoreBlock(self, code):
		codeBlock = QLabel("[CodeBlock]{0}[/CodeBlock]".format(code), self);
		self.setToolTip(code);
		self._items.append(codeBlock);
		self.layout().addWidget(codeBlock, 0, Qt.AlignTop);

