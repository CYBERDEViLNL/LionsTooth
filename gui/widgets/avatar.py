"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QSizePolicy
from gui.widgets.image import Image
from gui.views.user import LineWrappedText
class AvatarWidget(Image):
	def __init__(self, maxHeight=50, parent=None):
		Image.__init__(self, fixedHeight=maxHeight, parent=parent);

		self._guid = "";
		self._diasporaId = "";
		self._guid = "";

		sizePolicy = QSizePolicy(
					QSizePolicy.Maximum,
					QSizePolicy.Maximum);
		self.setSizePolicy(sizePolicy);

		self.setHeight(maxHeight);
		self.setStyleSheet("background-color: green;"); # TODO DEBUG

	def infoStr(self):
		return "guid: {0}\ndiaspora_id: {1}\nname: {2}".format(self.guid(), self.diasporaId(), self.name());

	def setAuthorData(self, data): pass;

	def setName(self, name): self._name = name;
	def setDiasporaId(self, diasporaId): self._diasporaId = diasporaId;
	def setGuid(self, guid): self._guid = guid;

	def guid(self): return self._guid;
	def diasporaId(self): return self._diasporaId;
	def name(self): return self._name;

	def setHeight(self, height):
		self._fixedHeight = height;
		self.setMinimumHeight(height);
		self.setMaximumHeight(height);
		self.setMaximumWidth(height);
