"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QWidget, QSizePolicy
from PyQt5.QtCore import Qt, QRectF, pyqtProperty
from PyQt5.QtGui import QPainter, QPen, QColor, QFontMetrics, QTextOption

from math import ceil

class WrappedText(QWidget):
	def __init__(self, text="", parent=None):
		QWidget.__init__(self, parent=parent);

		self._backgroundColor = QColor();
		self._textColor = QColor("#000000");
		self._borderColor = QColor();
		self._bold = False;
		self._fontSize = self.font().pointSize();
		self.__text = text;
		self.__textWidth = 0;
		self.__textHeight = 0;
		self.__textOptions = QTextOption();
		self.__textOptions.setWrapMode(QTextOption.WrapAnywhere);

		sizePolicy = QSizePolicy(
					QSizePolicy.Expanding,
					QSizePolicy.Preferred);
		self.setSizePolicy(sizePolicy);
		self.setMinimumWidth(32);
		self.setMaximumWidth(1243242);

		if text: self.setText(text);

	@pyqtProperty(QColor)
	def textColor(self): return self._textColor;

	@textColor.setter
	def textColor(self, color): self._textColor = color;

	@pyqtProperty(QColor)
	def backgroundColor(self): return self._backgroundColor;

	@backgroundColor.setter
	def backgroundColor(self, color): self._backgroundColor = color;

	@pyqtProperty(QColor)
	def borderColor(self): return self._borderColor;

	@borderColor.setter
	def borderColor(self, color): self._borderColor = color;

	@pyqtProperty(bool)
	def bold(self): return self._bold;

	@bold.setter
	def bold(self, bool): self._bold = bool;

	@pyqtProperty(int)
	def fontSize(self): return self._fontSize;

	@fontSize.setter
	def fontSize(self, int): self._fontSize = int;

	def text(self): return self.__text;
	def setText(self, text):
		self.__text = text;
		self.__recalcTextDimensions();
		self.update();

	def resizeEvent(self, event):
		self.__recalcTextDimensions();
		QWidget.resizeEvent(self, event);

	def __recalcTextDimensions(self):
		font = self.font();
		fontMetrics = QFontMetrics(font);
		self.__textWidth = fontMetrics.width(self.__text);
		self.__textHeight = fontMetrics.height();


		height = self.__textHeight;
		if height and self.__textWidth > self.size().width()-2:
			multiplier = ceil(self.__textWidth / (self.size().width()-2));
			height = self.__textHeight * multiplier;
			height = fontMetrics.lineSpacing() * multiplier;
			self.__textHeight = height;

		self.setMinimumHeight(self.__textHeight);
		self.setMaximumHeight(self.__textHeight);

	def paintEvent(self, event):
		painter = QPainter(self);
		painter.setPen(QPen(self.borderColor));


		#rect = QRectF(float(1),float(1), float(self.size().width()-2), float(height));
		rect = QRectF(float(1),float(1), float(self.size().width()-2), float(self.__textHeight));
		if self.backgroundColor.isValid(): painter.fillRect(rect, self.backgroundColor);
		if self.borderColor.isValid(): painter.drawRect(rect);

		font = painter.font();
		font.setBold(self.bold);
		font.setPointSize(self.fontSize)
		painter.setFont(font);
		painter.setPen(QPen(self.textColor));
		painter.drawText(rect, self.__text, self.__textOptions);

class LineWrappedText(WrappedText):
	def __init__(self, text="", parent=None):
		WrappedText.__init__(self, text=text, parent=parent);

		#font = self.font();
		#fontMetrics = QFontMetrics(font)
		#self.setMinimumHeight(fontMetrics.height());
		#self.setMaximumHeight(fontMetrics.height());
