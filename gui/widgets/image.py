"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QLabel, QMenu, QFileDialog, QSizePolicy, QApplication
from PyQt5.QtGui import QMovie, QPixmap
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QSize, qDebug

from core.notify import Notify

from PIL import Image as PILImage
from shutil import copyfile
class Image(QLabel):
	updated = pyqtSignal();
	anchorClicked = pyqtSignal(str); # str: the link
	requestRetry = pyqtSignal();
	def __init__(self, path="", title="", alt="", url="", hyperlink="", fixedHeight=0, maxSize=QSize(), disableAnimation=False, parent=None):
		QLabel.__init__(self, parent=parent)

		self._pixmap = None;
		self._movie = None;

		self._originalSize = [0,0];
		self._title = title;
		self._alt = alt;
		self._hyperlink = "";
		self._url = url;
		self._imgPath = "";
		self._imgFormat = "";
		self._fixedHeight = fixedHeight;
		self._maxSize = maxSize;
		self._disableAnimation = disableAnimation;

		if self._title and not self._hyperlink: self.setToolTip(self._title);
		self.setText(self._alt);

		self.__isAnimation = False;
		self.__valid = False;

		self._gifPlayButton = None;

		self.setHyperlink(hyperlink);
		self.setPath(self._imgPath);

	def __urlToClipboard(self): QApplication.clipboard().setText(self.url());

	def url(self): return self._url;

	def setPath(self, path):
		"""Sets image by path
		"""
		self._imgPath = path;
		self.__updateImage();

	def setHyperlink(self, hyperlink):
		self._hyperlink = hyperlink;

	def enterEvent(self, event):
		if self._hyperlink:
			self.setCursor(Qt.PointingHandCursor);
			self.setProperty("hover", True);
			self.style().unpolish(self);
			self.style().polish(self);

	def leaveEvent(self, event):
		if self._hyperlink:
			self.setCursor(Qt.ArrowCursor);
			self.setProperty("hover", False);
			self.style().unpolish(self);
			self.style().polish(self);

	def saveFile(self):
		"""Opens a dialog where to store the image.
		"""
		print(self._url[self._url.rfind("/")+1:]);
		lowerFormatStr = self._imgFormat.lower()

		# Get save path
		storePath, ext = QFileDialog.getSaveFileName(
													self,
													"Save image",
													self._url[self._url.rfind("/")+1:],
													"{0}-image (.{0});;"
													.format(lowerFormatStr));
		if not storePath: return; # user canceled.
		lenStorePath = len(storePath)

		# Check if user typed same ext as we want to append.
		if (storePath[lenStorePath-len(lowerFormatStr):lenStorePath].lower()
															!= lowerFormatStr):
			storePath = "{0}.{1}".format(storePath, lowerFormatStr)

		# Copy file using shutil
		try: copyfile(self._imgPath, storePath);
		except PermissionError as err:
			Notify.warning("Could not save image: {}".format(err));

	def __onContextMenu(self, pos):
		contextMenu = QMenu(self);
		contextMenu.addAction("Save image", self.saveFile);
		contextMenu.addAction("Reload image", self.__requestRetry);
		if self.url(): contextMenu.addAction("Copy image url", self.__urlToClipboard);
		contextMenu.addSeparator();
		contextMenu.exec_(pos);

	def set(self, path="", title="", alt="", url="", hyperlink="", fixedHeight=0):
		if path: self._path = path;
		if url: self._url = url;
		if title: self._title = title;
		if alt: self._alt = alt;
		if hyperlink: self._hyperlink = hyperlink;
		if fixedHeight: self._fixedHeight = fixedHeight;

	@pyqtSlot(str)
	def setFailed(self, err):
		qDebug("SET FAILED");
		self.clear();
		self._movie = None;
		self._pixmap = None;
		self._valid = False;

		self.setMaximumWidth(16777215);
		self.setMaximumHeight(16777215);
		self._imgPath = ":/images/failed_big";
		self._originalSize = [100,117];

		if not self.__setPixmap():
			qDebug("[DEBUG] Image().__updateImage() Setting RC pixmap for"
					"`{0}` failed.".format(":/icons/imageFailed")
			);
		self.setToolTip(err);

		self.show();

	def __requestRetry(self): self.requestRetry.emit();

	def isAnimation(self): return self.__isAnimation;

	def adjustPlayButton(self):
		movieSize = self._movie.scaledSize();
		buttonSize = self._gifPlayButton.size();

		x = (movieSize.width() / 2) - (buttonSize.width() / 2);
		y = (movieSize.height() / 2) - (buttonSize.height() / 2);

		self._gifPlayButton.move(x, y);

	def stopAnimation(self):
		self._movie.setPaused(True);
		self.adjustPlayButton();
		self._gifPlayButton.show();

	def startAnimation(self):
		self._movie.setPaused(False);
		self._gifPlayButton.hide();

	def mousePressEvent(self, event):
		button = event.button()
		if button == Qt.LeftButton:
			if self._movie:
				if self._movie.state() == QMovie.Running: self.stopAnimation();
				else: self.startAnimation();
			elif self._hyperlink:
				# Links cannot be pressed this way with a animation, but why do you want that.. hmm?
				self.anchorClicked.emit(self._hyperlink);

		elif button == Qt.RightButton: self.__onContextMenu(event.globalPos());

	def __initPlayButton(self):
		self._gifPlayButton = QLabel(self);
		self._gifPlayButton.setAttribute(Qt.WA_TranslucentBackground);
		pixmap = QPixmap(":/images/gif_play");
		self._gifPlayButton.setPixmap(pixmap);
		self._gifPlayButton.show();
		self.adjustPlayButton();

	def __scaleMovie(self):
		if self._movie:
			size = QSize(*self._originalSize);
			if self._fixedHeight:
				size.scale(self._fixedHeight, self._fixedHeight, Qt.KeepAspectRatio);
			else:
				self.setMaximumWidth(self._originalSize[0]);
				if self._originalSize[0] > self.width():
					size.scale(self.width(), self.width(), Qt.KeepAspectRatio);

			self._movie.setScaledSize(size);
			self.setMovie(self._movie);

	def __setMovie(self):
		movie = QMovie(self._imgPath);
		if movie:
			self._movie = movie;
			self.setMovie(self._movie);
			self._movie.start();
			self._movie.setPaused(True);
			self.__isAnimation = True;
			self.__valid = True;
			if not self._gifPlayButton: self.__initPlayButton();
			return True;
		return False;

	def __scalePixmap(self):
		if self._pixmap:
			if self._fixedHeight:
				scaledPixmap = self._pixmap.scaledToHeight(self._fixedHeight, Qt.SmoothTransformation);
				self.setPixmap(scaledPixmap);
			elif self._maxSize.isValid():
				if (self._pixmap.rect().size().width() > self._maxSize.width() or
					self._pixmap.rect().size().height() > self._maxSize.height()):
					scaledPixmap = self._pixmap.scaled(self._maxSize, Qt.KeepAspectRatio, Qt.SmoothTransformation);
					self.setPixmap(scaledPixmap);
				else: self.setPixmap(self._pixmap);
			else:
				self.setMaximumWidth(self._originalSize[0]);
				if self._originalSize[0] > self.width():
					scaledPixmap = self._pixmap.scaledToWidth(self.width(), Qt.SmoothTransformation);
					self.setPixmap(scaledPixmap);
				else:
					scaledPixmap = self._pixmap.scaled(*self._originalSize);
					self.setPixmap(scaledPixmap);
			return True;
		return False;

	def __setPixmap(self):
		pixmap = QPixmap(self._imgPath);
		if pixmap:
			self._pixmap = pixmap;
			self.__scalePixmap();
			self.__valid = True;
			return True;
		return False;

	def __rateTrackingPixel(self, test):
		"""
		:param test:
		:type test: PIL Image.open() object.
		"""
		score = 0;
		w, h = test.size;
		if w == 1 and h == 1:
			score += 2;
			pix = test.load();
			if not pix[0,0]: score += 1;
		elif w == 1 or h == 1: score += 1;
		return score;

	def __infoStr(self):
		return ("Path: `{0}`\n"
				"URL: `{1}`\n"
				"Title: `{2}`\n"
				"Alt: `{3}`\n"
				"Link: `{4}`\n"
				"Format: `{5}`\n"
				.format(self._imgPath,
				self._url,
				self._title,
				self._alt,
				self._hyperlink,
				self._imgFormat));

	def __updateImage(self):
		if self._imgPath:
			try:
				test = PILImage.open(self._imgPath);
				self._originalSize = test.size;
				self._imgFormat = test.format;
			except OSError:
				qDebug("[DEBUG] Image().__updateImage() Not a image: {}"
					.format(self._imgPath)
				);
				self.__valid = False;
				return;

			self.clear(); # Clear potential alt text.

			# Check for tracking pixel
			trackingScore = self.__rateTrackingPixel(test);
			if trackingScore:
				self.clear();
				self.setText("[WARNING]\n[TRACKING PIXEL]");
				self.setToolTip("Score: {0}\n{1}".format(trackingScore, self.__infoStr()));
				self.setStyleSheet("background-color: red; color: black;");
				self.setMaximumWidth(16777215);
				self.setMaximumHeight(16777215);
				if trackingScore > 0 and trackingScore < 3:
					qDebug("[ALERT] Possible tracking pixel found for image "
							"`{0}`\n {1}"
							.format(self._imgPath, self.__infoStr()));
				elif trackingScore == 3:
					qDebug("[ALERT] Tracking pixel found for image `{0}`\n"
							"{1}".format(self._imgPath, self.__infoStr()));
				return;

			# if there is no loop then it's not animated.
			if (not self._disableAnimation and
				self._imgFormat.lower() == "gif" and 'loop' in test.info):
				if not self.__setMovie():
					qDebug("[DEBUG] Image().__updateImage() It did look like "
							"the image is a animated gif but QMovie failed to "
							"init with this image: {0}".format(self._imgPath));

			if not self.__isAnimation:
				if not self.__setPixmap():
					qDebug("[DEBUG] Image().__updateImage() Setting pixmap for"
							"`{0}` failed.".format(self._imgPath));

			test.close(); # close PIL img
			self.updated.emit();

	def resizeEvent(self, event):
		if not self._fixedHeight:
			if event.oldSize().width() != event.size().width():
				if self._pixmap: self.__scalePixmap();
				elif self._movie: self.__scaleMovie();
