"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QComboBox, QStyleOptionComboBox, QColumnView, QSizePolicy, QStylePainter, QStyle
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtCore import Qt

class AspectItem(QStandardItem):
	def __init__(self, data, checked=None):
		"""
		:param data: keys: id, name, selected
		:type data: dict

		:param checked: If `checked` is set then ignore what the aspect data says about the checkstate.
		:type checked: bool
		"""
		QStandardItem.__init__(self, data['name']);
		self._id = data['id'];
		self._name = data['name'];

		self.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled);

		# If `checked` is set then ignore what the aspect data says.
		if checked != None: self.setChecked(checked);
		else: self.setChecked(data['selected']);

	def id(self): return self._id;
	def name(self): return self._name;

	def setChecked(self, state):
		if state: self.setData(Qt.Checked, Qt.CheckStateRole);
		else: self.setData(Qt.Unchecked, Qt.CheckStateRole);

class AspectsButton(QComboBox):
	__defaultItems = [{
				'id'		: 'public',
				'name'		: 'Public',
				'selected'	: False
			},{
				'id'		: 'all_aspects',
				'name'		: 'All aspects',
				'selected'	: True # TODO get default selected from settings (/user/edit)
			}];
	def __init__(self, aspectsModel, overrideCheckState=None, defaultItems=None, parent=None):
		QComboBox.__init__(self, parent=parent);

		self._aspects = aspectsModel;
		self._aspects.updated.connect(self._aspectsUpdated);

		""" default """
		self._previousText = "None";
		self._checkCount = 0;
		"""Override checked, set to None to use `selected` key from data, set
		to True or False to override check states."""
		self._overrideCheckState = overrideCheckState;
		# defaultItems may be a empty dict, but not None for override.
		if defaultItems != None: self.__defaultItems = defaultItems;
		self.__defaultItemsLen = len(self.__defaultItems);

		self.__itemIds = []; # current ids

		view = QColumnView();
		self.setView(view);

		if self.__defaultItemsLen: model = self.defaultItems();
		else: model = QStandardItemModel();
		self.setModel(model);

		""" hooks """
		self.view().pressed.connect(self.toggleItem);
		self.model().dataChanged.connect(self._dataChanged);
		
		""" separator """
		if defaultItems:
			self.insertSeparator(self.__defaultItemsLen);
			self.__defaultItemsLen += 1;

		""" on init """
		if aspectsModel: self._aspectsUpdated();

	def defaultItems(self):
		model = QStandardItemModel();
		for i in range(self.__defaultItemsLen):
			item = AspectItem(self.__defaultItems[i]);
			model.insertRow(i, item);
		return model;

	def toggleItem(self, modelIndex):
		item = self.model().itemFromIndex(modelIndex);
		if item.checkState() == Qt.Checked: item.setChecked(False);
		else: item.setChecked(True);

	""" title
	copied most of this from:
	https://stackoverflow.com/questions/47575880/qcombobox-set-title-text-regardless-of-items"""
	def uncheckedIndexes(self):
		return self.model().match(self.model().index(0, 0), Qt.CheckStateRole, Qt.Unchecked, -1, Qt.MatchRecursive);

	def checkedIndexes(self):
		return self.model().match(self.model().index(0, 0), Qt.CheckStateRole, Qt.Checked, -1, Qt.MatchRecursive);

	def paintEvent(self, event):
		painter = QStylePainter(self);

		opt = QStyleOptionComboBox();
		self.initStyleOption(opt);

		u_index = self.uncheckedIndexes();
		c_index = self.checkedIndexes();

		uncheck_count = len(u_index);
		checked_count = len(c_index);

		self._checkCount = checked_count;

		# none checked, this should not happen except on profile page
		if not checked_count: opt.currentText = "Not aspect";

		# all checked, minus public
		elif uncheck_count == 1 and self.model().itemFromIndex(u_index[0]).id() == "public":
			opt.currentText = "All aspects";

		# one checked
		elif checked_count == 1:
			try:
				opt.currentText = self.model().itemFromIndex( c_index[0] ).name();
			except AttributeError: pass;
		# multiple checked
		else: opt.currentText = "In {} aspects".format(checked_count);

		painter.drawComplexControl(QStyle.CC_ComboBox, opt);

		#draw the icon and text
		painter.drawControl(QStyle.CE_ComboBoxLabel, opt);

		if self._previousText != opt.currentText:
			self._previousText = opt.currentText;
		
	"""slot for dataChanged
	http://doc.qt.io/archives/qt-4.8/qabstractitemmodel.html#dataChanged"""
	def _dataChanged(self, topLeft, bottomRight):
		self.model().dataChanged.disconnect(self._dataChanged);

		item = self.model().itemFromIndex(topLeft);
		if type(item) is AspectItem:
			if item.id() == 'public': self.deselect(but='public');
			elif item.id() == 'all_aspects':
				self.deselect(but='all_aspects');
				self.selectAll();
			else:
				uncheckCount = len(self.uncheckedIndexes());
				minus = self.__defaultItemsLen;
				# Do this else you can't deselect aspects
				if item.checkState() == Qt.Unchecked: minus -= 1;
				# all aspects selected
				if not uncheckCount - minus: self.selectAll();
				# else select default items
				elif self.__defaultItems: self.deselectDefault();

		if not len(self.checkedIndexes()):
			index = self.model().index(0,0); # TODO set default index
			self.model().setData(index, Qt.Checked, Qt.CheckStateRole);

		self.model().dataChanged.connect(self._dataChanged);

	def selected(self): # returns a list with selected aspect id's
		ids = [];
		for index in self.checkedIndexes():
			item = self.model().itemFromIndex(index);
			ids.append(item.id());
		if 'all_aspects' in ids: # the ids will be there, it's enough
			ids.remove('all_aspects');
		return ids;

	def selectAll(self):
		for uncheckedIndex in self.uncheckedIndexes():
			uncheckedItem = self.model().itemFromIndex(uncheckedIndex);
			if uncheckedItem.id() != 'public': uncheckedItem.setChecked(True);

	def deselectDefault(self): # TODO this can be done more efficient
		for checked in self.checkedIndexes():
			checkedItem = self.model().itemFromIndex(checked);
			if checkedItem.id() in ['public', 'all_aspects']:
				checkedItem.setChecked(False);

	def deselect(self, but=None): # deselect everything but: public or all_aspects
		for checkedIndex in self.checkedIndexes():
			checkedItem = self.model().itemFromIndex(checkedIndex);
			if but and checkedItem.id != but: # uncheck everything except but
				checkedItem.setChecked(False);

	def setAspectsModel(self, aspectsModel):
		self._aspects = aspectsModel;
		self._aspectsUpdated();

	def _deletedIds(self):
		newIds = [aspect["id"] for aspect in self._aspects];
		deletedIds = [];
		for oldId in self.__itemIds:
			if oldId not in newIds: deletedIds.append(oldId);
		return deletedIds;

	def _purge(self):
		deletedIds = self._deletedIds();
		for rowIndex in range(self.model().rowCount()):
			item = self.model().item(rowIndex);
			if item and item.id() in deletedIds:
				self.model().removeRow(rowIndex);
				self.__itemIds.pop(self.__itemIds.index(item.id()));

	def aspectItem(self, id):
		"""Returns AspectItem for aspect["id"]
		Can be used to manually change the check state.
		"""
		for rowIndex in range(self.model().rowCount()):
			item = self.model().item(rowIndex);
			if item and item.id() == id: return item;

	def _aspectsUpdated(self):
		self._purge();
		if self._aspects:
			model = self.model()
			for i in range(len(self._aspects)):
				if self._aspects[i]["id"] not in self.__itemIds:
					index = self.__defaultItemsLen + i # +1 for the separator
					model.insertRow(
						index,
						AspectItem(
							self._aspects[i], checked=self._overrideCheckState
						)
					);
					self.__itemIds.append(self._aspects[i]["id"]);
			self.setModel(model);
