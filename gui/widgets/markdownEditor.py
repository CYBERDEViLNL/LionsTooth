"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QWidget, QFrame, QAction, QTabWidget, QPlainTextEdit, QToolBar, QVBoxLayout, QHBoxLayout, QInputDialog
from PyQt5.QtGui import QTextCursor
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt


from gui.widgets.markdownViewer import MarkdownViewer

class EditArea(QPlainTextEdit):
	clicked = pyqtSignal();
	def __init__(self, parent=None):
		QPlainTextEdit.__init__(self, parent=parent);
		self.setFrameShape(QFrame.NoFrame);
		self.setPlaceholderText("Oooh what a nice message.. :-)");

	def mousePressEvent(self, event):
		if event.button() == Qt.LeftButton: self.clicked.emit();
		QPlainTextEdit.mousePressEvent(self, event)

	def input(self, title="", label=""):
		text, state = QInputDialog.getText(self, title, label);
		if state: return text;
		return None;

	def bold(self):
		start, end = self.selectedTextRange();
		if not self.textCursor().selectedText():
			self.insertPlainText("** **")
		else:
			self.setCursorPosition(end);
			self.insertPlainText("**");
			self.setCursorPosition(start);
			self.insertPlainText("**");
			self.selectText(start, end+4);

	def italic(self):
		start, end = self.selectedTextRange();
		if not self.textCursor().selectedText():
			self.insertPlainText("* *")
		else:
			self.setCursorPosition(end);
			self.insertPlainText("*");
			self.setCursorPosition(start);
			self.insertPlainText("*");
			self.selectText(start, end+2);

	def header(self):
		cursor = self.textCursor();
		start = self.lineStart(cursor.position());
		self.setCursorPosition(start);
		self.insertPlainText("# ");

	def code(self):
		start, end = self.selectedTextRange()
		if not self.textCursor().selectedText():
			self.insertPlainText("\u2029````\u2029Code here..\u2029````\u2029");
		else:
			self.setCursorPosition(start);
			self.insertPlainText("\u2029````\u2029");
			self.setCursorPosition(end+6);
			self.insertPlainText("\u2029````\u2029");
			self.selectText(start, end+12);

	def quote(self, style=None):
		start, end = self.selectedTextRange();
		if not self.textCursor().selectedText():
			cursor = self.textCursor();
			start = self.lineStart(cursor.position());
			self.setCursorPosition(start);
			self.insertPlainText("> ");
		else:
			pos = start;
			text = self.textCursor().selectedText();
			text += '\u2029';
			for char in text:
				if char == '\u2029': # if white space
					self.setCursorPosition(self.lineStart(pos));
					self.insertPlainText("> ");
					end += 2;
				pos +=1;
			self.selectText(start, end);

	def link(self):
		start, end = self.selectedTextRange()
		link = self.input(title="Please enter url to link to", label="url: ");
		if not self.textCursor().selectedText():
			self.insertPlainText(
				"[displayed text here]({} \"alt text\")".format(link)
			);
		else:
			self.setCursorPosition(start);
			self.insertPlainText("[");
			self.setCursorPosition(end+1);
			self.edit.insertPlainText("]({} \"alt text\")".format(link));
			self.setCursorPosition(end);
	
	def image(self):
		start, end = self.selectedTextRange();
		link = self.input(title="Please enter image url", label="image url: ")
		if not self.textCursor().selectedText():
			self.insertPlainText(
				"![image text here]({} \"alt text\")".format(link)
			);
		else:
			self.setCursorPosition(start);
			self.insertPlainText("![");
			self.setCursorPosition(end+1);
			self.insertPlainText("]({} \"alt text\")".format(link));
			self.setCursorPosition(end);

	def pre(self):
		cursor = self.textCursor();
		start = self.lineStart(cursor.position());
		self.setCursorPosition(start);
		self.insertPlainText("    ");

	def nrList(self): self.list(style="nr");

	def list(self, style=None):
		start, end = self.selectedTextRange();
		text = self.textCursor().selectedText();
		if not text:
			cursor = self.textCursor();
			start = self.lineStart(cursor.position());
			self.setCursorPosition(start);
			if style == 'nr': self.insertPlainText("1. ");
			else: self.insertPlainText("- ");
		else:
			pos = start;
			count = 1;
			while pos < end:
				lineStart = self.lineStart(pos);
				self.setCursorPosition(lineStart);

				if style == 'nr':
					bullet = "{}. ".format(count);
					self.insertPlainText(bullet);
					count +=1;
					end += len(bullet);
				else:
					self.insertPlainText("- ");
					end += 2;

				pos = self.lineEnd(pos) + 1;
			self.selectText(start, end);

	def selectedTextRange(self):
		cursor = self.textCursor();
		if cursor.hasSelection():
			start = cursor.selectionStart();
			end = cursor.selectionEnd();
			return (start, end);
		return (0,0);

	def setCursorPosition(self, pos):
		cursor = self.textCursor();
		cursor.setPosition(pos);
		self.setTextCursor(cursor);

	def lineStart(self, pos):
		cursor = self.textCursor();
		cursor.setPosition(pos);
		cursor.movePosition(QTextCursor.StartOfLine)
		return cursor.position();

	def selectText(self, start, end):
		cursor = self.textCursor();
		cursor.setPosition(start);
		cursor.setPosition(end, QTextCursor.KeepAnchor);
		self.setTextCursor(cursor);

	def lineEnd(self, pos):
		cursor = self.textCursor();
		cursor.setPosition(pos);
		cursor.movePosition(QTextCursor.EndOfLine);
		return cursor.position();

class MarkdownEditor(QWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent=parent);

		layout = QVBoxLayout(self);
		layout.setContentsMargins(0,0,0,0);
		layout.setSpacing(0);

		# Toolbar
		self._toolBar = QToolBar(self);
		layout.addWidget(self._toolBar);

		self._boldAction = QAction("B", self);
		self._boldAction.setToolTip("Bold");
		self._toolBar.addAction(self._boldAction);
		self._italicAction = QAction("I", self);
		self._italicAction.setToolTip("Italic");
		self._toolBar.addAction(self._italicAction);
		self._headerAction = QAction("H", self);
		self._headerAction.setToolTip("Header");
		self._toolBar.addAction(self._headerAction);
		self._toolBar.addSeparator();

		self._linkAction = QAction("Link", self);
		self._toolBar.addAction(self._linkAction);
		self._linkAction.setToolTip("Link");
		self._imageAction = QAction("Img", self);
		self._imageAction.setToolTip("Image");
		self._toolBar.addAction(self._imageAction);
		self._preAction = QAction("Pre", self);
		self._preAction.setToolTip("Pre");
		self._toolBar.addAction(self._preAction);
		self._toolBar.addSeparator();

		self._listAction = QAction("List", self);
		self._listAction.setToolTip("List");
		self._toolBar.addAction(self._listAction);
		self._nrListAction = QAction("Nr List", self);
		self._nrListAction.setToolTip("Numbered list");
		self._toolBar.addAction(self._nrListAction);
		self._codeAction = QAction("<code>", self);
		self._codeAction.setToolTip("Code");
		self._toolBar.addAction(self._codeAction);
		self._quoteAction = QAction("\"quote\"", self);
		self._quoteAction.setToolTip("Quote");
		self._toolBar.addAction(self._quoteAction);
		self._toolBar.addSeparator();

		# Edit
		self._editWidget = EditArea(self);

		self._boldAction.triggered.connect(self._editWidget.bold);
		self._italicAction.triggered.connect(self._editWidget.italic);
		self._headerAction.triggered.connect(self._editWidget.header);
		self._linkAction.triggered.connect(self._editWidget.link);
		self._imageAction.triggered.connect(self._editWidget.image);
		self._preAction.triggered.connect(self._editWidget.pre);
		self._listAction.triggered.connect(self._editWidget.list);
		self._nrListAction.triggered.connect(self._editWidget.nrList);
		self._codeAction.triggered.connect(self._editWidget.code);
		self._quoteAction.triggered.connect(self._editWidget.quote);

		# Preview
		self._previewWidget = MarkdownViewer(doCollapse=False, parent=self);

		# QTabWidget
		self._tabWidget = QTabWidget(self);
		self._tabWidget.addTab(self._editWidget, "Edit");
		self._tabWidget.addTab(self._previewWidget, "Preview");
		self._tabWidget.currentChanged.connect(self._tabChanged);
		layout.addWidget(self._tabWidget);

	@pyqtSlot(int)
	def _tabChanged(self, index):
		if index == 1:
			self._previewWidget.setMarkdown(self._editWidget.toPlainText());

	def toolBar(self): return self._toolBar;
	def editWidget(self): return self._editWidget;
	def tabWidget(self): return self._tabWidget;
