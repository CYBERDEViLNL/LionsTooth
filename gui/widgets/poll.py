"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QWidget, QFrame, QRadioButton, QPushButton, QProgressBar, QVBoxLayout, QHBoxLayout
from PyQt5.QtCore import pyqtSignal

from gui.widgets.wraptext import WrappedText

class PollAnswer(QFrame):
	selected = pyqtSignal(int)
	def __init__(self, data, participated=False, voteMultiplier=0, parent=None):
		QFrame.__init__(self, parent=parent);

		# Data
		self._data = data;
		self._voteMultiplier = voteMultiplier;

		# Add stuff
		QVBoxLayout(self);

		topLayout = QHBoxLayout();
		self.layout().addLayout(topLayout);

		self._radioButton = QRadioButton(self);
		topLayout.addWidget(self._radioButton, 0);

		self._answerText = WrappedText(self._data["answer"], parent=self);
		topLayout.addWidget(self._answerText, 1);

		self._progressBar = QProgressBar(self);
		self._progressBar.setTextVisible(True);
		self._progressBar.setFormat("%p% (0 votes)");
		self._progressBar.setTextDirection(QProgressBar.TopToBottom);
		self.layout().addWidget(self._progressBar, 1);

		# Connect stuff
		self._radioButton.toggled.connect(self._selected);

		# Update stuff
		self._updateProgress();
		if participated: self.participated(True);

	def data(self): return self._data;

	def setMultiplier(self, multiplier):
		self._voteMultiplier = multiplier;
		self._updateProgress();

	def updateData(self, data, participated=False, voteMultiplier=0):
		self._data = data;
		if voteMultiplier: self.setMultiplier(voteMultiplier);
		if participated: self.participated(True);
		self._updateProgress();

	def _updateProgress(self):
		percent=0;
		if self._voteMultiplier:
			percent = self._data["vote_count"] * self._voteMultiplier;
		self._progressBar.setProperty("value", percent);
		if self._data["vote_count"] == 1:
			self._progressBar.setFormat(
				"%p% ({} vote)".format(self._data["vote_count"])
			);
		else:
			self._progressBar.setFormat(
				"%p% ({} votes)".format(self._data["vote_count"])
			);

	def participated(self, bool=True):
		if bool:
			self._radioButton.hide(); # TODO dont hide, delete.
			self.showResult(True);
		else:
			self._radioButton.show();

	def showResult(self, bool=True):
		if bool: self._progressBar.show(); # TODO deleteLater
		else: self._progressBar.hide();

	def _selected(self):
		self.selected.emit(self._data["id"]);
		self.setProperty("selected", True);
		self.rePolish();

	def deselect(self):
		self._radioButton.setChecked(False);
		self.setProperty("selected", False);
		self.rePolish();

	def rePolish(self):
		self.style().unpolish(self);
		self.style().polish(self);
		self._radioButton.style().unpolish(self._radioButton);
		self._radioButton.style().polish(self._radioButton);
		self._progressBar.style().unpolish(self._progressBar);
		self._progressBar.style().polish(self._progressBar);
		self._answerText.style().unpolish(self._answerText);
		self._answerText.style().polish(self._answerText);

	def select(self):
		self._radioButton.setChecked(True);
		self.setProperty("selected", True);
		self.rePolish();

class Poll(QFrame):
	voteRequest = pyqtSignal(int);
	def __init__(self, data, answer=None, parent=None):
		QFrame.__init__(self, parent=parent);

		self._data = data;
		self._id = data["poll_id"];

		"""if this is not None you have participated and this should be the
		answer id.
		"""
		self._answer = answer;

		self._voteMultiplier = 0;
		self.updateMultiplier();
		self._answers = {};
		self._selectedId = 0;

		QVBoxLayout(self);

		self._pollQuestion = WrappedText(self._data["question"], parent=self);
		self._pollQuestion.setMinimumHeight(32);
		self._pollQuestion.setMinimumWidth(32);
		self._pollQuestion.setStyleSheet("background-color: red;");
		self.layout().addWidget(self._pollQuestion, 1);

		self._answersLayout = QVBoxLayout();
		self.layout().addLayout(self._answersLayout);

		self._voteButton = QPushButton("Vote", self);
		self.layout().addWidget(self._voteButton);

		""" Add answers """
		for answer in data["poll_answers"]:
			pollAnswer = PollAnswer(answer, participated=self.participated(), voteMultiplier=self._voteMultiplier);
			self._answers.update({answer["id"] : pollAnswer});

			if not self.participated(): pollAnswer.selected.connect(self._selectionChanged);
			if answer["id"] == self._answer: pollAnswer.select();
			self._answersLayout.addWidget(self._answers[answer["id"]]);

		if self.participated():
			self._voteButton.hide();
		else: self._voteButton.clicked.connect(self.vote);

	def id(self): return self._id;

	def updateMultiplier(self):
		if self._data["participation_count"]:
			self._voteMultiplier = 100 / self._data["participation_count"];

	def updateData(self, data):
		self._data = data;
		self.updateMultiplier();
		for answerId in self._answers:
			answerData = {}
			for a in data["poll_answers"]:
				if a["id"] == answerId:
					answerData = a;
					break;
			self._answers[answerId].updateData(
				answerData,
				participated=self.participated(),
				voteMultiplier=self._voteMultiplier
			);

	def participated(self):
		participated = False;
		if self._answer != None: return True;
		return False;

	def vote(self):
		if self._selectedId and not self._answer:
			# Emit that has been voted, so vote can be send
			self.voteRequest.emit(self._selectedId);

	def voted(self, answerId):
		if self._selectedId and not self._answer:
			self._answer = answerId;
			self._voteButton.hide();
			self._voteButton.clicked.disconnect(self.vote);

			self.updateMultiplier();

			for id in self._answers:
				self._answers[id].setMultiplier(self._voteMultiplier);
				# Hide radio buttons, show progressbar and make option where voted for bold
				self._answers[id].participated();
				if id == answerId: self._answers[id].select();
				else: self._answers[id].deselect();

	def _selectionChanged(self, id):
		if self._selectedId:
			self._answers[ self._selectedId ].deselect();
		self._selectedId = id;
