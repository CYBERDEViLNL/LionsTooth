"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QFrame, QWidget, QLabel, QTextEdit, QScrollArea, QPushButton, QHBoxLayout, QVBoxLayout, QSizePolicy
from PyQt5.QtGui import QTextOption, QTextDocument, QIcon, QPixmap
from PyQt5.QtCore import Qt, QtInfoMsg, QtWarningMsg, qDebug, qWarning, pyqtSlot, pyqtSignal, QSize, QUrl

from core.notify import Notify

from gui.widgets.markdownViewer import HtmlContent

class NotifyItemProto(QFrame):
	"""Generic notify item
	"""
	def __init__(self, msg, maxWidth=None, orientation=Qt.Horizontal,
	parent=None):
		QFrame.__init__(self, parent=parent);

		self._msg = msg;
		
		if orientation == Qt.Horizontal: QHBoxLayout(self);
		elif orientation == Qt.Vertical: QVBoxLayout(self);
		else: qWarning("No orientation");

		self.layout().setSpacing(0);
		self.layout().setContentsMargins(0,0,0,0);

		self._messageWidget = HtmlContent(msg, parent=self);
		self.layout().addWidget(self._messageWidget, 1);

	def _destroy(self):
		self.setParent(None);
		self.deleteLater();

	def height(self):
		height = self._messageWidget.size().height();
		height += 2 * 12;
		return height;

	def updateSize(self, maxWidth=None):
		if maxWidth:
			self.setMinimumWidth(maxWidth);
			self.setMaximumWidth(maxWidth);

			self._messageWidget.setMinimumWidth(maxWidth);
			self._messageWidget.setMaximumWidth(maxWidth);

		self.adjustSize();
		self._messageWidget.adjustSize();

class NotifyItem(NotifyItemProto):
	"""Application notification item
	"""
	def __init__(self, msg, title, type=QtInfoMsg, maxWidth=None, parent=None):
		msg = "<html><body>{}</body></html>".format(msg);
		NotifyItemProto.__init__(self, msg=msg, maxWidth=maxWidth,
			orientation=Qt.Vertical, parent=parent
		);

		if type == Notify.Type.Message: self.setProperty("type", "message");
		elif type == Notify.Type.Warning: self.setProperty("type", "warning");
		elif type == Notify.Type.Error: self.setProperty("type", "error");

		self._topLayout = QHBoxLayout();
		self.layout().insertLayout(0, self._topLayout);

		self._titleWidget = QLabel(title, parent=self);
		self._topLayout.addWidget(self._titleWidget, 1);

		self._closeButton = QPushButton("Close", parent=self);
		self._topLayout.addWidget(self._closeButton, 0);

		self._closeButton.clicked.connect(self._destroy);

	def height(self):
		topHeight = self._titleWidget.size().height();
		if self._closeButton.size().height() > topHeight:
			topHeight = self._closeButton.size().height();
		topHeight += self._topLayout.contentsMargins().top();
		topHeight += self._topLayout.contentsMargins().bottom();
		return NotifyItemProto.height(self) + topHeight;

class NotificationArea(QFrame):
	def __init__(self, parent=None):
		QFrame.__init__(self, parent=parent);

		QVBoxLayout(self);

		self._scrollArea = QScrollArea(parent=self);
		self._scrollArea.setWidgetResizable(True);
		self._scrollArea.setMouseTracking(True);
		self._contentWidget = QWidget(parent=self._scrollArea);
		self._contentWidget.setMouseTracking(True);
		self._contentWidget.setObjectName("centralWidget");
		QVBoxLayout(self._contentWidget);
		self._contentWidget.layout().setAlignment(Qt.AlignTop);
		self._contentWidget.layout().setSpacing(3);
		self._contentWidget.layout().setContentsMargins(0,0,24,0);
		self._scrollArea.setWidget(self._contentWidget);
		self._scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff);

		self.layout().addWidget(self._scrollArea, 0, Qt.AlignTop);

		self.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Expanding);

		self._parent = parent;

		self._maximumHeight = 600;

		self._topLayout = QHBoxLayout();
		self._topLayout.setSpacing(3);
		self._topLayout.setContentsMargins(0,0,0,0);

		self._hideButton = QPushButton("Hide", parent=self);
		self._hideButton.clicked.connect(self.hide);
		self._topLayout.addWidget(self._hideButton, 1);

		self.layout().insertLayout(0, self._topLayout);
		

		self.setMouseTracking(True);

		self._adjustingHeight = False;
		self._adjustYStart = 0;

		self.hide();

	def show(self):
		self.adjustWidth();
		for i in range(self._contentWidget.layout().count()):
			item = self._contentWidget.layout().itemAt(i);
			if item:
				widget = item.widget();
				if type(widget) is NotifyItemProto:
					widget.updateSize(self.contentWidth());
		QFrame.show(self);

	def leaveEvent(self, event):
		self.setCursor(Qt.ArrowCursor);

	def mouseReleaseEvent(self, event):
		if self._adjustingHeight:
			self._adjustingHeight = False;
			self._maximumHeight = self.size().height();
			self.setCursor(Qt.ArrowCursor);

	def mousePressEvent(self, event):
		if event.button() == Qt.LeftButton:
			if self._posInDragArea(event.pos()):
				self._adjustYStart = event.pos().y();
				self.setCursor(Qt.ClosedHandCursor);
				self._adjustingHeight = True;

	def mouseMoveEvent(self, event):
		if self._adjustingHeight:
			if event.pos().y() != self._adjustYStart:
				height = event.pos().y() - self._adjustYStart
				self.setMinimumHeight(self.size().height() + height);
				self.setMaximumHeight(self.size().height() + height);

				maxScrollAreaHeight = self.size().height() + height;
				if self._contentWidget.layout().count() > 1:
					maxScrollAreaHeight -= self._hideButton.size().height();
					maxScrollAreaHeight -= self.layout().spacing();
					maxScrollAreaHeight -= 24; # for the grabarea
				self._scrollArea.setMinimumHeight(maxScrollAreaHeight);
				self._scrollArea.setMaximumHeight(maxScrollAreaHeight);

			self._adjustYStart = event.pos().y();
		elif self._posInDragArea(event.pos()): self.setCursor(Qt.SizeVerCursor);
		else: self.setCursor(Qt.ArrowCursor);

	def _posInDragArea(self, pos):
		bottom = self.pos().y();
		bottom += self.visibleRegion().boundingRect().bottom();
		if pos.y() in range(bottom-24, bottom): return True;
		return False;

	def horizontalLayoutHeight(self, layout):
		height = 0;
		for i in range(0, layout.count()):
			item = layout.itemAt(i);
			if item:
				widget = item.widget();
				if widget:
					if widget.size().height() > height:
						height = widget.size().height();
		height += layout.contentsMargins().top();
		height += layout.contentsMargins().bottom();
		return height;

	def verticalLayoutHeight(self, layout):
		height = 0;
		for i in range(0, layout.count()):
			item = layout.itemAt(i);
			if item:
				widget = item.widget();
				if height: height += layout.spacing();
				if widget: height += widget.size().height();
		height += layout.contentsMargins().top();
		height += layout.contentsMargins().bottom();
		return height;

	def contentWidth(self):
		width = self.size().width();
		width -= self._contentWidget.layout().contentsMargins().left();
		width -= self._contentWidget.layout().contentsMargins().right();
		width -= self._contentWidget.layout().spacing();
		width -= self.layout().contentsMargins().left();
		width -= self.layout().contentsMargins().right();
		width -= self.layout().spacing();

	def adjustWidth(self):
		parent = self.parentWidget();
		width = 300;
		if parent:
			width = parent.size().width();
			parentCentralWidget = parent.centralWidget();
			if parentCentralWidget:
				parentLayout = parent.centralWidget().layout();
			else:
				parentLayout = parent.layout();
			if parentLayout:
				width -= parentLayout.contentsMargins().left();
				width -= parentLayout.contentsMargins().right();
				self.move(
					parentLayout.contentsMargins().left(),
					parentLayout.contentsMargins().top()
				);
		self.setMinimumWidth(width);
		self.setMaximumWidth(width);
		return width;

	def adjustHeight(self):
		margins = self.layout().contentsMargins();
		height = margins.top();
		height += margins.bottom();
		height += self.layout().spacing();
		height += self.verticalLayoutHeight(self._contentWidget.layout());

		topLayoutHeight = self.verticalLayoutHeight(self._topLayout);
		height += topLayoutHeight;

		maxScrollAreaHeight = 0;
		maxScrollAreaHeight -= 24; # for the grabarea
		maxScrollAreaHeight -= topLayoutHeight;

		if height > self._maximumHeight:
			maxScrollAreaHeight += self._maximumHeight;
			self.setMinimumHeight(self._maximumHeight);
			self.setMaximumHeight(self._maximumHeight);
			self._scrollArea.setMinimumHeight(maxScrollAreaHeight);
			self._scrollArea.setMaximumHeight(maxScrollAreaHeight);
		else:
			maxScrollAreaHeight += height;
			self.setMinimumHeight(height);
			self.setMaximumHeight(height);
			self._scrollArea.setMinimumHeight(maxScrollAreaHeight);
			self._scrollArea.setMaximumHeight(maxScrollAreaHeight);


class Notifier(NotificationArea):
	"""Application notifications area
	"""
	def __init__(self, parent=None):
		NotificationArea.__init__(self, parent=parent);

		self._closeAllButton = QPushButton("Close all", parent=self);
		self._closeAllButton.clicked.connect(self._closeAll);
		self._topLayout.addWidget(self._closeAllButton, 0);

	def _closeAll(self):
		for i in range(self._contentWidget.layout().count()):
			item = self._contentWidget.layout().takeAt(0);
			if item:
				widget = item.widget();
				if widget: widget.deleteLater();
		self.adjustHeight();

	def _notificationDestroyed(self):
		if self._contentWidget.layout().count() == 0: self.hide();
		elif self._contentWidget.layout().count() == 1: self._closeAllButton.hide();
		else: self._closeAllButton.show();
		self.adjustHeight();

	def message(self, title, msg):
		self._notify(title, msg, type=Notify.Type.Message);

	def warning(self, title, msg):
		self._notify(title, msg, type=Notify.Type.Warning);

	def error(self, title, msg):
		self._notify(title, msg, type=Notify.Type.Error);

	def _notify(self, title, msg, type):
		width = self.adjustWidth();
		width = self.contentWidth();

		notification = NotifyItem(msg, title=title, type=type, maxWidth=width,
			parent=self._contentWidget
		);
		notification._messageWidget.setHtml(msg);
		notification.destroyed.connect(self._notificationDestroyed);
		self._contentWidget.layout().insertWidget(0, notification,
			0, Qt.AlignTop
		);
		notification.updateSize(width);

		if self._contentWidget.layout().count() == 1:
			self._closeAllButton.hide();
		else: self._closeAllButton.show();
		self.show();
		self.adjustSize();
		self._scrollArea.verticalScrollBar().setValue(0);

from gui.widgets.avatar import AvatarWidget
class NotificationItem(NotifyItemProto):
	"""Diaspora notification item
	"""
	toggleSeen = pyqtSignal();
	anchorClicked = pyqtSignal(QUrl);
	def __init__(self, msg, id, type, createdAt, unread=True, maxWidth=None,
	parent=None):
		NotifyItemProto.__init__(self, msg=msg, maxWidth=maxWidth,
			orientation=Qt.Horizontal, parent=parent
		);

		self._id = id;
		self.setProperty("type", type);

		self._avatarWidget = AvatarWidget(parent=self);
		self.layout().addWidget(self._avatarWidget, 1, Qt.AlignTop);

		self._contentLayout = QVBoxLayout();
		self.layout().addLayout(self._contentLayout);

		self._contentLayout.addWidget(self._messageWidget);

		self._timeAgoWidget = QLabel(createdAt, parent=self);
		self._contentLayout.addWidget(self._timeAgoWidget);

		self._seenStatusButton = QPushButton(parent=self);
		self._seenStatusButton.setSizePolicy(
			QSizePolicy.Maximum,QSizePolicy.Maximum
		);
		self.layout().addWidget(self._seenStatusButton, 0);

		self._seenStatusButton.clicked.connect(self._seenButtonClicked);
		self._messageWidget.anchorClicked.connect(self.anchorClicked)

		self.setSeen(unread);

	def id(self): return self._id;

	@pyqtSlot()
	def _seenButtonClicked(self): self.toggleSeen.emit();

	def avatarWidget(self): return self._avatarWidget;

	def toggle(self):
		if self.property("seen"): self.setSeen(unread=True);
		else: self.setSeen(unread=False);

	def setSeen(self, unread=True):
		icon = QIcon();
		if unread:
			self.setProperty("seen", False);
			pixmap = QPixmap(":/icons/32x32/eye_active");
			icon.addPixmap(pixmap, QIcon.Normal, QIcon.Off);
		else:
			self.setProperty("seen", True);
			pixmap = QPixmap(":/icons/32x32/eye");
			icon.addPixmap(pixmap, QIcon.Normal, QIcon.Off);
		self._seenStatusButton.setIcon(icon);
		self._seenStatusButton.setIconSize(QSize(32, 32));
		self.style().unpolish(self);
		self.style().polish(self);
		self._seenStatusButton.style().unpolish(self._seenStatusButton);
		self._seenStatusButton.style().polish(self._seenStatusButton);
		self._messageWidget.style().unpolish(self._messageWidget);
		self._messageWidget.style().polish(self._messageWidget);
		self._timeAgoWidget.style().unpolish(self._timeAgoWidget);
		self._timeAgoWidget.style().polish(self._timeAgoWidget);

	def height(self):
		topHeight = self._titleWidget.size().height();
		if self._closeAllButton.size().height() > topHeight:
			topHeight = self._closeAllButton.size().height();
		topHeight += self._topLayout.contentsMargins().top();
		topHeight += self._topLayout.contentsMargins().bottom();
		return NotifyItemProto.height(self) + topHeight;


class Notifications(NotificationArea):
	"""Diaspora notifications area
	"""
	anchorClicked = pyqtSignal(str);
	def __init__(self, notificationsModel, imageResolver, parent=None):
		NotificationArea.__init__(self, parent=parent);

		self._notificationsModel = notificationsModel;
		self._imageResolver = imageResolver;
		self._notifications = {}; # diaspora notification id : NotificationItem

		self._updateButton = QPushButton("Update", parent=self);
		self._updateButton.clicked.connect(self._updateModel);
		self._topLayout.addWidget(self._updateButton, 0);

		self._markAllButton = QPushButton("Mark all", parent=self);
		#self._markAllButton.clicked.connect(self.); # TODO
		self._topLayout.addWidget(self._markAllButton, 0);

		self._loadMoreButton = QPushButton("Load more", parent=self);
		self._loadMoreButton.clicked.connect(self._moreModel);
		self._contentWidget.layout().addWidget(self._loadMoreButton);

		notificationsModel.updated.connect(self._notificationsUpdated);
		notificationsModel.notificationToggled.connect(self._toggled);

	def _moreModel(self):
		self._notificationsModel.more();

	def _updateModel(self):
		self._notificationsModel.update();
		# TODO Disable controls?

	@pyqtSlot(int)
	def __notificationItemDestroyed(self, id): self._notifications.pop(id);

	def _toggled(self, id): self._notifications[id].toggle();

	def _toggleSeen(self, id):
		self._notificationsModel.toggleSeen(id);

	@pyqtSlot()
	def _toggleSeenClicked(self):
		nItem = self.sender();
		self._toggleSeen(nItem.id());

	@pyqtSlot(QUrl)
	def __anchorClicked(self, quri): self.anchorClicked.emit(quri.toString());

	def _notificationsUpdated(self):
		if not self._notificationsModel:
			for id, item in self._notifications.items():
				item.deleteLater();
				item.setParent(None);
		else:
			for index, notification in enumerate(self._notificationsModel):
				if notification["id"] not in self._notifications:
					nItem = NotificationItem(msg=notification["parsed_html"],
						id=notification["id"],
						type=notification.type,
						createdAt=notification["created_at"],
						unread=notification["unread"],
						parent=self
					);
					self._notifications.update({notification["id"]:nItem});
					self._contentWidget.layout().insertWidget(index, nItem);

					# Avatar
					workerId = self._imageResolver.newJob(
							notification["avatar"]);
					self._imageResolver.worker(workerId).signals.result.connect(
						nItem.avatarWidget().setPath
					);
					self._imageResolver.worker(workerId).signals.exception.connect(
						nItem.avatarWidget().setFailed
					);
					self._imageResolver.startWorker(workerId);

					# Signals
					nItem.toggleSeen.connect(self._toggleSeenClicked);
					nItem.anchorClicked.connect(self.__anchorClicked);
					nItem.destroyed.connect(
						lambda nullptr, id=notification["id"]:
							self.__notificationItemDestroyed(id)
					);
		width = self.adjustWidth();
		self.adjustHeight();
