"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QFrame, QLabel, QPushButton, QHBoxLayout, QVBoxLayout, QSizePolicy
from PyQt5 import QtCore
from PyQt5.QtGui import QFontMetrics

class tagsBox(QFrame):
	tagClicked = QtCore.pyqtSignal(str) # tag_name
	def __init__(self, deletableTags=False, parent=None):
		QFrame.__init__(self, parent)

		QHBoxLayout(self);
		self._deletableTags = deletableTags
		self._items = [];

	def tags(self): return [tagView for tagView in self._items];

	def setDeletable(self,bool):
		for tagView in self._items: tagView.setDeletable(bool);

	def delTag(self,index):
		self._items[index].deleteLater();
		del self._items[tag_name];

	#def set_max_width(self, max_width, change_layout=True):
	#	self.max_width = max_width
	#	for t_name in self.view_tags:
	#		self.view_tags[t_name].set_max_width( max_width )
	#	if change_layout: self.change_layout()

	@QtCore.pyqtSlot()
	def __tagClicked(self):
		self.tagClicked.emit(self.sender.name());

	def addTag(self, name):
		tag = tagView(name, deletable=self._deletableTags);
		self._items.append(tag);
		tag.clicked.connect(self.__tagClicked)
		tag.deleteMe.connect(self.del_tag )

		self.layout().addWidget( self.view_tags[name], 0, QtCore.Qt.AlignLeft )
	
	def clear_layout(self):
		for name in self.view_tags:
			self.view_tags[name].deleteLater()
		self.view_tags = {}
		
	def change_layout(self):
		lo = self.layout()
		if type(lo) is QHBoxLayout and self.size().width() >= self.max_width:
			# delete current layout
			QtCore.QObjectCleanupHandler().add(self.layout())
			
			# create new layout
			new_layout = QVBoxLayout(self)
			
			# add widgets again
			for t_name in self.view_tags:
				self.add_tag( t_name )
			
			# set new layout
			self.setLayout( new_layout )

		elif type(lo) is QVBoxLayout and self.size().width() < self.max_width:
			# delete current layout
			QtCore.QObjectCleanupHandler().add(self.layout())
			
			# create new layout
			new_layout = QHBoxLayout(self)
			
			# add widgets again
			for t_name in self.view_tags:
				self.add_tag( t_name )
			
			# set new layout
			self.setLayout( new_layout )

class tagView(QLabel):
	clicked = QtCore.pyqtSignal();
	deleteMe = QtCore.pyqtSignal();
	def __init__(self, name, deletable=False, parent=None):
		QLabel.__init__(self, parent)
		self.name = name
		self.max_width = max_width
		
		sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
		self.setSizePolicy(sizePolicy)

		name = self.word_wrap_anywhere(name)
		self.set_name(name)

		self.delete_button=None
		if deletable:
			self.delete_button=QPushButton(self)
			self.delete_button.setText("X")
			self.delete_button.show()

		self.default_css = ("color: #fff;\n"
		"background-color: #2c2c2c;\n"
		"border: 1px solid #000;\n"
		"border-radius: 3px;")
		
		self.default_hover_css = ("color: #fff;\n"
		"background-color: #1a1a1a;\n"
		"border: 1px solid #000;\n"
		"border-radius: 3px;")
		
		self.setStyleSheet(self.default_css)
		
		self.setOpenExternalLinks(False)
		self.linkActivated.connect( self.emit_tag_clicked )
		
		self.setCursor( QtCore.Qt.PointingHandCursor )

	def __del_tag(self): self.del_tag.emit(self.name)

	def setDeletable(self,bool):
		if bool and not self.delete_button:
			self.delete_button=QtWidgets.QPushButton(self)
			self.delete_button.setText("X")
			self.delete_button.clicked.connect(self.__del_tag)
			self.delete_button.show()
		elif self.delete_button:
			self.delete_button.deleteLater()
			self.delete_button=None

	def emit_tag_clicked(self):
		self.tag_clicked.emit( self.name )
	
	def set_name(self, name):
		css = '<style>a:link { color: #fff; }</style>'
		self.setText("{}<a href=\"tag://{}\"><b>#{}</b></a>".format(css, name, name))
	
	def set_max_width(self, max_width):
		self.max_width = max_width
		new_text = self.word_wrap_anywhere(self.name)
		self.set_name(new_text)
	
	def enterEvent(self, event):
		self.setStyleSheet(self.default_hover_css)
		
	def leaveEvent(self, event):
		self.setStyleSheet(self.default_css)
	
	def word_wrap_anywhere(self, text):
		font = self.font()
		font_metrics = QFontMetrics(font)
		text_size = font_metrics.size(0, text)
		
		if text_size.width() > self.max_width:	
			new_text = ""
			cur_line = ""
			for char in text:
				if font_metrics.size(0, cur_line+char).width() >= self.max_width:
					cur_line += '<br>'
					new_text += cur_line
					cur_line = ""
				
				cur_line += char
			new_text += cur_line
			return new_text
		return text
