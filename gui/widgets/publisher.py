"""
	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from PyQt5.QtWidgets import QWidget, QFrame, QFileDialog, QLineEdit, QPushButton, QHBoxLayout, QVBoxLayout, QSizePolicy, QAction
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot, QSize, qDebug

from core.notify import Notify

from gui.widgets.avatar import AvatarWidget
from gui.widgets.markdownEditor import MarkdownEditor
from gui.widgets.aspects import AspectsButton
from gui.widgets.layouts import FlowLayout
from gui.widgets.image import Image

import re

class MinimalPublisher(QFrame):
	publishRequest = pyqtSignal();
	def __init__(self, userModel, collapse=True, parent=None):
		QFrame.__init__(self, parent=parent);

		self._userModel = userModel;
		self.__collapseHeight = 60;

		layout = QHBoxLayout(self);
		layout.setContentsMargins(0,0,0,0);
		layout.setSpacing(3);

		self._avatar = AvatarWidget(parent=self);
		layout.addWidget(self._avatar, 0, Qt.AlignTop);

		self._rightLayout = QVBoxLayout();
		self._rightLayout.setContentsMargins(0,0,0,0);
		self._rightLayout.setSpacing(0);
		layout.addLayout(self._rightLayout);

		self._markdownEditor = MarkdownEditor(self);
		self._markdownEditor.editWidget().clicked.connect(self._expand);
		self._rightLayout.addWidget(self._markdownEditor);

		self._closeButton = QAction("X", self);
		self._closeButton.setToolTip("Collapse");
		self._closeButton.triggered.connect(self._collapse);
		spacer = QWidget();
		spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred);
		self._markdownEditor.toolBar().addWidget(spacer);
		self._markdownEditor.toolBar().addAction(self._closeButton);

		self._bottomLayout = QHBoxLayout();
		self._bottomLayout.setSpacing(3);
		self._bottomLayout.setAlignment(Qt.AlignRight);
		self._rightLayout.addLayout(self._bottomLayout);

		self._publishButton = QPushButton("Publish", self);
		self._publishButton.setSizePolicy(
			QSizePolicy.Maximum, QSizePolicy.Maximum
		);
		self._bottomLayout.addWidget(self._publishButton, 0);

		self._publishButton.clicked.connect(self._publish);
		self._userModel.avatarModel().updated.connect(self.__updateAvatar);

		if self._userModel.avatarModel().local(): self.__updateAvatar();
		if collapse: self._collapse();

	def __updateAvatar(self):
		self._avatar.setPath(
			self._userModel.avatarModel().local()
		);

	def avatarWidget(self): return self._avatar;

	def markdownText(self):
		return self._markdownEditor.editWidget().toPlainText();

	def published(self, failed=False):
		if not failed:
			self._markdownEditor.editWidget().clear();
			self._collapse();
		self.setEnabled(True);

	def validateTags(self, markdownText):
		# check if total length of markdown doesn't exceed x
		# .. TODO findout x

		# check if used any tags
		# if so check if they are smaller then 256 chars
		tags = re.findall(r'#([\w0-9\_\-]+)', markdownText);
		for tag in tags:
			if len(tag) > 255: return False;
		return True;

	@pyqtSlot()
	def _publish(self):
		if not self.validateTags(self.markdownText()): 
			Notify.warning("Cannot publish this message since a tag used in "
				"text is to long. The maximum length of a tag is 255 characters"
				" long."
			);
			return;
		elif not self.markdownText():
			Notify.warning("Cannot publish this message. Enter a text first.");
			return;
		self.setEnabled(False);
		self.publishRequest.emit();

	def _collapse(self):
		self._markdownEditor.toolBar().hide();
		self._markdownEditor.tabWidget().setCurrentIndex(0);
		self._markdownEditor.tabWidget().tabBar().hide();
		self._markdownEditor.editWidget().setMaximumHeight(
			self.__collapseHeight
		);
		self._publishButton.hide();
		self.setMaximumHeight(self.__collapseHeight);

	def _expand(self):
		self._publishButton.show();
		self._markdownEditor.toolBar().show();
		self._markdownEditor.tabWidget().tabBar().show();
		self._markdownEditor.editWidget().setMaximumHeight(16777215);
		self.setMaximumHeight(16777215);

class VSpacer(QFrame):
	def __init__(self, parent=None):
		QFrame.__init__(self, parent=parent)
		self.setFrameShape(QFrame.HLine);
		self.setFrameShadow(QFrame.Sunken);

class PollEditAnswer(QFrame):
	#removeMe = pyqtSignal(int) # self.id
	def __init__(self, id, parent=None):
		QFrame.__init__(self, parent);
		QHBoxLayout(self);

		self._answerEdit = QLineEdit(self);
		self._answerEdit.setPlaceholderText("Answer ..");
		self.layout().addWidget(self._answerEdit);

		self._deleteButton = QPushButton("X", self);
		self.layout().addWidget(self._deleteButton);

		self._id = id; # temporary id

		self._deleteButton.clicked.connect(self._removeMe);

		self._deleteButton.hide();

	def answer(self): return self._answerEdit.text();
	def deleteButton(self): return self._deleteButton;

	def _removeMe(self):
		self.deleteLater();
		#self.removeMe.emit(self.id)

class PollEdit(QFrame):
	def __init__(self, parent=None):
		QFrame.__init__(self, parent=parent)
		QVBoxLayout(self);

		questionLayout = QHBoxLayout();
		self.layout().addLayout(questionLayout);

		self._questionEdit = QLineEdit(self);
		self._questionEdit.setPlaceholderText("Question..");
		questionLayout.addWidget(self._questionEdit, 1);

		delPollButton = QPushButton("X", self);
		delPollButton.clicked.connect(self.deleteLater);
		questionLayout.addWidget(delPollButton, 0);

		vSpacer = VSpacer(self);
		self.layout().addWidget(vSpacer);


		self._answersLayout = QVBoxLayout();
		self.layout().addLayout(self._answersLayout);


		bottomLayout = QHBoxLayout();
		self.layout().addLayout(bottomLayout);

		vSpacer = VSpacer(self);
		bottomLayout.addWidget(vSpacer, 1);

		addAnswerButton = QPushButton("+", self);
		bottomLayout.addWidget(addAnswerButton, 0);

		self._answers = {};
		self._answerIdCounter = -1;
		self._deleteButtonsVisible = False;

		# Add default answers (minimal 2)
		for i in range(0,2): self.addAnswer()

		# Hooks
		addAnswerButton.clicked.connect(self.addAnswer);

	def data(self):
		question = self._questionEdit.text().strip();
		if question:
			answers = [];
			for id in self._answers:
				answer = self._answers[id].answer().strip();
				if answer: answers.append(str(answer));
			if len(answers) > 1: return (question, answers);
		return (None, None);

	def toggleDeleteButtons(self):
		if len(self._answers) > 2:
			for id in self._answers: self._answers[id].deleteButton().show()
			self._deleteButtonsVisible = True
		elif self._deleteButtonsVisible:
			for id in self._answers: self._answers[id].deleteButton().hide()
			self._deleteButtonsVisible = False

	def addAnswer(self):
		self._answerIdCounter +=1;
		answer = PollEditAnswer(self._answerIdCounter);
		id = self._answerIdCounter;
		answer.destroyed.connect(lambda nullptr, id=id: self.removeAnswer(id));
		self._answersLayout.addWidget(answer);
		self._answers.update({self._answerIdCounter:answer});
		self.toggleDeleteButtons();

	def removeAnswer(self,id):
		self._answers.pop(id);
		self.toggleDeleteButtons();


class DeletableThumb(Image):
	deleteMe = pyqtSignal(int);
	def __init__(self, path="", photoId=0, parent=None):
		Image.__init__(self, path=path, maxSize=QSize(100, 100),
			disableAnimation=True, parent=parent
		);
		self._photoId = photoId;
		self.setPath(path);

		self._deleteButton = QPushButton(self);

		icon = QIcon();
		pixmap = QPixmap(":/images/delete_thumb");
		icon.addPixmap(pixmap, QIcon.Normal, QIcon.Off);

		test = QPixmap(path);
		size = QSize();
		if (test.rect().size().width() < pixmap.rect().size().width() or
			test.rect().size().height() < pixmap.rect().size().height()):
			if test.rect().size().width() > test.rect().size().height():
				size.setWidth(test.rect().size().height());
				size.setHeight(test.rect().size().height());
			else:
				size.setWidth(test.rect().size().width());
				size.setHeight(test.rect().size().width());
		else: size = pixmap.rect().size();

		self._deleteButton.setIcon(icon);
		self._deleteButton.setIconSize(size);
		self._deleteButton.setFlat(True);
		self._deleteButton.clicked.connect(self._deleteMe);


		self._deleteButton.hide();

	def _deleteMe(self): self.deleteMe.emit(self.photoId());

	def photoId(self): return self._photoId;

	def setHeight(self, height):
		self._fixedHeight = height;
		self.setMaximumHeight(height);
		self.setMaximumWidth(height);

	def enterEvent(self, event):
		self._deleteButton.show();
		x = self.size().width() / 2;
		x -= self._deleteButton.size().width() / 2;
		y = self.size().height() / 2;
		y -= self._deleteButton.size().height() / 2;
		self._deleteButton.move(x,y);
		self.setToolTip("Delete");

	def leaveEvent(self, event):
		self.setToolTip("");
		self._deleteButton.hide();

class PhotoThumbs(QFrame):
	def __init__(self, parent=None):
		QFrame.__init__(self, parent=parent);
		FlowLayout(horizontalSpacing=3, verticalSpacing=3, parent=self);

		self._thumbPaths = [];
		self._thumbIds = [];

		sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum);
		sizePolicy.setVerticalStretch(1);
		sizePolicy.setHorizontalStretch(1);
		self.setSizePolicy(sizePolicy);

	def _thumbDestroyed(self, path):
		index = self._thumbPaths.index(path);
		self._thumbPaths.pop(index);
		self._thumbIds.pop(index);

	def thumbById(self, photoId):
		index = self._thumbIds.index(photoId);
		item = self.layout().itemAt(index);
		if item: return item.widget();

	def addThumb(self, photoId=None, path=""):
		if path not in self._thumbPaths:
			thumb = DeletableThumb(path=path, photoId=photoId, parent=self);
			self.layout().addWidget(thumb);
			thumb.destroyed.connect(
				lambda nullptr, path=path: self._thumbDestroyed(path)
			);
			self._thumbPaths.append(path);
			self._thumbIds.append(photoId);
			return thumb;
		else: Notify.warning("Photo aleady present.");
		
class PostPublisher(MinimalPublisher):
	photoUploadRequest = pyqtSignal(str); # photo path (local)
	photoDeleteRequest = pyqtSignal(int); # photo id.
	def __init__(self, userModel, parent=None):
		MinimalPublisher.__init__(self, userModel=userModel, collapse=False,
		parent=parent);

		self._pollEdit = None;
		self._photoThumbs = None;
		self._uploadedPhotoIds = [];

		self._bottomButtonsLayout = QHBoxLayout();
		self._bottomButtonsLayout.setAlignment(Qt.AlignRight);
		self._bottomButtonsLayout.setSpacing(3);
		self._rightLayout.insertLayout(1, self._bottomButtonsLayout);

		self._pollButton = QPushButton("poll",self);
		icon = QIcon();
		icon.addPixmap(QPixmap(":/icons/24x24/poll"), QIcon.Normal, QIcon.Off)
		self._pollButton.setIconSize(QSize(24, 24));
		self._pollButton.setIcon(icon);
		self._pollButton.clicked.connect(self._addPollEdit);
		self._bottomButtonsLayout.addWidget(self._pollButton, 0);

		self._photoButton = QPushButton("photo",self);
		icon = QIcon();
		icon.addPixmap(QPixmap(":/icons/24x24/camera"), QIcon.Normal, QIcon.Off)
		self._photoButton.setIconSize(QSize(24, 24));
		self._photoButton.setIcon(icon);
		self._photoButton.clicked.connect(self._addPhotos);
		self._bottomButtonsLayout.addWidget(self._photoButton, 0);

		self._locationButton = QPushButton("location",self);
		icon = QIcon();
		icon.addPixmap(QPixmap(":/icons/24x24/location"), QIcon.Normal, QIcon.Off)
		self._locationButton.setIconSize(QSize(24, 24));
		self._locationButton.setIcon(icon);
		self._bottomButtonsLayout.addWidget(self._locationButton, 0);

		self._aspectsButton = AspectsButton(userModel.aspectsModel(), parent=self);
		self._aspectsButton.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
		self._bottomLayout.insertWidget(0, self._aspectsButton, 0);

		self._collapse();

	def published(self):
		if self._pollEdit:
			self._pollEdit.deleteLater();
			self._pollEdit = None;
		if self._photoThumbs:
			self._photoThumbs.deleteLater();
			self._photoThumbs = None;
		MinimalPublisher.published(self);

	def selectedAspects(self): return self._aspectsButton.selected();

	def _pollEditDestroyed(self):
		self._pollEdit = None;

	def _addPollEdit(self):
		if self._pollEdit == None:
			self._pollEdit = PollEdit(self);
			self._pollEdit.destroyed.connect(self._pollEditDestroyed);
			self._rightLayout.insertWidget(0, self._pollEdit);

	def pollData(self):
		if self._pollEdit: return self._pollEdit.data();
		return (None, None);

	def uploadedPhotoIds(self): pass;

	def photoDeleted(self, photoId):
		self._uploadedPhotoIds.pop(self._uploadedPhotoIds.index(photoId));
		thumb = self._photoThumbs.thumbById(photoId);
		thumb.deleteLater();

	def photoUploaded(self, photoId, filepath):
		self._uploadedPhotoIds.append(photoId);
		thumb = self._photoThumbs.addThumb(photoId, filepath);
		if thumb: thumb.deleteMe.connect(self.photoDeleteRequest);

	def _addPhotos(self):
		""" Diaspora allows the following formats:
		*.jpeg *.jpg *.png *.gif

		TODO:
		- check for max file size
		- check for max resolution
		- client size resize?
		"""
		options = QFileDialog.Options()
		options |= QFileDialog.DontUseNativeDialog
		photos, _ = QFileDialog.getOpenFileNames(self,"Select images", "","Images / Photo's (*.jpeg *.jpg *.png *.gif);;All (*)", options=options)
		if photos:
			if self._photoThumbs == None:
				self._photoThumbs = PhotoThumbs(self);
				self._rightLayout.addWidget(self._photoThumbs);
			for filepath in photos: self.photoUploadRequest.emit(filepath);

	def _collapse(self):
		self._aspectsButton.hide();
		self._pollButton.hide();
		self._photoButton.hide();
		self._locationButton.hide();
		if self._pollEdit: self._pollEdit.hide();
		if self._photoThumbs: self._photoThumbs.hide();
		MinimalPublisher._collapse(self);

	def _expand(self):
		self._aspectsButton.show();
		self._pollButton.show();
		self._photoButton.show();
		self._locationButton.show();
		if self._pollEdit: self._pollEdit.show();
		if self._photoThumbs: self._photoThumbs.show();
		MinimalPublisher._expand(self);
