/*	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
/* Toolbar (side menu)
 * */

NavigationBarView QWidget#NavigationBar{
	background-color: #1D1919;
}

NavigationBarView QWidget#ToggleButton {
	background: #334433;
}

NavigationBarAction, NavigationBarAction * {
	color: white;
	background-color: black;
}

NavigationBarAction[hover=true],
NavigationBarAction[hover=true] * {
	background-color: #111111;
}

NavigationBarAction[checked=true],
NavigationBarAction[checked=true] * {
	background-color: #355535;
}

/* Header
 *
 * */
HeaderView {
	background-color: black;
}

HeaderView QPushButton,						/*header search field*/
HeaderView QLineEdit {						/*header buttons*/
	margin: 3px;
	background-color: black;
	border: 1px solid #2c2c2c;
	border-radius: 3px;
	color: white;
}

CountButton #count {
	margin: 1px;
	background-color: rgba(255, 0, 0, 160);
	border-radius: 2px;
	color: white;
	font-weight: bold;
}


/* Checked head of a group */
NavigationBarGroup[active=true] NavigationBarAction#NavigationBarGroupTopAction,
NavigationBarGroup[active=true] NavigationBarAction#NavigationBarGroupTopAction * {
	background-color: green;
}

/* Group expand/collapse button */
NavigationBarGroup NavigationBarAction QPushButton#toggleButton {
	background-color: green;
}
NavigationBarGroup NavigationBarAction QPushButton#toggleButton[state=true] {
	background-color: red;
}

/* Childeren of a group */
NavigationBarGroup NavigationBarAction[child=true] QLabel {
	margin-left: 10px;
}

/* Checked childeren of a group */
NavigationBarGroup[active=true] NavigationBarAction[checked=true],
NavigationBarGroup[active=true] NavigationBarAction[checked=true] * {
	background-color: purple;
}


/* Pages
 * */
PageProto QWidget#PageContent {
	color: white;
	background-color: #1a1a1a;
}

StreamPage QLabel {							/* StreamPage header (title) */
	color: white;
}

/* SettingsPage */
SettingsPage QGroupBox::title {
	subcontrol-origin: margin;
	subcontrol-position: top center;/* position at the top center */
	padding: 0 3px;
	color: black;
	background-color: orange;
}
SettingsPage QLabel,
SettingsPage QCheckBox {
	color: white;
}

/* ConnectionSettings */
ConnectionSettings QLineEdit {
	border: 1px solid black;
}
ConnectionSettings QLineEdit[valid=false] {
	border: 1px solid red;
}
ConnectionSettings QLineEdit[valid=true] {
	border: 1px solid green;
}

/* Publisher
 * */
MinimalPublisher, MinimalPublisher MarkdownEditor QToolBar, MinimalPublisher MarkdownEditor QToolBar QToolButton {
	background-color: 3a4631;
	margin: 3px;
	border: 1px solid grey;
	border-radius: 3px;
	color: white;
}

QToolBar QToolButton:hover {
	background-color: green;
}

NotificationArea QScrollArea, NotificationArea QWidget#centralWidget{
	background-color:transparent;
}


NotificationArea {
	background-color: rgba(0, 0, 0, 180);
}

NotificationArea QPushButton {
	background-color: grey;
	color: black;
}

NotifyItemProto {
	background-color: black;
	margin: 3px;
	border: 1px solid grey;
	border-radius: 6px;
	color: white;
}
NotifyItemProto, NotifyItemProto * {
	background-color: dark-grey;
	color: white;
}

NotifyItemProto[type="message"], NotifyItemProto[type="message"] * {
	background-color: green;
	color: white;
}

NotifyItemProto[type="warning"], NotifyItemProto[type="warning"] * {
	background-color: orange;
	color: white;
}

NotificationItem[seen=false], NotificationItem[seen=false] * {
	background-color: green;
	color: white;
}
NotificationItem[seen=true], NotificationItem[seen=true] * {
	background-color: #2c2c2c;
	color: white;
}



/* PostView
 * */
PostView, PostView HtmlContent, PhotoBrowserView, PhotoBrowserView QScrollArea, PhotoBrowserView QWidget{
	background-color: #3a4631;
}

PostView[selected=true] {
	border-left: 4px solid green;
}

PostView OptionButton:hover {				/*Post option buttons*/
	margin: 1px;
	border: 1px solid grey;
	border-radius: 2px;
	background-color: red;
}

HtmlContent {
	qproperty-styleSheet: "a { color: #50ae50; }";
	color: #aaaaaa;
	font-size: 18px;
}

Poll {
	margin: 3px;
	border: 1px solid grey;
	border-radius: 5px;
	background-color: #5a6651;
	color: white;
}

PollAnswer[selected=true] {
	background-color: #6a7661;
	margin: 3px;
	border: 1px solid grey;
	border-radius: 5px;
}

InteractionButton {
	margin: 3px;
	border: 1px solid grey;
	border-radius: 5px;
	background-color: white;
	color: black;
}

CommentsView {
	background-color: #102500;
	border: 1px solid black;
}

CommentView, CommentView HtmlContent {
	background-color: #1d350b;
}
Thumb {
	border-radius: 5px;
	border: 3px solid black;
}

Thumb[hover=true] {
	border: 3px solid red;
}

MarkdownViewer QPushButton#CollapseButton {
	margin: 3px;
	color: white;
	background-color: black;
	border-radius: 3px;
	border: 2px solid black;
}

/* Reshare PostView
 * */
PostView PostView, PostView PostView HtmlContent, PostView PostView PhotoBrowserView, PostView PostView PhotoBrowserView QScrollArea, PostView PostView PhotoBrowserView QWidget{
	background-color: #2a3621;
}

CommentsView QPushButton#ToggleButton {
	margin: 3px;
	color: white;
	background-color: black;
	border-radius: 3px;
	border: 2px solid black;
}

/* TagsStreamView
 * */
TagsStreamView #TagsFollowButton {
	color: black;
	background-color: orange;
}
TagsStreamView #TagsFollowButton[following=true] {
	background-color: green;
}

/* UserNameView
 * */
UserNameView
{
 qproperty-textColor: red;
 /*qproperty-backgroundColor: black;
 qproperty-borderColor: red;*/
 qproperty-bold: true;
 qproperty-fontSize: 9;
}
