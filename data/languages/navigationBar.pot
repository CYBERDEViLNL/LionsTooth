# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-12-16 02:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"


#: controllers/navigationBarController.py:39
msgid "Stream"
msgstr ""

#: controllers/navigationBarController.py:40
msgid "@Me"
msgstr ""

#: controllers/navigationBarController.py:46
msgid "Activity"
msgstr ""

#: controllers/navigationBarController.py:49
msgid "Liked"
msgstr ""

#: controllers/navigationBarController.py:52
msgid "Commented"
msgstr ""

#: controllers/navigationBarController.py:64
msgid "Aspects"
msgstr ""

#: controllers/navigationBarController.py:76
msgid "Following Tags"
msgstr ""

#: controllers/navigationBarController.py:86
msgid "Public"
msgstr ""

#: controllers/navigationBarController.py:90
msgid "Profile"
msgstr ""

#: controllers/navigationBarController.py:94
msgid "Contacts"
msgstr ""

#: controllers/navigationBarController.py:98
msgid "Notifications"
msgstr ""

#: controllers/navigationBarController.py:102
msgid "Conversations"
msgstr ""

#: controllers/navigationBarController.py:105
msgid "D* Settings"
msgstr ""

#: controllers/navigationBarController.py:108
msgid "Settings"
msgstr ""

