# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-12-16 02:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"


#: gui/views/post.py:49
msgid "Delete"
msgstr ""

#: gui/views/post.py:58
msgid "Report"
msgstr ""

#: gui/views/post.py:148
msgid "Show {0} more comments."
msgstr ""

#: gui/views/post.py:149
msgid "Show less comments."
msgstr ""

#: gui/views/post.py:288
msgid "person(s) reshared this."
msgstr ""

#: gui/views/post.py:290
msgid "person(s) liked this."
msgstr ""

#: gui/views/post.py:292
msgid "comments."
msgstr ""

#: gui/views/post.py:405
msgid "Public"
msgstr ""

#: gui/views/post.py:406
msgid "Private"
msgstr ""

#: gui/views/post.py:407
msgid " via {0}"
msgstr ""

#: gui/views/post.py:640 gui/views/post.py:694
msgid "Like"
msgstr ""

#: gui/views/post.py:643
msgid "Reshare"
msgstr ""

#: gui/views/post.py:646
msgid "Comment"
msgstr ""

#: gui/views/post.py:660
msgid "Update post"
msgstr ""

#: gui/views/post.py:693
msgid "Liked"
msgstr ""

