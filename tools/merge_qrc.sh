#!/bin/bash
: '	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
'
function infoStr {
	echo -e """Useage:
	merge_qrc [OUTPUT_FILE] [INPUT_FILE_0] [INPUT_FILE_1]

	[OUTPUT_FILE]		path to store output.
	[INPUT_FILE_0]		first file to merge with the second.
	[INPUT_FILE_1]		second file to merge with the first.
	"""
}

if [ $# -lt 3 ]; then
	infoStr
	exit
fi

OUTPUT_FILE=$1
INPUT_FILES=($2 $3)

if ! [ -w $(dirname $OUTPUT_FILE) ]; then
	echo "Directory is not writeable:'$(dirname $OUTPUT_FILE)'"
	exit
fi

for FILE in "${INPUT_FILES[@]}"; do
	if ! [ -r $FILE ]; then
		echo "File is not readable:'$FILE'"
		exit
	fi
done

declare -A HEADERS
declare -A LINES

CURRENT_HEADER_POS=0
CURRENT_LINE_POS=0

for FILE in "${INPUT_FILES[@]}"; do
	while read LINE; do
		if [[ "$LINE" == *"</file>" ]]; then
			# First check if this line is a duplicate
			DUPLICATE=0
			LINE_NR=0
			TEST=1
			while [ $TEST -eq 1 ]; do
				if [ "${LINES[$CURRENT_HEADER_POS, $LINE_NR]}" == "" ]; then
					TEST=0
				elif [ "${LINES[$CURRENT_HEADER_POS, $LINE_NR]}" == "${LINE}" ]; then
					DUPLICATE=1
				fi
				LINE_NR=$((LINE_NR+1))
			done

			# If not a duplicate then add
			if [ $DUPLICATE -eq 0 ]; then
				LINES[$CURRENT_HEADER_POS, $CURRENT_LINE_POS]="${LINE}"
				CURRENT_LINE_POS=$((CURRENT_LINE_POS+1))
			fi
		elif [[ "$LINE" == *"<qresource"* ]]; then
			if [ ${#HEADERS[@]} -eq 0 ]; then
				# First header
				HEADERS[$CURRENT_HEADER_POS]="${LINE}"
			else
				: 'Check if header already exists and set CURRENT_HEADER_POS
					to that position'
				CURRENT_HEADER_POS=0
				MATCH=0
				for HEADER_LINE in "${HEADERS[@]}"; do
					if [ "${HEADER_LINE}" == "${LINE}" ]; then
						MATCH=1
					else
						CURRENT_HEADER_POS=$((CURRENT_HEADER_POS+1))
					fi
				done

				# New header
				if [ $MATCH -eq 0 ]; then
					HEADERS[$CURRENT_HEADER_POS]="${LINE}"
					CURRENT_HEADER_POS=$((${#HEADERS[@]}-1))
					CURRENT_LINE_POS=0
				fi
			fi
		fi
	done < "$FILE"
done

# Join everything to a new QRC file.
HEADER_NR=0
NEW_QRC="<RCC>\n"
for HEADER_LINE in "${HEADERS[@]}"; do
	if [ $HEADER_NR -eq 0 ]; then
		NEW_QRC="$NEW_QRC  ${HEADER_LINE}\n"
	else
		NEW_QRC="$NEW_QRC\n  ${HEADER_LINE}\n"
	fi
	NEW_QRC_FILES=""
	LINE_NR=0
	TEST=1
	while [ $TEST -eq 1 ]; do
		if [ "${LINES[$HEADER_NR, $LINE_NR]}" == "" ]; then
			TEST=0
		else
			NEW_QRC_FILES="$NEW_QRC_FILES    ${LINES[$HEADER_NR, $LINE_NR]}\n"
		fi
		LINE_NR=$((LINE_NR+1))
	done
	#NEW_QRC_FILES=$(echo -e "${NEW_QRC_FILES}" | sort)
	NEW_QRC="$NEW_QRC$NEW_QRC_FILES"
	NEW_QRC="$NEW_QRC  </qresource>"
	HEADER_NR=$((HEADER_NR+1))
done
NEW_QRC="$NEW_QRC\n</RCC>"

echo -e "${NEW_QRC}" > "${OUTPUT_FILE}"
echo "Done"
