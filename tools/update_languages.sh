#!/bin/bash
: '	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
'
FILES=(settings header post navigationBar);
LANGUAGES=(nl de es ru);

if [ $# -eq 0 ]; then
	if [ "$(basename $(pwd))" == "tools" ]; then
		echo "YEAH TOOLS"
		LANGUAGE_DIR="../data/languages";
	else
		LANGUAGE_DIR="data/languages";
	fi;
else
	LANGUAGE_DIR=$1;
fi;


/usr/lib/python3.7/Tools/i18n/pygettext.py -d "settings" -o "$LANGUAGE_DIR/settings.pot" "gui/pages/settings.py";
/usr/lib/python3.7/Tools/i18n/pygettext.py -d "header" -o "$LANGUAGE_DIR/header.pot" "gui/views/header.py";
/usr/lib/python3.7/Tools/i18n/pygettext.py -d "post" -o "$LANGUAGE_DIR/post.pot" "gui/views/post.py";
/usr/lib/python3.7/Tools/i18n/pygettext.py -d "navigationBar" -o "$LANGUAGE_DIR/navigationBar.pot" "controllers/navigationBarController.py";


# Create/update and merge .po and .mo files.
for LANG in ${LANGUAGES[@]}; do
	if ! [ -d "$LANGUAGE_DIR/$LANG/LC_MESSAGES/" ]; then
		mkdir -p "$LANGUAGE_DIR/$LANG/LC_MESSAGES/";
	fi;

	for NAME in ${FILES[@]}; do
		if ! [ -f "$LANGUAGE_DIR/$LANG/LC_MESSAGES/$NAME.po" ]; then
			cp "$LANGUAGE_DIR/$NAME.pot" "$LANGUAGE_DIR/$LANG/LC_MESSAGES/$NAME.po";
		fi;
		msgmerge -U "$LANGUAGE_DIR/$LANG/LC_MESSAGES/$NAME.po" "$LANGUAGE_DIR/$NAME.pot";
		msgfmt -o "$LANGUAGE_DIR/$LANG/LC_MESSAGES/$NAME.mo" "$LANGUAGE_DIR/$LANG/LC_MESSAGES/$NAME";
	done;
done;
