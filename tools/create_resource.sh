: '	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
'
: '
Batch convert qrc files for all themes in a specific themes directory.
Give path to themes directory as first argument.
'
if [ $# -eq 0 ]; then
	THEME_PATH=../data/themes #fallback
else
	THEME_PATH=$1
fi

if [ -d "$THEME_PATH" ]; then
	for themeName in "$THEME_PATH"/* ; do
		if [ -e "$themeName"/icons.qrc ]; then
			pyrcc5 "$themeName"/icons.qrc -o "$themeName"/icons.py
		fi
	done
	echo "Done"
else
	echo "Theme directory not found. Did you give it as first argument?"
fi
