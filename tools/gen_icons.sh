#!/bin/bash
: '	LionsTooth is a *diaspora client (python3, pyqt5 and diaspy)
	Copyright (C) 2018  CYBERDEViLNL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
'
function infoStr {
	echo -e """Batch convert .svg files to 16x16, 24x24, 32x32 and 64x64 .png
icons. It also outputs a .qrc file.

	Useage:
	gen_icons [OUTPUT_FILE] [SVG_INPUT_DIRECTORY]

	[SVG_INPUT_DIRECTORY]	Path to a directory directory that contains .svg
							files, it will convert those to icons and a .qrc
							file.

	[OUTPUT_DIRECTORY]		Theme path.

	[OUTPUT_FILE]			Filepath to store qrc output.
	"""
}
if [ $# -lt 3 ]; then
	infoStr
	exit
fi


SVG_INPUT_DIRECTORY=$(realpath $1)/
if ! [ -r $(dirname $SVG_INPUT_DIRECTORY) ]; then
	echo "Directory is not writeable:'$(dirname $SVG_INPUT_DIRECTORY)'"
	exit
fi


OUTPUT_DIRECTORY=$(realpath $2)
TO_CHECK=($OUTPUT_DIRECTORY/icons/16x16/ $OUTPUT_DIRECTORY/icons/24x24/ $OUTPUT_DIRECTORY/icons/32x32/ $OUTPUT_DIRECTORY/icons/64x64/)
for DIR in ${TO_CHECK[@]}; do
	if ! [ -d $DIR ]; then
		mkdir -p $DIR;
	fi
	#echo $DIR
done


RC_OUT=$3
if ! [ -w $(dirname $RC_OUT) ]; then
	echo "Directory is not writeable:'$(dirname $RC_OUT)'"
	exit
fi

EXT=.svg
RC_START="""<RCC>
  <qresource prefix=\"icons/\">"""
RC_END="""  </qresource>
</RCC>"""
RC_FILES=""
ADD_FILE() {
	RC_FILES="${RC_FILES}\n    <file alias=\"16x16/${1}\">icons/16x16/${1}.png</file>"
	RC_FILES="${RC_FILES}\n    <file alias=\"24x24/${1}\">icons/24x24/${1}.png</file>"
	RC_FILES="${RC_FILES}\n    <file alias=\"32x32/${1}\">icons/32x32/${1}.png</file>"
	RC_FILES="${RC_FILES}\n    <file alias=\"64x64/${1}\">icons/64x64/${1}.png</file>"
}
CONVERT_FILES() {
	{
		inkscape -z "$1" -e "$OUTPUT_DIRECTORY/icons/16x16/$(basename $1 $EXT).png" -w 16 -h 16
	} &> /dev/null
	if [ $? -eq 0 ]; then
		echo -ne "16x16 "
	else
		echo -ne "FAIL  "
	fi

	{
		inkscape -z "$1" -e "$OUTPUT_DIRECTORY/icons/24x24/$(basename $1 $EXT).png" -w 24 -h 24
	} &> /dev/null
	if [ $? -eq 0 ]; then
		echo -ne "24x24 "
	else
		echo -ne "FAIL  "
	fi

	{
		inkscape -z "$1" -e "$OUTPUT_DIRECTORY/icons/32x32/$(basename $1 $EXT).png" -w 32 -h 32
	} &> /dev/null
	if [ $? -eq 0 ]; then
		echo -ne "32x32 "
	else
		echo -ne "FAIL  "
	fi

	{
		inkscape -z "$1" -e "$OUTPUT_DIRECTORY/icons/64x64/$(basename $1 $EXT).png" -w 64 -h 64
	} &> /dev/null
	if [ $? -eq 0 ]; then
		echo -ne "64x64"
	else
		echo -ne "FAIL  "
	fi

}
echo ""
for filepath in $SVG_INPUT_DIRECTORY*$EXT; do
	if ! [ -r $filepath ]; then
		echo "File is not readable:'$filepath'"
	else
		printf '%-32s' "$(basename $filepath $EXT)"
		CONVERT_FILES $filepath
		ADD_FILE $(basename $filepath $EXT)
		echo -ne "\n"
	fi
done
RC_FILES=$(echo -e "${RC_FILES}" | sort)
RC="${RC_START}${RC_FILES}${RC_END}"
echo -e "${RC}" > "${RC_OUT}"
