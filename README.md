# LionsTooth (WIP)
LionsTooh is a graphical **diaspora\*** client written in Python, it makes use
 of [Diaspy](https://github.com/marekjm/diaspy) for the communication from/to a
 **diaspora\*** pod. The view is done in PyQt5.

More info/docs follows.

# Goals
* [ ] Full featured grapgical **diaspora\*** client.
* [ ] Make as lightweight as possible.
* [ ] Userfriendly.
* [ ] Themeable.
* [ ] Translateable.
* [ ] Touchscreen support.

# Extra goals
* [ ] GPG support for conversations.


# Requirements
| Name		| License		| Version		| Website					|
|:--------- |:------------- |:------------- |:------------------------- |
|Python3	| PSF			| 				| https://www.python.org/ |
|Diaspy		| MIT			| >= 0.6.1.dev	| https://github.com/marekjm/diaspy |
|PyQt5		| GPLv3			| 				| https://www.riverbankcomputing.com/software/pyqt/intro |
|bs4		| MIT			| 				| https://www.crummy.com/software/BeautifulSoup/ |
|urllib		| MIT			| 				| https://urllib3.readthedocs.io/ |
|markdown	| BSD			| 				| https://python-markdown.github.io/ |
|PIL		| PIL			| 				| https://python-pillow.org/ |
|requests	| Apache 2		| 				| http://docs.python-requests.org/en/master/ |
|dateutil	| Apache + BSD	| 				| https://dateutil.readthedocs.io/ |
|keyring	| PSF + MIT		| 				| https://github.com/jaraco/keyring |

# Cloning `diaspy-devel`
***Note***: All commands should be executed from LionsTooth's root directory! So first `cd` in it ;-)
> `svn checkout https://github.com/marekjm/diaspy/branches/devel/diaspy diaspy`


# Generating data
***Note***: All commands should be executed from LionsTooth's root directory! So first `cd` in it ;-)

## Generate language data.
> Generate/update `.po` files to changes and generate`.mo` files from them.

> `bash /tools/update_languages.sh`

## Generate resources (images/icons)
\****Requires `inkscape`, this is temporary.***
> First we generate `.png` icons (16x16, 24x24, 32x32, 64x64) for each theme from their `.svg` files and create a `_icons.qrc` resource file for it with `tools/gen_icons.sh`.

> **default theme**
> `bash tools/gen_icons.sh data/themes/default/svg/icons data/themes/default data/themes/default/_icons.qrc` 

> **light theme**
> `bash tools/gen_icons.sh data/themes/light/svg/icons data/themes/default data/themes/light/_icons.qrc`

Now merge the new created `_icons.qrc` with the theme its '_images.qrc' to `icons.qrc` with `tools/merge_qrc.sh`. 

> **default theme**
> `bash tools/merge_qrc.sh data/themes/default/icons.qrc data/themes/default/_icons.qrc data/themes/default/_images.qrc`

> **light theme**
> `bash tools/merge_qrc.sh data/themes/light/icons.qrc data/themes/light/_icons.qrc data/themes/light/_images.qrc`

At last we are going to create python modules from each theme it's `icons.qrc` resource file with `tools/create_resource.sh`.

> `bash tools/create_resource.sh data/themes/`
